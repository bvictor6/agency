<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExtraCoversTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExtraCoversTable Test Case
 */
class ExtraCoversTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExtraCoversTable
     */
    public $ExtraCovers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.extra_covers',
        'app.cover_types',
        'app.vehicles',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ExtraCovers') ? [] : ['className' => ExtraCoversTable::class];
        $this->ExtraCovers = TableRegistry::get('ExtraCovers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ExtraCovers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
