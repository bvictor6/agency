<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoverTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoverTypesTable Test Case
 */
class CoverTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CoverTypesTable
     */
    public $CoverTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cover_types',
        'app.line_of_businesses',
        'app.policies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CoverTypes') ? [] : ['className' => CoverTypesTable::class];
        $this->CoverTypes = TableRegistry::get('CoverTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CoverTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
