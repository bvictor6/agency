<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PolicyActionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PolicyActionsTable Test Case
 */
class PolicyActionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PolicyActionsTable
     */
    public $PolicyActions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.policy_actions',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PolicyActions') ? [] : ['className' => PolicyActionsTable::class];
        $this->PolicyActions = TableRegistry::get('PolicyActions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PolicyActions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
