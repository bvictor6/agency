<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LinesOfBusinessesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LinesOfBusinessesTable Test Case
 */
class LinesOfBusinessesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LinesOfBusinessesTable
     */
    public $LinesOfBusinesses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.lines_of_businesses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LinesOfBusinesses') ? [] : ['className' => LinesOfBusinessesTable::class];
        $this->LinesOfBusinesses = TableRegistry::get('LinesOfBusinesses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LinesOfBusinesses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
