<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InsuranceFirmsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InsuranceFirmsTable Test Case
 */
class InsuranceFirmsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InsuranceFirmsTable
     */
    public $InsuranceFirms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.insurance_firms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InsuranceFirms') ? [] : ['className' => InsuranceFirmsTable::class];
        $this->InsuranceFirms = TableRegistry::get('InsuranceFirms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InsuranceFirms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
