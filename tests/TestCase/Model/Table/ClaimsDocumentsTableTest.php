<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimsDocumentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimsDocumentsTable Test Case
 */
class ClaimsDocumentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimsDocumentsTable
     */
    public $ClaimsDocuments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.claims_documents',
        'app.document_types',
        'app.claims'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ClaimsDocuments') ? [] : ['className' => ClaimsDocumentsTable::class];
        $this->ClaimsDocuments = TableRegistry::get('ClaimsDocuments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimsDocuments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
