<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LineOfBusinessesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LineOfBusinessesTable Test Case
 */
class LineOfBusinessesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LineOfBusinessesTable
     */
    public $LineOfBusinesses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.line_of_businesses',
        'app.policies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LineOfBusinesses') ? [] : ['className' => LineOfBusinessesTable::class];
        $this->LineOfBusinesses = TableRegistry::get('LineOfBusinesses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LineOfBusinesses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
