<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger" onclick="this.classList.add('hidden');"><i class="fa fa-info-circle"></i>&nbsp;&nbsp;<?= $message ?></div>

