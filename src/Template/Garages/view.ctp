<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Garage $garage
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $garage->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Garage Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;GARAGES'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Garages','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create Garage','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Setup</li>
                <li> <?= $this->Html->link('Garage', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($garage->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title"><?= h($garage->name) ?></h3>
    </div>
    <div class="block-content">
        <div class="row items-push">         
            <div class="col-sm-6">
                <h5><?= __('Description') ?></h5>
                <?= $this->Text->autoParagraph(h($garage->description)); ?>
            </div> 
        </div>
        <!-- End of main content -->
        
        <!-- Start related stuff  -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Claims</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($garage->claims)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>                
                                <th class="hidden-sm"><?= __('Vehicle') ?></th>
                                <th class="hidden-sm"><?= __('Accident Date') ?></th>
                                <th class="hidden-sm"><?= __('Date Created') ?></th>
                                
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($garage->claims as $claims):  ?>
                            <tr>
                                <td class="hidden-xs"><?= h($claims->vehicle_id) ?></td>
                                <td class="hidden-xs"><?= h($claims->accident_date) ?></td>
                                <td class="hidden-xs"><?= h($claims->date_reported) ?></td>
                                                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Claims', 'action' => 'view', $claims->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Claim Details','escape'=>false]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
       
        <!-- End related stuff -->  
    </div>
</div>
<!-- End Paragraphs in Block -->