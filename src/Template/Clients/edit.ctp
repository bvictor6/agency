<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <!--h3 class="h3 text-danger animated zoomIn"><i class="fa fa-plus"></i>&nbsp;Users</h3-->

            <div class="btn-group">    
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;CLIENTS'), 
                        ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Clients','escape'=>false]) ?>

                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                        ['controller' => 'CoverTypes', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                        ['controller' => 'InsuranceFirms', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                        ['controller' => 'Policies', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Client Policies','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Clients</li>
                <li><?= $this->Html->link('Clients',['action'=>'index'] ,['class'=>'link-effect']) ?></li>
                <li><?= $this->Html->link('New',['action'=>'add'] ,['class'=>'link-effect']) ?></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Form -->
<div class="block block-bordered">
    <div class="block-header bg-gray-lighter">
        <ul class="block-options">
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
        </ul>
        <h3 class="block-title">Client <small>&nbsp;NEW</small></h3>
    </div>
    <div class="block-content"> 
        <?= $this->Form->create($client,["autocomplete"=>"off"])  ?>
        <fieldset>
            <div class="col-sm-6"><?php echo $this->Form->control('name'); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('type', ['options'=>['individual'=>'Individual','corporate'=>'Corporate']]); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('gender',['options'=>['male'=>'Male','female'=>'Female', 'other'=>'Other']]);  ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('email'); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('phone'); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('nationality'); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('id_no', ['label'=>'ID/Passport No','style'=>'text-transform: uppercase','required']); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('pin_no', ['label'=>'PIN No','style'=>'text-transform: uppercase','required']); ?></div>            
            <div class="col-sm-6"><?php echo $this->Form->control('address'); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('city', ['label'=>'City/Town']); ?></div>        
        </fieldset>
        <?= $this->Form->button(__("&nbsp;&nbsp;Update Client Details"), 
                ['class'=>'btn btn-md btn-danger fa fa-save','data-action'=>'refresh_toggle',
                    'data-toggle'=>'block-option']) ?>
        <?= $this->Form->end() ?>        
        <div>&nbsp;</div>
    </div>
</div>
<!-- End Form -->