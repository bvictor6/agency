<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $client->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Client Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;CLIENTS'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Clients','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create New Client','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Clients</li>
                <li> <?= $this->Html->link('Clients', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($client->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Details</h3>
    </div>
    <div class="block-content">
        <div class="row items-push">
            <div class="col-sm-6">
                
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600"><?= __('Name') ?></th>
                            <td class="font-w400"><?= h($client->name) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Gender') ?></th>
                            <td class="font-w400"><?= h($client->gender) ?></td>
                        </tr>

                        <tr>
                            <td class="font-w600"><?= __('Email') ?></th>
                            <td class="font-w400"><?= h($client->email) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Phone') ?></th>
                            <td class="font-w400"><?= h($client->phone) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('ID/Passport#') ?></th>
                            <td class="font-w400"><?= h($client->id_no) ?></td>
                        </tr>                        
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6">
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600"><?= __('Client Type') ?></th>
                            <td class="font-w400"><?= h($client->type) ?></td>
                        </tr> 
                        <tr>
                            <td class="font-w600"><?= __('PIN#') ?></th>
                            <td class="font-w400"><?= h($client->pin_no) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Nationality') ?></th>
                            <td class="font-w400"><?= h($client->nationality) ?></td>
                        </tr> 
                        <tr>
                            <td class="font-w600"><?= __('Address') ?></th>
                            <td class="font-w400"><?= $this->Text->autoParagraph(h($client->address)) ?></td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Paragraphs in Block -->
