<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Batch $batch
 */
?>



<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;CERTIFICATE BATCHES'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Certificate Batches','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'New Batch','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Certificates</li>
                <li> <?= $this->Html->link('Certificate Batch', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($batch->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">BATCH#&nbsp;&nbsp;<?= h($batch->batch_no) ?></h3>
    </div>
    <div class="block-content">
        <div class="row items-push">           
            <div class="col-sm-6">
                <h5>Insurance Firm</h5>
                <?= $batch->has('insurance_firm') ? $this->Html->link($batch->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $batch->insurance_firm->id]) : '' ?>
            </div>                           
            <div class="col-sm-6">
                <h5><?= __('Category') ?></h5>
                <?= $batch->has('category') ? $this->Html->link($batch->category->name, ['controller' => 'Categories', 'action' => 'view', $batch->category->id]) : '' ?>
            </div> 
            <div class="col-sm-6">
                <h5>Date Received</h5>
                <?= h($batch->date_received) ?>
            </div>
            <div class="col-sm-6">
                <h5>Certificate Serial</h5>
                <?= h($batch->batch_from) ?>&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;<?=h($batch->batch_to) ?>
            </div>
        </div>
        <!-- End of main content -->
        
        <!-- Start related stuff  -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Certificates</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($batch->certificates)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="hidden-sm"><?= __('Serial#') ?></th>
                                <th class="hidden-sm"><?= __('Status') ?></th>
                                <th class="hidden-sm"><?= __('Printed') ?></th>
                                
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($batch->certificates as $certificates):  ?>
                            <tr>
                                <td class="hidden-xs"><?= h($certificates->serial_no) ?></td>
                                <!--td class="hidden-xs"><?= h($certificates->vehicle->registration_number) ?></td>
                                <td class="hidden-xs"><?= h($certificates->vehicle->policy->name) ?></td-->
                                <td class="hidden-xs">
                                    <?php if($certificates->status){ ?>
                                    <span class="fa fa-lock label label-warning">&nbsp;&nbsp;Assigned</span>
                                    <?php } else { ?>
                                        <span class="fa fa-unlock-alt label label-success">&nbsp;&nbsp;Not Assigned</span>
                                    <?php } ?>
                                </td>
                                <td class="hidden-xs">
                                    <?php if($certificates->printed){ ?>
                                    <span class="fa fa-lock label label-warning">&nbsp;&nbsp;Printed</span>
                                    <?php } else { ?>
                                        <span class="fa fa-unlock-alt label label-success">&nbsp;&nbsp;Not Printed</span>
                                    <?php } ?>
                                </td>
                                <td hidden="hidden">&nbsp;</td>                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Certificates', 'action' => 'view', $certificates->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Certificate Details','escape'=>false]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
       
        <!-- End related stuff -->  
    </div>
</div>
<!-- End Paragraphs in Block -->
