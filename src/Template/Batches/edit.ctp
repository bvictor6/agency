<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Batch $batch
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $batch->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $batch->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Insurance Firms'), ['controller' => 'InsuranceFirms', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Insurance Firm'), ['controller' => 'InsuranceFirms', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="batches form large-9 medium-8 columns content">
    <?= $this->Form->create($batch) ?>
    <fieldset>
        <legend><?= __('Edit Batch') ?></legend>
        <?php
            echo $this->Form->control('batch_no');
            echo $this->Form->control('insurance_firm_id', ['options' => $insuranceFirms]);
            echo $this->Form->control('category_id', ['options' => $categories]);
            echo $this->Form->control('date_received');
            echo $this->Form->control('date_created', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
