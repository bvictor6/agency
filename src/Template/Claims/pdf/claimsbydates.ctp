<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim[]|\Cake\Collection\CollectionInterface $claims
 */
?>

<!-- Page Content -->
<div class="row">
    <!-- Dynamic Table Full -->
    <div class="columns large-12">
        <h3 class="block-title">Claim's <small><?= $from ?> TO <?= $to ?></small></h3>
        <div>
            <table>
                <thead>
                    <tr>
                        <th class="font-s12">Claim#</th>
                        <th class="font-s12">Policy#</th>
                        <th class="font-s12">Vehicle</th>
                        <th class="font-s12">Garage</th>
                        <th class="font-s12">Date Reported</th>
                        <th class="font-s12">Accident Date</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($claims as $claim): ?>
                    <tr>
                        <td class="font-s12"><?= h($claim->claim_no) ?></td>
                        <td class="font-s12"><?= $claim->has('policy') ? $this->Html->link($claim->policy->name, ['controller' => 'Policies', 'action' => 'view', $claim->policy->id]) : '' ?></td>
                        <td class="font-s12"><?= $claim->has('vehicle') ? $this->Html->link($claim->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $claim->vehicle->id]) : '' ?></td>
                        <td class="font-s12"><?= $claim->has('garage') ? $this->Html->link($claim->garage->name, ['controller' => 'Garages', 'action' => 'view', $claim->garage->id]) : '' ?></td>
                        <td class="font-s12"><?= h($claim->date_reported) ?></td>
                        <td class="font-s12"><?= h($claim->accident_date) ?></td>
                                                                                         
                        
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->