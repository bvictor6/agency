<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim $claim
 */
?>

<?= $this->Html->css('base.css',['fullBase' => true]) ?>
<?= $this->Html->css('style.css',['fullBase' => true]) ?>
<?= $this->Html->css('home.css',['fullBase' => true]) ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">


<!-- Paragraphs in Block -->
<div class="row">
    
    <div class="columns large-12">
        <h3><?= $claim->claim_no ?></h3>
        <div>
            <div>
                
                <table>
                    <tbody>
                        <tr>
                            <td class="font-s12"><?= __('Policy#') ?></th>
                            <td class="font-s12"><?= $claim->has('policy') ? $this->Html->link($claim->policy->name, ['controller' => 'Policies', 'action' => 'view', $claim->policy->id]) : '' ?></td>
                        </tr>
                        <tr>
                            <td class="font-s12"><?= __('Vehicle') ?></th>
                            <td class="font-s12"><?= $claim->has('vehicle') ? $this->Html->link($claim->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $claim->vehicle->id]) : '' ?></td>
                        </tr>
                        <tr>
                            <td class="font-s12"><?= __('Garage') ?></th>
                            <td class="font-s12"><?= $claim->has('garage') ? $this->Html->link($claim->garage->name, ['controller' => 'Garages', 'action' => 'view', $claim->garage->id]) : '' ?></td>
                        </tr>

                        <tr>
                            <td class="font-s12"><?= __('Accident Date') ?></th>
                            <td class="font-s12"><?= date('Y-M-d',  strtotime($claim->accident_date)) ?></td>
                        </tr> 
                        <tr>
                            <td class="font-s12"><?= __('Date Reported') ?></th>
                            <td class="font-s12"><?= date('Y-M-d',  strtotime($claim->date_reported)) ?></td>
                        </tr> 
                    </tbody>
                </table>
            </div>
            <div>
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-s12">&nbsp;</th>
                            <td class="font-s12">&nbsp;</td>
                        </tr>                                                 
                    </tbody>
                </table>
            </div>
            <div>
                <h4><?= __('Description') ?></h4>
                <?= $this->Text->autoParagraph(h($claim->description)); ?>
            </div>
            <!-- Related Documents -->
            <div>
                    <h5 class="block-title">Related <small>Documents</small></h5>
                <div>
                    <?php if (!empty($claim->claims_documents)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table>
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="font-s12">Type</th>
                                <th class="font-s12">Name</th>
                                <th class="font-s12">Filesize</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($claim->claims_documents as $documents): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="font-s12"><?= h($documents->document_type->name) ?></td>
                                <td class="font-s12"><?= h($documents->name) ?></td>
                                <td class="font-s12"><?= h($documents->filesize) ?></td>
                                                            
                                
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        <!-- End Related Documents -->
        </div>       
        
    </div>
</div>
<!-- End Paragraphs in Block -->