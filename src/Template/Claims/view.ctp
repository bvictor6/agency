<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim $claim
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $claim->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Claim Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;CLAIMS'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Claims','escape'=>false]) ?>
                    
                
            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Claims</li>
                <li> <?= $this->Html->link('Claims', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($claim->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header col-sm-7">
        <h3 class="block-title"><?= $claim->claim_no ?></h3>
    </div>
    <!-- Excel Button -->
    <div class="col-sm-5 text-right hidden-xs">
        <ol class="breadcrumb push-10-t">
            <?= $this->Html->link(__('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF'), 
                        ['action' => 'view', $claim->id,'_ext' => 'pdf'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Export PDF','escape'=>false]) ?>
            
        </ol>
    </div>
    <!-- End Excel Button -->
    <div class="block-content">
        <div class="row items-push">
            <div class="col-sm-6">
                
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600"><?= __('Policy#') ?></th>
                            <td class="font-w400"><?= $claim->has('policy') ? $this->Html->link($claim->policy->name, ['controller' => 'Policies', 'action' => 'view', $claim->policy->id]) : '' ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Vehicle') ?></th>
                            <td class="font-w400"><?= $claim->has('vehicle') ? $this->Html->link($claim->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $claim->vehicle->id]) : '' ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Garage') ?></th>
                            <td class="font-w400"><?= $claim->has('garage') ? $this->Html->link($claim->garage->name, ['controller' => 'Garages', 'action' => 'view', $claim->garage->id]) : '' ?></td>
                        </tr>

                        <tr>
                            <td class="font-w600"><?= __('Accident Date') ?></th>
                            <td class="font-w400"><?= date('Y-M-d',  strtotime($claim->accident_date)) ?></td>
                        </tr> 
                        <tr>
                            <td class="font-w600"><?= __('Date Reported') ?></th>
                            <td class="font-w400"><?= date('Y-M-d',  strtotime($claim->date_reported)) ?></td>
                        </tr> 
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6">
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600">&nbsp;</th>
                            <td class="font-w400">&nbsp;</td>
                        </tr>                                                 
                    </tbody>
                </table>
            </div>
            <div class="col-sm-12">
                <h4><?= __('Description') ?></h4>
                <?= $this->Text->autoParagraph(h($claim->description)); ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-6">
                <div class="btn-group">
                    <?= $this->Html->link(__('<i class="fa fa-upload"></i>&nbsp;&nbsp;UPLOAD DOCUMENTS'), 
                            ['controller' => 'ClaimsDocuments', 'action' => 'add',$claim->id],
                            ['class'=>'btn btn-sm btn-info animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'Upload Claims Documents','escape'=>false]) ?>

                </div> 
            </div>             
            <div class="col-sm-3">&nbsp;</div>
        </div>
        <!-- Related Documents -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Documents</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($claim->claims_documents)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="hidden-xs">Type</th>
                                <th class="hidden-sm">Name</th>
                                <th class="hidden-sm">Filesize</th>
                                
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($claim->claims_documents as $documents): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="hidden-xs"><?= h($documents->document_type->name) ?></td>
                                <td class="hidden-xs"><?= h($documents->name) ?></td>
                                <td class="hidden-xs"><?= h($documents->filesize) ?></td>
                                                           
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'ClaimsDocuments', 'action' => 'view', $documents->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Document Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Related Documents -->
    </div>
</div>
<!-- End Paragraphs in Block -->