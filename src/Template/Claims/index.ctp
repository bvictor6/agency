<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim[]|\Cake\Collection\CollectionInterface $claims
 */
?>

<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                
                <div class="btn-group">                     
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;VEHICLES'), 
                        ['controller' => 'Vehicles', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Vehicles','escape'=>false]) ?>
                
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                            ['controller' => 'InsuranceFirms', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>

                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;LOB\'s'), 
                            ['controller' => 'LinesOfBusinesses', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Lines of Business','escape'=>false]) ?>
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Underwriting</li>
                    <li><a class="link-effect" href="">Claims</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Claim's <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                &nbsp;
            </ol>
        </div>
        <!-- End Excel Button --> 
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 7%;"></th> 
                        <th class="hidden-sm">Claim#</th>
                        <th class="hidden-sm">Policy#</th>
                        <th class="hidden-sm">Vehicle</th>
                        <th class="hidden-sm">Garage</th>
                        <th class="hidden-sm">Date Reported</th>
                        <th class="hidden-sm">Accident Date</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($claims as $claim): ?>
                    <tr>
                        <td class="text-center"><?= $this->Number->format($claim->id) ?></td>
                        <td class="font-w300"><?= h($claim->claim_no) ?></td>
                        <td class="font-w300"><?= $claim->has('policy') ? $this->Html->link($claim->policy->name, ['controller' => 'Policies', 'action' => 'view', $claim->policy->id]) : '' ?></td>
                        <td class="font-w300"><?= $claim->has('vehicle') ? $this->Html->link($claim->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $claim->vehicle->id]) : '' ?></td>
                        <td class="font-w300"><?= $claim->has('garage') ? $this->Html->link($claim->garage->name, ['controller' => 'Garages', 'action' => 'view', $claim->garage->id]) : '' ?></td>
                        <td class="hidden-xs"><?= h($claim->date_reported) ?></td>
                        <td class="hidden-xs"><?= h($claim->accident_date) ?></td>
                                                                                         
                        <td class="text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $claim->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Claim Details','escape'=>false]) ?>
                                
                                <?= $this->Html->link(__('<i class="fa fa-pencil"></i>'), ['action' => 'edit', $claim->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'Edit Claim Details','escape'=>false]) ?>
                                                                                         
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <!--div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div-->
            
        </div>
    </div>
</div>
<!-- End Page Content -->