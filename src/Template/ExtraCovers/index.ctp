<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExtraCover[]|\Cake\Collection\CollectionInterface $extraCovers
 */
?>


<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                
                <div class="btn-group"> 
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                            ['controller' => 'CoverTypes', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;VEHICLES'), 
                        ['controller' => 'Vehicles', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Vehicles','escape'=>false]) ?>
                                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                            ['controller' => 'Policies', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Vehicles</li>
                    <li><a class="link-effect" href="">Additional Covers</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Extra Cover's <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <!--div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <?= 
                    $this->Html->link(__('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Excel'), 
                            ['controller' => 'Garages', 'action' => 'indexls', '_ext'=>'xlsx'],
                                ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'Download as excel','escape'=>false]); 
                ?>
            </ol>
        </div-->
        <!-- End Excel Button -->
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 7%;"></th>    
                        <th class="hidden-sm">Cover Type</th>    
                        <th class="hidden-sm">Vehicle</th> 
                        <th class="hidden-sm">Amount</th> 
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($extraCovers as $extraCover): ?>
                    <tr>
                        <td class="text-center"><?= $this->Number->format($extraCover->id) ?></td>
                        <td class="hidden-xs">
                            <?= $extraCover->has('cover_type') ? $this->Html->link($extraCover->cover_type->name, ['controller' => 'CoverTypes', 'action' => 'view', $extraCover->cover_type->id]) : '' ?>
                        </td>
                        <td class="hidden-xs"><?= $extraCover->has('vehicle') ? $this->Html->link($extraCover->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $extraCover->vehicle->id]) : '' ?></td>
                        <td class="hidden-xs"><?= $this->Number->format($extraCover->amount) ?></td>
                        
                        <td class="text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $extraCover->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Garage Details','escape'=>false]) ?>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <!--div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div-->
            
        </div>
    </div>
</div>
<!-- End Page Content -->
