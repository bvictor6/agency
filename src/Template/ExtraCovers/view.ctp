<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExtraCover $extraCover
 */
?>


<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $extraCover->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Extra Cover Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;VEHICLES'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Extra Covers','escape'=>false]) ?>
                
            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Underwriting</li>
                <li> <?= $this->Html->link('Extra Covers', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($extraCover->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title"></h3>
    </div>
    <div class="block-content">
        <div class="row items-push">         
            <div class="col-sm-6">
                <table class="table">
                    <tr>
                        <td class="hidden-xs"><?= __('Vehicle') ?></th>
                        <td class="hidden-xs"><?= $extraCover->has('vehicle') ? $this->Html->link($extraCover->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $extraCover->vehicle->id]) : '' ?></td>
                    </tr>        
                    <tr>
                        <td class="hidden-xs"><?= __('Cover Type') ?></th>
                        <td class="hidden-xs">
                            <?= $extraCover->has('cover_type') ? $this->Html->link($extraCover->cover_type->name, ['controller' => 'CoverTypes', 'action' => 'view', $extraCover->cover_type->id]) : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="hidden-xs"><?= __('Amount') ?></th>
                        <td class="hidden-xs"><?= $this->Number->format($extraCover->amount) ?></td>
                    </tr>        
                </table>
            </div> 
        </div>
        <!-- End of main content -->
        
    </div>
</div>
<!-- End Paragraphs in Block -->
