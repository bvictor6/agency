<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Expense[]|\Cake\Collection\CollectionInterface $expenses
 */
?>

<!-- Page Content -->
<div>
    <!-- Dynamic Table Full -->
    <div class="columns large-12">
         <h3 class="block-title">Expenses <small><?= $from ?> TO <?= $to ?></small></h3>
        
        <div>
            <table>
                <thead>
                    <tr>
                        <th class="hidden-sm">Expense</th>
                        <th class="hidden-sm">Date</th>
                        <th class="hidden-sm">Amount</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($expenses as $expense): ?>
                    <tr> 
                        <td class="hidden-sm"><?= h($expense->name) ?></td>
                        <td class="hidden-sm"><?= h($expense->expense_date) ?></td>
                        <td class="hidden-sm"><?= $this->Number->format($expense->amount) ?></td>
                                                                                                                       
                        
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->