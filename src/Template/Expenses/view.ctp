<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Expense $expense
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $expense->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Expense Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;EXPENSES'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Expenses','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create Expense','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>MISC</li>
                <li> <?= $this->Html->link('Expense', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($expense->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title"><?= h($expense->name) ?></h3>
    </div>
    <div class="block-content">
        <div class="row items-push">  
            <div class="col-sm-6">
                <table class="vertical-table">
                    <tr>
                        <td><?= __('Amount') ?></td>
                        <td><?= $this->Number->format($expense->amount) ?></td>
                    </tr>
                    <tr>
                        <td><?= __('Expense Date') ?></td>
                        <td><?= h($expense->expense_date) ?></td>
                    </tr>

                </table>
            </div>
            <div class="col-sm-6">
                <h5><?= __('Description') ?></h5>
                <?= $this->Text->autoParagraph(h($expense->remarks)); ?>
            </div> 
        </div>
        <!-- End of main content -->
    </div>
</div>
<!-- End Paragraphs in Block -->
