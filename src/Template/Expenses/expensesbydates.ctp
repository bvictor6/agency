<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Expense[]|\Cake\Collection\CollectionInterface $expenses
 */
?>

<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                &nbsp;
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Reports</li>
                    <li><a class="link-effect" href="">Expenses Report</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Expenses <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <?= $this->Html->link(__('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF'), 
                        ['action' => 'expensesbydates',$from,$to,'R_','_ext' => 'pdf'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Export PDF','escape'=>false]) ?>
            </ol>
        </div>
        <!-- End Excel Button -->
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 7%;"></th>                        
                        <th class="hidden-sm">Expense</th>
                        <th class="hidden-sm">Date</th>
                        <th class="hidden-sm">Amount</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($expenses as $expense): ?>
                    <tr> 
                        <td class="text-center"><?= $this->Number->format($expense->id) ?></td>
                        <td class="font-w300"><?= h($expense->name) ?></td>
                        <td class="font-w300"><?= h($expense->expense_date) ?></td>
                        <td class="font-w300"><?= $this->Number->format($expense->amount) ?></td>
                                                                                                                       
                        <td class="text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $expense->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Expense Details','escape'=>false]) ?>
                                                       
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <!--div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div-->
            
        </div>
    </div>
</div>
<!-- End Page Content -->