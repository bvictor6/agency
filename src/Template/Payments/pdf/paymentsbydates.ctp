<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payment[]|\Cake\Collection\CollectionInterface $payments
 */
?>

<!-- Page Content -->
<div class="row">    
    <div class="columns large-12">
        <h3 class="block-title">Payments <small><?= $from ?> To <?= $to ?></small></h3>
        
        <div>
            <table>
                <thead>
                    <tr>                        
                        <th class="font-s12">Policy#</th>
                        <th class="font-s12">Vehicle</th>
                        <th class="font-s12">Amount</th>
                        <th class="font-s12">Reference</th>                       
                        <th class="font-s12">Method</th>
                        <th class="font-s12">Date</th>
                        <th class="font-s12">Insurance</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($payments as $payment): ?>
                    <tr>
                        <td class="font-s12"><?= $payment->has('policy') ? $this->Html->link($payment->policy->name, ['controller' => 'Policies', 'action' => 'view', $payment->policy->id]) : '' ?></td>
                        <td class="font-s12"><?= $payment->has('vehicle') ? $this->Html->link($payment->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $payment->vehicle->id]) : '' ?></td>
                        <td class="font-s12"><?= $this->Number->format($payment->amount) ?></td>
                        <td class="font-s12"><?= h($payment->transaction_ref) ?></td>
                        <td class="font-s12"><?= h($payment->payment_method) ?></td>
                        <td class="font-s12"><?= date('Y-M-d',  strtotime($payment->payment_date)) ?></td>
                        <td class="font-s12"><?= $payment->has('policy') ? $this->Html->link($payment->policy->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $payment->policy->insurance_firm->id]) : '' ?></td>
                        
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->

