<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payment $payment
 */
?>

<?= $this->Html->script('jquery-3.2.1.min.js'); ?>
<?= $this->Html->script('bootstrap-datepicker.min.js'); ?>
<?= $this->Html->css('bootstrap-datepicker3.css'); ?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <!--h3 class="h3 text-danger animated zoomIn"><i class="fa fa-plus"></i>&nbsp;Users</h3-->

            <div class="btn-group">    
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;PAYMENTS'), 
                        ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Payments','escape'=>false]) ?>

                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                        ['controller' => 'CoverTypes', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                        ['controller' => 'InsuranceFirms', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                        ['controller' => 'Policies', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Underwriting</li>
                <li><?= $this->Html->link('Payments',['action'=>'index'] ,['class'=>'link-effect']) ?></li>
                <li><?= $this->Html->link('New',['action'=>'add'] ,['class'=>'link-effect']) ?></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Form -->
<div class="block block-bordered">
    <div class="block-header bg-gray-lighter">
        <ul class="block-options">
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
        </ul>
        <h3 class="block-title">Payment <small>&nbsp;NEW</small></h3>
    </div>
    <div class="block-content"> 
        <?= $this->Form->create($payment,["autocomplete"=>"off"])  ?>
        <fieldset>
            <div class="col-sm-6"><?php echo $this->Form->control('policy_id', ['options' => $policies]); ?></div>            
            <div class="col-sm-6"><?php echo $this->Form->control('vehicle_id', ['options' => $vehicles]);  ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('amount');  ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('transaction_ref');  ?></div>            
            <div class="col-sm-6">
                <?php echo $this->Form->control('payment_method', ['options'=>[
                    'Cash'=>'Cash', 'Cheque'=>'Cheque','Mpesa'=>'Mpesa','Visa'=>'Visa','Other'=>'Other']]);  ?>
            </div>
            <div class="col-sm-6"><?php echo $this->Form->control('payment_date',['label'=>'Payment Date','type'=>'text','placeholder'=>'YYYY-MM-DD','required']); ?></div>
            <div class="col-sm-12"><?php echo $this->Form->control('remarks');  ?></div>
                        
        </fieldset>
        <?= $this->Form->button(__("&nbsp;&nbsp;Save Payment Details"), 
                ['class'=>'btn btn-md btn-danger fa fa-save','data-action'=>'refresh_toggle',
                    'data-toggle'=>'block-option']) ?>
        <?= $this->Form->end() ?>        
        <div>&nbsp;</div>
    </div>
</div>
<!-- End Form -->

<script>
    $(document).ready(function()
    {
      var date_input=$('input[name="payment_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
        
</script>

