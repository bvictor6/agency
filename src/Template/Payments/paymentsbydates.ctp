<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payment[]|\Cake\Collection\CollectionInterface $payments
 */
?>

<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                
                <div class="btn-group">                     
                    &nbsp;
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Reports</li>
                    <li><a class="link-effect" href="">Payments</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Payments <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <?= $this->Html->link(__('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF'), 
                        ['action' => 'paymentsbydates',$from,$to,'R_','_ext' => 'pdf'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Export PDF','escape'=>false]) ?>
            </ol>
        </div>
        <!-- End Excel Button -->
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>                        
                        <th class="hidden-sm">Policy#</th>
                        <th class="hidden-sm">Vehicle</th>
                        <th class="hidden-sm">Amount</th>
                        <th class="hidden-sm">Reference</th>                       
                        <th class="hidden-sm">Method</th>
                        <th class="hidden-sm">Date</th>
                        <th class="hidden-sm">Insurance</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($payments as $payment): ?>
                    <tr>
                        <td class="font-w300"><?= $payment->has('policy') ? $this->Html->link($payment->policy->name, ['controller' => 'Policies', 'action' => 'view', $payment->policy->id]) : '' ?></td>
                        <td class="font-w300"><?= $payment->has('vehicle') ? $this->Html->link($payment->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $payment->vehicle->id]) : '' ?></td>
                        <td class="hidden-xs"><?= $this->Number->format($payment->amount) ?></td>
                        <td class="hidden-xs"><?= h($payment->transaction_ref) ?></td>
                        <td class="hidden-xs"><?= h($payment->payment_method) ?></td>
                        <td class="hidden-xs"><?= date('Y-M-d',  strtotime($payment->payment_date)) ?></td>
                        <td class="font-w300"><?= $payment->has('policy') ? $this->Html->link($payment->policy->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $payment->policy->insurance_firm->id]) : '' ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $payment->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Payment Details','escape'=>false]) ?>
                                                                                         
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->

