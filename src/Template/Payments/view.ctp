<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payment $payment
 */
?>


<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;PAYMENTS'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Payments','escape'=>false]) ?>
                    
                

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Underwriting</li>
                <li> <?= $this->Html->link('Payments', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($payment->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Details</h3>
    </div>
    <div class="block-content">
        <div class="row items-push">
            <div class="col-sm-6">
                
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600"><?= __('Policy') ?></th>
                            <td class="font-w400"><?= $payment->has('policy') ? $this->Html->link($payment->policy->name, ['controller' => 'Policies', 'action' => 'view', $payment->policy->id]) : '' ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Vehicle') ?></th>
                            <td class="font-w400"><?= $payment->has('vehicle') ? $this->Html->link($payment->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $payment->vehicle->id]) : '' ?></td>
                        </tr>
                        
                        <tr>
                            <td class="font-w600"><?= __('Transaction Ref') ?></td>
                            <td class="font-w600"><?= h($payment->transaction_ref) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Payment Method') ?></td>
                            <td class="font-w600"><?= h($payment->payment_method) ?></td>
                        </tr>
                        
                        <tr>
                            <td class="font-w600"><?= __('Amount') ?></th>
                            <td class="font-w600"><?= $this->Number->format($payment->amount) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Payment Date') ?></th>
                            <td class="font-w600"><?= date('Y-M-d',  strtotime($payment->payment_date)) ?></td>
                        </tr>
 
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6">
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600">&nbsp;</th>
                            <td class="font-w400">&nbsp;</td>
                        </tr>                                                 
                    </tbody>
                </table>
            </div>
            <div class="col-sm-12">
                <h4><?= __('Description') ?></h4>
                <?= $this->Text->autoParagraph(h($payment->remarks)); ?>
            </div>
        </div>
    </div>
</div>
<!-- End Paragraphs in Block -->
