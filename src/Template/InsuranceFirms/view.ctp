<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\InsuranceFirm $insuranceFirm
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $insuranceFirm->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Insurance Firm Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;INSURANCE FIRMS'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create New Insurance Firm','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Settings</li>
                <li> <?= $this->Html->link('Insurance Firms', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($insuranceFirm->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title"><?= h($insuranceFirm->name) ?></h3>
    </div>
    <div class="block-content">
        <div class="row items-push">
            
                    
            <div class="col-sm-6">
                <h5>Contacts</h5>
                <ul>
                    <li class="fa fa-envelope-o">&nbsp;&nbsp;<?= h($insuranceFirm->email) ?></li>                    
                </ul>
                <ul>
                    <li class="fa fa-phone-square">&nbsp;&nbsp;<?= h($insuranceFirm->tel) ?></li>
                </ul>
            </div>
            <div class="col-sm-6">
                <h5>Location</h5>
                <ul>
                    <li class="fa fa-building-o">&nbsp;&nbsp;<?= h($insuranceFirm->location) ?></li>
                </ul>

            </div>
            
            <div class="row">               
                <hr/>
            </div>            
            <div class="col-sm-12">
                <h5><?= __('Address') ?></h5>
                <?= $this->Text->autoParagraph(h($insuranceFirm->address)); ?>
            </div>  
            
        </div>
    </div>
</div>
<!-- End Paragraphs in Block -->
