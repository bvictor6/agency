<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CoverType $coverType
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $coverType->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Cover Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;COVER TYPES'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create New Cover','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Settings</li>
                <li> <?= $this->Html->link('Cover Types', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($coverType->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title"><?= h($coverType->name) ?></h3>
    </div>
    <div class="block-content">
        <div class="row items-push">           
            <div class="col-sm-6">
                <h5>LOB</h5>
                <?= $coverType->has('line_of_business') ? $this->Html->link($coverType->line_of_business->name, ['controller' => 'LineOfBusinesses', 'action' => 'view', $coverType->line_of_business->id]) : '' ?>                 
                </ul> 
            </div>                           
            <div class="col-sm-6">
                <h5><?= __('Description') ?></h5>
                <?= $this->Text->autoParagraph(h($coverType->description)); ?>
            </div> 
        </div>
        <!-- End of main content -->
        
        <!-- Start related stuff  -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Risks</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($coverType->vehicles)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="hidden-sm"><?= __('Reg#') ?></th>
                                <th class="hidden-sm"><?= __('Model') ?></th>
                                <th class="hidden-sm"><?= __('Make') ?></th>
                                <th class="hidden-sm"><?= __('Effective Date') ?></th>
                                <th class="hidden-sm"><?= __('Expiry Date') ?></th>
                                
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($coverType->vehicles as $vehicles): ?>
                            <tr>
                                <td class="hidden-xs"><?= h($vehicles->registration_number) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->model) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->make) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->effective_date) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->expiry_date) ?></td>
                                                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Vehicles', 'action' => 'view', $vehicles->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Vehicle Details','escape'=>false]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
       
        <!-- End related stuff -->  
    </div>
</div>
<!-- End Paragraphs in Block -->