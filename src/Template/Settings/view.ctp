<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<?= $this->Html->css('base.css') ?>
<?= $this->Html->css('style.css') ?>
<?= $this->Html->css('home.css') ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
 
<body class="home">
    <div class="block">
        <div class="block-header col-sm-7">
            <h3 class="block-title"><?= h($setting->name) ?> <small>Details</small></h3>
        </div>
        
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <div class="btn-group">
                        <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                                ['action' => 'edit', $setting->id],
                                ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                                'data-toggle'=>'tooltip', 'title'=>'Edit Master Details','escape'=>false]) ?>
                </div>
            </ol>
        </div>
        
        <!-- End Excel Button -->
    </div>
    
    <div class="row">        
        <div class="columns large-6">
            <h5>Contacts</h5>
            <ul>
                <li><i class="fa fa-phone"></i>&nbsp;&nbsp;<?= h($setting->phone) ?></li>
                <li><i class="fa fa-inbox"></i>&nbsp;&nbsp;<?= h($setting->email) ?></li>
            </ul>
        </div>
        <div class="columns large-6">
            <h5>Location</h5>
            <ul>
                <li><i class="fa fa-building"></i>&nbsp;&nbsp;<?= h($setting->building) ?></li>
                <li><i class="fa fa-street-view"></i>&nbsp;&nbsp;<?= h($setting->street) ?></li>
            </ul>
            
        </div>
    </div>
    
    <div class="row">
        <div class="columns large-12 text-center">
            <h6 class="more">More Information</h6>
            <ul>
                <li><?= h($setting->address) ?></li>
                <li><?= h($setting->city) ?></li>
            </ul>
        </div>
        <hr/>
    </div>
</body>
