<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

$cakeDescription = '';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    
    
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="js/plugins/slick/slick.min.css">
    <link rel="stylesheet" href="js/plugins/slick/slick-theme.min.css">

    <!-- OneUI CSS framework -->
    <link rel="stylesheet" id="css-main" href="css/oneui.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <link rel="stylesheet" id="css-theme" href="css/themes/city.min.css"> 
    <!-- END Stylesheets -->
        
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>

  <body>
      
        <!-- Page Container -->
        <!--
            Available Classes:

            'sidebar-l'                  Left Sidebar and right Side Overlay
            'sidebar-r'                  Right Sidebar and left Side Overlay
            'sidebar-mini'               Mini hoverable Sidebar (> 991px)
            'sidebar-o'                  Visible Sidebar by default (> 991px)
            'sidebar-o-xs'               Visible Sidebar by default (< 992px)

            'side-overlay-hover'         Hoverable Side Overlay (> 991px)
            'side-overlay-o'             Visible Side Overlay by default (> 991px)

            'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

            'header-navbar-fixed'        Enables fixed header
        -->
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
           

            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                            <div class="btn-group pull-right">
                                <button class="btn btn-link text-gray dropdown-toggle" data-toggle="dropdown" type="button">
                                    <i class="si si-drop"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right font-s13 sidebar-mini-hide">
                                    <li>
                                        <a data-toggle="theme" data-theme="default" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-default pull-right"></i> <span class="font-w600">Default</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="assets/css/themes/amethyst.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-amethyst pull-right"></i> <span class="font-w600">Amethyst</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="assets/css/themes/city.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-city pull-right"></i> <span class="font-w600">City</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="assets/css/themes/flat.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-flat pull-right"></i> <span class="font-w600">Flat</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="assets/css/themes/modern.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-modern pull-right"></i> <span class="font-w600">Modern</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="assets/css/themes/smooth.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-smooth pull-right"></i> <span class="font-w600">Smooth</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <a class="h5 text-white" href="index.html">
                                <i class="fa fa-circle-o-notch text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide"></span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                                <li>
                                    <a class="active" href="/agency">
                                        <i class="si si-speedometer"></i>
                                        <span class="sidebar-mini-hide">Dashboard</span>
                                    </a>

                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Home</span></li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide">Settings</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Master Details', ['controller'=>'Settings','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Insurance Firms', ['controller'=>'InsuranceFirms','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("LOB's", ['controller'=>'LineOfBusinesses','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Covers', ['controller'=>'CoverTypes','action'=>'index']) ?>
                                        </li> 
                                        <li>
                                            <?= $this->Html->link('Categories', ['controller'=>'Categories','action'=>'index']) ?>
                                        </li> 
                                    </ul>
                                </li>
                                
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-wrench"></i><span class="sidebar-mini-hide">Setup</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Document Types', ['controller'=>'DocumentTypes','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Garages",['controller'=>'Garages','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Vehicle Models",['controller'=>'Models','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Vehicle Makes", ['controller'=>'Makes','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Colors", ['controller'=>'Colors','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-book-open"></i><span class="sidebar-mini-hide">Underwriting</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Clients', ['controller'=>'Clients', 'action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Policies', ['controller'=>'Policies','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Claims', ['controller'=>'Claims','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Payments', ['controller'=>'Payments','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">More++</span></li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Certificates</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Batches', ['controller'=>'Batches','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Available Certificates', ['controller'=>'Certificates','action'=>'index',0]) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Printed Certificates', ['controller'=>'Certificates','action'=>'index',1]) ?>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Reports</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Reports [ALL]', ['controller'=>'Reports','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Reports By Type', ['controller'=>'ReportTypes','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Misc</span></a>
                                    <ul>                                      
                                        <li>
                                            <?= $this->Html->link('Expenses', ['controller'=>'Expenses','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Reminders', ['controller'=>'Reminders','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Admin</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Users', ['controller'=>'Users','action'=>'index']) ?>
                                        </li>
                                        <!--li>
                                            <?= $this->Html->link('Backup DB', ['controller'=>'Users','action'=>'backupdb']) ?>
                                        </li-->
                                    </ul>
                                </li>
                                
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
                <!-- Header Navigation Right -->
                <ul class="nav-header pull-right">
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                                <i class="fa fa-user"></i>&nbsp;&nbsp;
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php
                                    $user = $this->request->getSession()->read('Auth.User'); 
                                    if(isset($user))
                                    {
                                ?>
                                        <li class="dropdown-header">Profile</li> 
                                        <li>
                                            <a tabindex="-1" href="#">
                                                <i class="si si-user pull-right"></i>
                                                <?= $user['name'] ?>
                                            </a>
                                        </li>
                                        <li>                                        
                                            <a tabindex="-1" href="#">
                                                <i class="si si-envelope pull-right"></i>
                                                <?= $user['email'] ?>
                                            </a>
                                        </li>
                                        
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Actions</li>
                                        
                                        <li>                                            
                                            <?= $this->Html->link(__('<i class="si si-shuffle pull-right"></i>&nbsp;&nbsp; Change Password'), 
                                                    ['controller'=>'Users','action'=>'changepwd',$user['id']],
                                                    ['tabindex'=>'-1','escape'=>false]) 
                                            ?>
                                        </li>
                                        <li>
                                            <a tabindex="-1" href="/agency/Users/logout">
                                                <i class="si si-logout pull-right"></i>Sign out
                                            </a>
                                        </li>

                                <?php } else {?>
                                        <li class="dropdown-header">Profile</li>                                
                                        <li>                                            
                                            <?= $this->Html->link(__('<i class="si si-login pull-right"></i>&nbsp;&nbsp; Sign In'), 
                                                    ['controller'=>'Users','action'=>'login'],
                                                    ['tabindex'=>'-1','escape'=>false]) 
                                            ?>
                                        </li>
                                <?php }?>
                            </ul>
                        </div>
                    </li>
                    
                </ul>
                <!-- END Header Navigation Right -->

                <!-- Header Navigation Left -->
                <ul class="nav-header pull-left">
                    <li class="hidden-md hidden-lg">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                            <i class="fa fa-navicon"></i>
                        </button>
                    </li>
                    <li class="hidden-xs hidden-sm">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                    </li>
                    
                </ul>
                <!-- END Header Navigation Left -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <?= $this->Flash->render() ?>
                <?= $this->Flash->render('auth') ?>
                <!-- Page Header -->
                <div class="content bg-image overflow-hidden" style="background-image: url('assets/img/photos/bg.jpg');">
                    <div class="push-50-t push-15">
                        <?= $this->Cell('Dashboard::companyName') ?>
                        <h2 class="h5 text-white-op animated zoomIn">
                            Welcome 
                            <?php
                                if(isset($user))
                                {
                                    echo $user['name'];
                                }
                            ?>
                        </h2>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Stats -->
                <div class="content bg-white border-b">
                    <div class="row items-push text-uppercase">
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker animated fadeIn">Firms</div>
                            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Total Insurance Firms</small></div>
                            <?= $this->cell('Dashboard::totalFirms') ?>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker animated fadeIn">LOB's</div>
                            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Total LOB's</small></div>
                            <?= $this->cell('Dashboard::totallob') ?>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker animated fadeIn">Policies</div>
                            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Total Policies</small></div>
                            <?= $this->cell('Dashboard::totalPolicies') ?>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker animated fadeIn">Clients</div>
                            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Total Clients</small></div>
                            <?= $this->cell('Dashboard::totalClients') ?>
                        </div>
                    </div>
                </div>
                <!-- END Stats -->

                <!-- Page Content -->
                <div class="content">
                    <div class="row">
                        <div class="col-lg-8">
                            <!-- Main Dashboard Chart -->
                            <div class="block">
                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Week Overview</h3>
                                </div>
                                <div class="block-content block-content-full bg-gray-lighter text-center">
                                    <!-- Chart.js Charts (initialized in js/pages/base_pages_dashboard.js), for more examples you can check out http://www.chartjs.org/docs/ -->
                                    <div style="height: 374px;">
                                        <?= $this->cell('Dashboard'); ?>
                                    </div>
                                </div>
                                <div class="block-content text-center">
                                    <div class="row items-push text-center">
                                        <div class="col-xs-6 col-lg-3">
                                            <div class="push-10"><i class="si si-graph fa-2x"></i></div>
                                            <?= $this->cell('Dashboard::policiesLastWeek') ?>
                                        </div>
                                        <div class="col-xs-6 col-lg-3">
                                            <div class="push-10"><i class="si si-users fa-2x"></i></div>
                                            <?= $this->cell('Dashboard::clientsLastWeek') ?>
                                        </div>
                                        <div class="col-xs-6 col-lg-3 visible-lg">
                                            <div class="push-10"><i class="si si-star fa-2x"></i></div>
                                            <?= $this->cell('Dashboard::certsLastWeek') ?>
                                        </div>
                                        <div class="col-xs-6 col-lg-3 visible-lg">
                                            <div class="push-10"><i class="fa fa-car fa-2x"></i></div>
                                            <?= $this->cell('Dashboard::vehiclesLastWeek') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?= $this->cell('Dashboard') ?>
                            <!-- END Main Dashboard Chart -->
                        </div>
                        <div class="col-lg-4">
                            <!-- Latest Sales Widget -->
                            <div class="block">
                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Latest Sales</h3>
                                </div>
                                <div class="block-content bg-gray-lighter">
                                    <div class="row items-push">
                                        <div class="col-xs-4">
                                            <div class="text-muted"><small><i class="si si-calendar"></i> 24 hrs</small></div>
                                            <?= $this->cell('Dashboard::last24hrs') ?>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="text-muted"><small><i class="si si-calendar"></i> 7 days</small></div>
                                            <?= $this->cell('Dashboard::lastweek') ?>
                                        </div>
                                        <div class="col-xs-2 h1 font-w300 text-right">&nbsp;</div>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="pull-t pull-r-l">
                                        <!-- Slick slider (.js-slider class is initialized in App() -> uiHelperSlick()) -->
                                        <!-- For more info and examples you can check out http://kenwheeler.github.io/slick/ -->
                                        <div class="js-slider remove-margin-b" data-slider-autoplay="true" data-slider-autoplay-speed="2500">
                                              <?= $this->cell('Dashboard::latestPolicies') ?>                                          
                                        </div>
                                        <!-- END Slick slider -->
                                    </div>
                                </div>
                            </div>
                            <!-- END Latest Sales Widget -->
                        </div>
                    </div>
                    
                        
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <!--footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-right">
                    Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="javascript:void(0)" target="_blank">VicSoft</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="javascript:void(0)" target="_blank">Agency+ 1.0</a> &copy; <span class="js-year-copy"></span>
                </div>
            </footer-->
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Apps Modal -->
        <!-- Opens from the button in the header -->
        <div class="modal fade" id="apps-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-sm modal-dialog modal-dialog-top">
                <div class="modal-content">
                    <!-- Apps Block -->
                    <div class="block block-themed block-transparent">
                        <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Apps</h3>
                        </div>
                        <div class="block-content">
                            <div class="row text-center">
                                <div class="col-xs-6">
                                    <a class="block block-rounded" href="index.html">
                                        <div class="block-content text-white bg-default">
                                            <i class="si si-speedometer fa-2x"></i>
                                            <div class="font-w600 push-15-t push-15">Backend</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-6">
                                    <a class="block block-rounded" href="frontend_home.html">
                                        <div class="block-content text-white bg-modern">
                                            <i class="si si-rocket fa-2x"></i>
                                            <div class="font-w600 push-15-t push-15">Frontend</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Apps Block -->
                </div>
            </div>
        </div>
        <!-- END Apps Modal -->      
    
        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="js/core/jquery.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/core/jquery.slimscroll.min.js"></script>
        <script src="js/core/jquery.scrollLock.min.js"></script>
        <script src="js/core/jquery.appear.min.js"></script>
        <script src="js/core/jquery.countTo.min.js"></script>
        <script src="js/core/jquery.placeholder.min.js"></script>
        <script src="js/core/js.cookie.min.js"></script>
        <script src="js/app.js"></script>

        <!-- Page Plugins -->
        <script src="js/plugins/slick/slick.min.js"></script>
        <script src="js/plugins/chartjs/Chart.min.js"></script>

        <!-- Page JS Code -->
        
        <script>
            $(function () {
                // Init page helpers (Slick Slider plugin)
                App.initHelpers('slick');
            });
        </script>

  </body>
</html>
