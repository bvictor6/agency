<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Certificate[]|\Cake\Collection\CollectionInterface $certificates
 */
?>

<?php 
    $batchNo = null;
    $firm = null;
    $cat = null;
    $fromTo = null;
    $rcvd = null;
    foreach ($details as $detail):
        $firm = $detail->insurance_firm->name;
        $cat = $detail->category->name;
        $batchNo = $detail->batch_no;
        $rcvd = $detail->date_received;
        $fromTo = $detail->batch_from." To ".$detail->batch_to;
    endforeach; 
?>

<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                <div class="btn-group">                     
                    &nbsp;
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>REPORTS</li>
                    <li><a class="link-effect" href="">Certificates By Batch</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Certificates <small>&Tab;Batch:&nbsp;&nbsp;<?= strtoupper($all)." :: ".strtoupper($firm) ?></small></h3>
            <h3 class="block-title"><small><?= strtoupper($cat) ?>&Tab;[<?= $fromTo ?>]&Tab;&Tab;Received:&nbsp;&nbsp;<?= date('Y-M-d', strtotime($rcvd)) ?></small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <?= $this->Html->link(__('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF'), 
                        ['action' => 'batchcerts',$all,'R_','_ext' => 'pdf'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Export PDF','escape'=>false]) ?>
            </ol>
        </div>
        <!-- End Excel Button -->  
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="font-s12">Serial#</th>
                        <th class="font-s12">Reg#</th>
                        <th class="font-s12">Policy#</th>
                        <th class="font-s12">Date</th>
                        <th class="font-s12">Approved</th>
                        <th class="font-s12">Printed</th>
                        <th class="font-s12 text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($certificates as $certificate): ?>
                    <tr>
                        <td class="font-s12"><?= ($certificate->serial_no) ?></td>
                        <td class="font-s12"><?= $certificate->has('vehicle') ? $this->Html->link($certificate->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $certificate->vehicle->id]) : '' ?></td>
                        <td class="font-s12">
                            <?php 
                                //if($all=='No')
                                {
                                    if($certificate->vehicle->has('policy'))
                                    {
                                        echo $this->Html->link($certificate->vehicle->policy->name, 
                                        ['controller' => 'Policies', 'action' => 'view', 
                                        $certificate->vehicle->policy->id]);                                        
                                    } else{ echo '';}                                    
                                }
                                //else {}
                                                                
                            ?>
                        </td>
                        <td class="font-s12"><?= date('Y-M-d',  strtotime($certificate->modified)) ?></td>
                        <td class="font-s12">
                            <?php 
                                if($certificate->status){
                                    echo "<span class='label label-success fa fa-check-circle'>  Yes</span>";
                                } else {
                                    echo "<span class='label label-danger fa fa-times-circle'>  No</span>";
                                }
                            ?>
                        </td>
                        <td class="font-s12">
                            <?php 
                                if($certificate->printed){
                                    echo "<span class='label label-success fa fa-check-circle'>  Yes</span>";
                                } else {
                                    echo "<span class='label label-danger fa fa-times-circle'>  No</span>";
                                }
                            ?>
                        </td>
                                                                                               
                        <td class="font-s12 text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $certificate->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Certificate Details','escape'=>false]) ?>
                                                       
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <!--div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div-->
            
        </div>
    </div>
</div>
<!-- End Page Content -->