<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Certificate $certificate
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-print"></i>&nbsp;&nbsp;PRINT'), 
                        ['action' => 'staging', $certificate->id,
                            ($certificate->vehicle_id!=null)?$certificate->vehicle_id:0,
                            date('Y-M-d',  strtotime($certificate->date_from)),
                            date('Y-M-d',  strtotime($certificate->date_to)),
                            $certificate->serial_no],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Print Certificate','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;CERTIFICATES'), ['action' => 'index',0],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Certificates','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Generate Certifcates','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>MISC</li>
                <li> <?= $this->Html->link('Certificates', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($certificate->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">
            Serial#:&nbsp;&nbsp;<?= h($certificate->serial_no) ?>
            <small>
                &nbsp;&nbsp;
                <?php if($certificate->printed){ ?>
                <span class="fa fa-lock label label-warning">&nbsp;&nbsp;Printed</span>
                <?php } else { ?>
                    <span class="fa fa-unlock-alt label label-success">&nbsp;&nbsp;Not Printed</span>
                <?php } ?>
            </small>
        </h3>
    </div>
    <div class="block-content">
        <div class="row items-push">            
                    
            <div class="col-sm-4">
                <h5>Batch No</h5>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $certificate->batch_no ?></li>                    
                </ul>
                <h5>Insurance Firm</h5>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $certificate->has('insurance_firm') ? $this->Html->link($certificate->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $certificate->insurance_firm->id]) : '' ?></li>                    
                </ul>
            </div>
            <div class="col-sm-4">
                <h5>Vehicle Reg#</h5>
                <ul>                    
                    <li class="fa fa-caret-right">
                        &nbsp;&nbsp;<?= $certificate->has('vehicle') ? $this->Html->link($certificate->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $certificate->vehicle->id]) : '' ?>
                    </li>
                </ul>
                <h5>Status</h5>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;
                        <?php if($certificate->status){ ?>
                        <span class="fa fa-check-circle-o label label-success">&nbsp;&nbsp;Approved</span>
                        <?php } else { ?>
                            <span class="si si-close label label-danger">&nbsp;&nbsp;Not Approved</span>
                        <?php } ?>
                    </li>
                </ul>
            </div>
              
            <div class="col-sm-4">
                <h5>Category</h5>
                <ul>                    
                    <li class="fa fa-caret-right">
                        &nbsp;&nbsp;<?= $certificate->has('category') ? $this->Html->link($certificate->category->name, ['controller' => 'Categories', 'action' => 'view', $certificate->category->id]) : '' ?>
                    </li>
                </ul>
                <h5>Period</h5>
                <ul>                    
                    <li class="fa fa-caret-right">
                        &nbsp;&nbsp;<?= date('Y-M-d',  strtotime($certificate->date_from))." to ".date('Y-M-d',  strtotime($certificate->date_to)); ?>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
</div>
<!-- End Paragraphs in Block -->
