<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Certificate[]|\Cake\Collection\CollectionInterface $certificates
 */
?>

<!-- Page Content -->
<div class="row">
    <div class="columns large-12">
            <h3 class="block-title">Certificates <small>By Firm</small></h3>
        <div>
            <table>
                <thead>
                    <tr>
                        <th class="font-s12">Serial#</th>
                        <th class="font-s12">Batch#</th>
                        <th class="font-s12">Reg#</th>
                        <th class="font-s12">Policy#</th>
                        <th class="font-s12">Insurance</th>
                        <th class="font-s12">Date</th>
                        <th class="font-s12">Approved</th>
                        <th class="font-s12">Printed</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($certificates as $certificate):  ?>
                    <tr>
                        <td class="font-s12"><?= ($certificate->serial_no) ?></td>
                        <td class="font-s12"><?= ($certificate->batch_no) ?></td>
                        <td class="font-s12"><?= $certificate->has('vehicle') ? $this->Html->link($certificate->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $certificate->vehicle->id]) : '' ?></td>
                        <td class="font-s12">
                            <?php 
                                if($all=='No')
                                {
                                    if($certificate->vehicle->has('policy'))
                                    {
                                        echo $this->Html->link($certificate->vehicle->policy->name, 
                                        ['controller' => 'Policies', 'action' => 'view', 
                                        $certificate->vehicle->policy->id]);                                        
                                    } else{ echo '';}                                    
                                }
                                else {}
                                                                
                            ?>
                        </td>
                        <td class="font-s12"><?= $certificate->has('insurance_firm') ? $this->Html->link($certificate->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $certificate->insurance_firm->id]) : '' ?></td>
                        <td class="font-s12"><?= date('Y-M-d',  strtotime($certificate->date_received)) ?></td>
                        <td class="font-s12">
                            <?php 
                                if($certificate->status){
                                    echo "<span class='label label-success fa fa-check-circle'>  Yes</span>";
                                } else {
                                    echo "<span class='label label-danger fa fa-times-circle'>  No</span>";
                                }
                            ?>
                        </td>
                        <td class="font-s12">
                            <?php 
                                if($certificate->printed){
                                    echo "<span class='label label-success fa fa-check-circle'>  Yes</span>";
                                } else {
                                    echo "<span class='label label-danger fa fa-times-circle'>  No</span>";
                                }
                            ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    </div>
</div>
<!-- End Page Content -->