<?= $this->Html->css('base.css',['fullBase' => true]) ?>
<?= $this->Html->css('style.css',['fullBase' => true]) ?>
<?= $this->Html->css('home.css',['fullBase' => true]) ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">

<table width="100%">
    <tr>
        <td>
            <div>
                <ul>
                    <?php foreach ($rows as $row): ?>
                    <li class="">&nbsp;</li>
                    <li class="">&nbsp;</li>
                    <li class="">
                        <strong><?= $row['client'] ?></strong>
                    </li>
                    <li class="">
                        <strong><?= $row['policy'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                    </li>
                    <li>
                        <strong><?= $dFrom ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= strtoupper(substr($row['cover'],0,4)) ?></strong>
                    </li>
                    <li>
                        <strong><?= $dTo ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;00:00</strong>
                    </li>
                    <li>
                        <strong><?= $row['reg'] ?></strong>
                    </li>
                    <li class="">&nbsp;</li>
                    <li>
                        <strong><?= $row['firm'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                    </li>
                    <li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong><?= strtoupper($serial) ?></strong>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </td>
        <td nowrap>
            <div>
                <ul>
                    <?php foreach ($rows as $row): ?>
		    <li class="">
                        &nbsp;
                    </li>
		    <li class="">
                        &nbsp;
                    </li>
                    <li class="">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['client'] ?></strong>
                    </li>
                    <li class="">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['policy'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                    </li>
                    <li>
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $dFrom ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= strtoupper(substr($row['cover'],0,4)) ?></strong>
                    </li>
                    <li>
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $dTo ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;00:00</strong>
                    </li>
                    <li>
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['reg'] ?></strong>
                    </li>
                    <li class="">&nbsp;</li>
                    <li>
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['firm'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                    </li>
                    <li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong><?= strtoupper($serial) ?></strong>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="columns large-4">
                <ul>
                    <?php foreach ($rows as $row): ?>
                    <li class="">
                        &nbsp;
                    </li>
		    <li class="">
                        &nbsp;
                    </li>
                    <li class="">
                        &nbsp;
                    </li>
                    <li class="">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['policy'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                    </li>
                    <li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $dFrom ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= strtoupper(substr($row['cover'],0,4)) ?></strong>
                    </li>
                    <li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $dTo ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;00:00</strong>
                    </li>
                    <li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['reg'] ?></strong>
                    </li>
                    <li class="">&nbsp;</li>
                    <li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['firm'] ?></strong>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </td>
    </tr>
</table>



