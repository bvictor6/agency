<?= $this->Html->css('base.css',['fullBase' => true]) ?>
<?= $this->Html->css('style.css',['fullBase' => true]) ?>
<?= $this->Html->css('home.css',['fullBase' => true]) ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">


<?php 
    $batchNo = null;
    $firm = null;
    $cat = null;
    $fromTo = null;
    $rcvd = null;
    foreach ($details as $detail):
        $firm = $detail->insurance_firm->name;
        $cat = $detail->category->name;
        $batchNo = $detail->batch_no;
        $rcvd = $detail->date_received;
        $fromTo = $detail->batch_from." To ".$detail->batch_to;
    endforeach; 
?>

<!-- Page Content -->
<div class="row">
    
    <div class="columns large-12">
        <h5 class="block-title">Certificates <small>&Tab;Batch:&nbsp;&nbsp;<?= strtoupper($all)." :: ".strtoupper($firm) ?></small></h5>
        <h5 class="block-title"><small><?= strtoupper($cat) ?>&Tab;[<?= $fromTo ?>]&Tab;&Tab;Received:&nbsp;&nbsp;<?= date('Y-M-d', strtotime($rcvd)) ?></small></h5>
          
        <div>
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="font-s12">Serial#</th>
                        <th class="font-s12">Policy#</th>
                        <th class="font-s12">Reg#</th>
                        <th class="font-s12">Cert Date From</th>
                        <th class="font-s12">Cert Date To</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($certificates as $certificate): ?>
                    <tr>
                        <td class="font-s12"><?= ($certificate->serial_no) ?></td>
                        <td class="font-s12" nowrap>
                            <?php 
                                if($certificate->vehicle->has('policy'))
                                {
                                    echo $this->Html->link($certificate->vehicle->policy->name, 
                                    ['controller' => 'Policies', 'action' => 'view', 
                                    $certificate->vehicle->policy->id]);                                        
                                } else{ echo '';}                                    
                                                                
                            ?>
                        </td>
                        <td class="font-s12"><?= $certificate->has('vehicle') ? $this->Html->link($certificate->vehicle->registration_number, ['controller' => 'Vehicles', 'action' => 'view', $certificate->vehicle->id]) : '' ?></td>
                        
                        <td class="font-s12"><?= date('Y-M-d',  strtotime($certificate->date_from)) ?></td>
                        <td class="font-s12"><?= date('Y-M-d',  strtotime($certificate->date_to)) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->