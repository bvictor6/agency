
<?= $this->Html->css('base.css') ?>
<?= $this->Html->css('style.css') ?>
<?= $this->Html->css('home.css') ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">

<div class="columns large-4">
    <ul>
        <?php foreach ($rows as $row): ?>
        <li class="">
            <?= $row['client'] ?>
        </li>
        <li class="">
            <?= $row['policy'] ?>
        </li>
        <li>
            <?= $row['edate'] ?>
        </li>
        <li>
            <?= $row['exdate'] ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="columns large-4">
    <ul>
        <?php foreach ($rows as $row): ?>
        <li class="">
            <?= $row['client'] ?>
        </li>
        <li class="">
            <?= $row['policy'] ?>
        </li>
        <li>
            <?= $row['edate'] ?>
        </li>
        <li>
            <?= $row['exdate'] ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="columns large-4">
    <ul>
        <?php foreach ($rows as $row): ?>
        <li class="">
            <?= $row['client'] ?>
        </li>
        <li class="">
            <?= $row['policy'] ?>
        </li>
        <li>
            <?= $row['edate'] ?>
        </li>
        <li>
            <?= $row['exdate'] ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>