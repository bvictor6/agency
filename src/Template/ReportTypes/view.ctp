<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReportType $reportType
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;REPORTS TYPES'), 
                        ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Report Types','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW REPORT TYPE'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create New Report Type','escape'=>false]) ?>
                               
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;REPORTS [ALL]'), 
                        ['controller'=>'Reports','action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Reports','escape'=>false]) ?>
                
            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Reports</li>
                <li> <?= $this->Html->link('Report Types', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($reportType->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Details</h3>
    </div>
    <div class="block-content">
        <div class="row items-push">             
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <span class="font-w600">REPORT TYPE</span>
                    <div>
                        <?= h($reportType->name) ?>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <span class="font-w600">Description</span>
                    <?= $this->Text->autoParagraph(h($reportType->description)); ?>
                </div>
            </div> 
        </div>
        
        <!-- Related Reports -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Reports</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($reportType->reports)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="hidden-sm">Report</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($reportType->reports as $report): ?>
                            <tr>                
                                <td class="hidden-xs"><?= h($report->name) ?></td>
                                <td hidden="hidden"></td>
                                <td hidden="hidden"></td>
                                <td hidden="hidden"></td>                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?php
                                            if($report->type=='PDF')
                                            {
                                                echo $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                                'controller'=> h($report->controller) ,
                                                'action' => h($report->action),'_ext' => 'pdf'],
                                                    ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                        'data-toggle'=>'tooltip', 'title'=>'Run Report','escape'=>false]);
                                            }
                                            elseif ($report->type=='EXCEL') 
                                            {
                                                echo $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                                'controller'=> h($report->controller) ,
                                                'action' => h($report->action),'_ext' => 'xlsx'],
                                                    ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                        'data-toggle'=>'tooltip', 'title'=>'Run Report','escape'=>false]);                                                                            
                                            }
                                            else 
                                            {
                                                echo $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                                'controller'=> h($report->controller) ,
                                                'action' => h($report->action)],
                                                    ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                        'data-toggle'=>'tooltip', 'title'=>'Run Report','escape'=>false]);
                                            }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Related Reports -->        
    </div>
</div>
<!-- End Paragraphs in Block -->
