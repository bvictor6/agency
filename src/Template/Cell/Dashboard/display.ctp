<?php
/*
 * MySQL DAYOFWEEK() returns the week day number (1 for Sunday,2 for Monday …… 7 for Saturday ) 
 * for a date specified as argument.
 */
$sun = 0;
$mon = 0;
$tue = 0;
$wed = 0;
$thur = 0;
$fri = 0;
$sat = 0;
foreach ($policies as $policy):
    switch ($policy['weekday'])
    {
        case 1:
            $sun = $policy['total'];
            break;
        case 2:
            $mon = $policy['total'];
            break;
        case 3:
            $tue = $policy['total'];
            break;
        case 4:
            $wed = $policy['total'];
            break;
        case 5:
            $thur = $policy['total'];
            break;
        case 6:
            $fri = $policy['total'];
            break;
        case 7:
            $sat = $policy['total'];
            break;
    
    }
endforeach;
?>

<canvas class="js-dash-chartjs-lines2">                                         
</canvas>
<script src="js/core/jquery.min.js"></script>
<script>
    var BasePagesDashboard = function() 
    {
        // Chart.js Chart, for more examples you can check out http://www.chartjs.org/docs
        var initDashChartJS = function(){
            // Get Chart Container
            var $dashChartLinesCon  = jQuery('.js-dash-chartjs-lines2')[0].getContext('2d');

            // Set Chart and Chart Data variables
            var $dashChartLines, $dashChartLinesData;

            // Lines Chart Data
            var $dashChartLinesData = {
                labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
                datasets: [
                    /*{
                        label: 'This Week',
                        fillColor: 'rgba(44, 52, 63, .07)',
                        strokeColor: 'rgba(44, 52, 63, .25)',
                        pointColor: 'rgba(44, 52, 63, .25)',
                        pointStrokeColor: '#fff',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(44, 52, 63, 1)',
                        data: [34, 42, 40, 65, 48, 56, 80]
                    },*/
                    {
                        label: 'This Week',
                        fillColor: 'rgba(44, 52, 63, .1)',
                        strokeColor: 'rgba(44, 52, 63, .55)',
                        pointColor: 'rgba(44, 52, 63, .55)',
                        pointStrokeColor: '#fff',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(44, 52, 63, 1)',
                        data: ["<?php echo $mon; ?>", "<?php echo $tue; ?>", "<?php echo $wed; ?>", 
                            "<?php echo $thur; ?>", "<?php echo $fri; ?>", "<?php echo $sat; ?>", 
                            "<?php echo $sun; ?>"]
                    }
                ]
            };

            // Init Lines Chart
            $dashChartLines = new Chart($dashChartLinesCon).Line($dashChartLinesData, {
                scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
                scaleFontColor: '#999',
                scaleFontStyle: '600',
                tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
                tooltipCornerRadius: 3,
                maintainAspectRatio: false,
                responsive: true
            });
        };

        return {
            init: function () {
                // Init ChartJS chart
                initDashChartJS();
            }
        };
    }();

    // Initialize when page loads
    jQuery(function(){ BasePagesDashboard.init(); });
</script>

