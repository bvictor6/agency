<?php
$tab1 = array();
$tab1 = array_slice($policies, 0, 10);
$tab2 = array_slice($policies, 10);

?>
<div>
    <table class="table remove-margin-b font-s13">
        <tbody>
            <?php foreach ($tab1 as $policy): ?>
                <tr>
                    <td class="font-w600">
                        <a href="javascript:void(0)"><?= $policy['name'] ?></a>&nbsp;&nbsp;
                    </td>
                    <td class="hidden-xs text-muted text-right" ><?= h($policy['date_created']) /* date("H:i:s",strtotime($policy['modified']))*/ ?></td>
                    <!--td class="font-w600 text-success text-right" style="width: 70px;">+ <?= $policy['id'] ?></td-->
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php if(count($tab2)>0) { ?>

<div>
    <table class="table remove-margin-b font-s13">
        <tbody>
            <?php foreach ($tab2 as $policy): ?>
                <tr>
                    <td class="font-w600">
                        <a href="javascript:void(0)"><?= $policy['name'] ?></a>&nbsp;&nbsp;
                    </td>
                    <td class="hidden-xs text-muted text-right"><?= h($policy['date_created']) /*date("H:i:s",strtotime($policy['modified']))*/ ?></td>
                    <!--td class="font-w600 text-success text-right" style="width: 70px;">+ <?= $policy['id'] ?></td-->
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php } ?>
