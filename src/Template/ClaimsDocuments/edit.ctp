<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClaimsDocument $claimsDocument
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $claimsDocument->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $claimsDocument->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Claims Documents'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Document Types'), ['controller' => 'DocumentTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Document Type'), ['controller' => 'DocumentTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Claims'), ['controller' => 'Claims', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Claim'), ['controller' => 'Claims', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="claimsDocuments form large-9 medium-8 columns content">
    <?= $this->Form->create($claimsDocument) ?>
    <fieldset>
        <legend><?= __('Edit Claims Document') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('document_type_id', ['options' => $documentTypes]);
            echo $this->Form->control('claim_id', ['options' => $claims]);
            echo $this->Form->control('filesize');
            echo $this->Form->control('filemime');
            echo $this->Form->control('path');
            echo $this->Form->control('date_created', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
