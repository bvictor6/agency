<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->css('base.css',['fullBase' => true]) ?>
    <?= $this->Html->css('style.css',['fullBase' => true]) ?>
    <?= $this->Html->css('home.css',['fullBase' => true]) ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body class="home">
    <div class="row">
        <?= $this->fetch('content') ?>
    </div>
</body>
</html>