<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title></title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <?= $this->Html->meta('icon') ?>
    
    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Page JS Plugins CSS -->
    <?= $this->Html->css('../js/plugins/datatables/jquery.dataTables.min.css') ?>

    <!-- OneUI CSS framework -->
    <?= $this->Html->css('oneui.css',['id'=>'css-main']); ?>

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <?= $this->Html->css('themes/city.min.css',['id'=>'css-theme']); ?>
    <!-- END Stylesheets -->
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    
    <!-- Page Container -->
        <!--
            Available Classes:

            'sidebar-l'                  Left Sidebar and right Side Overlay
            'sidebar-r'                  Right Sidebar and left Side Overlay
            'sidebar-mini'               Mini hoverable Sidebar (> 991px)
            'sidebar-o'                  Visible Sidebar by default (> 991px)
            'sidebar-o-xs'               Visible Sidebar by default (< 992px)

            'side-overlay-hover'         Hoverable Side Overlay (> 991px)
            'side-overlay-o'             Visible Side Overlay by default (> 991px)

            'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

            'header-navbar-fixed'        Enables fixed header
        -->
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            
            
            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                            <div class="btn-group pull-right">
                                <button class="btn btn-link text-gray dropdown-toggle" data-toggle="dropdown" type="button">
                                    <i class="si si-drop"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right font-s13 sidebar-mini-hide">
                                    <li>
                                        <a data-toggle="theme" data-theme="default" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-default pull-right"></i> <span class="font-w600">Default</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="/agency/css/themes/amethyst.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-amethyst pull-right"></i> <span class="font-w600">Amethyst</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="/agency/css/themes/city.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-city pull-right"></i> <span class="font-w600">City</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="/agency/css/themes/flat.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-flat pull-right"></i> <span class="font-w600">Flat</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="/agency/css/themes/modern.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-modern pull-right"></i> <span class="font-w600">Modern</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="theme" data-theme="/agency/css/themes/smooth.min.css" tabindex="-1" href="javascript:void(0)">
                                            <i class="fa fa-circle text-smooth pull-right"></i> <span class="font-w600">Smooth</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <a class="h5 text-white" href="index.html">
                                <i class="fa fa-circle-o-notch text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide">!</span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                                <li>
                                    <a class="active" href="/agency">
                                        <i class="si si-speedometer"></i>
                                        <span class="sidebar-mini-hide">Dashboard</span>
                                    </a>

                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Home</span></li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide">Settings</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Master Details', ['controller'=>'Settings','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Insurance Firms', ['controller'=>'InsuranceFirms','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("LOB's", ['controller'=>'LineOfBusinesses','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Covers', ['controller'=>'CoverTypes','action'=>'index']) ?>
                                        </li> 
                                        <li>
                                            <?= $this->Html->link('Categories', ['controller'=>'Categories','action'=>'index']) ?>
                                        </li> 
                                    </ul>
                                </li>
                                
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-wrench"></i><span class="sidebar-mini-hide">Setup</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Document Types', ['controller'=>'DocumentTypes','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Garages",['controller'=>'Garages','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Vehicle Models",['controller'=>'Models','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Vehicle Makes", ['controller'=>'Makes','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link("Colors", ['controller'=>'Colors','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>  
                                
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-book-open"></i><span class="sidebar-mini-hide">Underwriting</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Clients', ['controller'=>'Clients', 'action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Policies', ['controller'=>'Policies','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Claims', ['controller'=>'Claims','action'=>'index']) ?>
                                        </li> 
                                        <li>
                                            <?= $this->Html->link('Payments', ['controller'=>'Payments','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">More++</span></li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Certificates</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Batches', ['controller'=>'Batches','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Available Certificates', ['controller'=>'Certificates','action'=>'index',0]) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Printed Certificates', ['controller'=>'Certificates','action'=>'index',1]) ?>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Reports</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Reports [ALL]', ['controller'=>'Reports','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Reports By Type', ['controller'=>'ReportTypes','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Misc</span></a>
                                    <ul>                                      
                                        <li>
                                            <?= $this->Html->link('Expenses', ['controller'=>'Expenses','action'=>'index']) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('Reminders', ['controller'=>'Reminders','action'=>'index']) ?>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Admin</span></a>
                                    <ul>
                                        <li>
                                            <?= $this->Html->link('Users', ['controller'=>'Users','action'=>'index']) ?>
                                        </li>
                                        <!--li>
                                            <?= $this->Html->link('Backup DB', ['controller'=>'Users','action'=>'backupdb']) ?>
                                        </li-->
                                        
                                    </ul>
                                </li>
                                
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->
            
            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
                <!-- Header Navigation Right -->
                <ul class="nav-header pull-right">
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                                <!--img src="assets/img/avatars/avatar10.jpg" alt="Avatar"-->
                                <i class="fa fa-user"></i>&nbsp;&nbsp;
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php
                                    $user = $this->request->getSession()->read('Auth.User'); 
                                    if(isset($user))
                                    {
                                ?>
                                        <li class="dropdown-header">Profile</li> 
                                        <li>
                                            <a tabindex="-1" href="#">
                                                <i class="si si-user pull-right"></i>
                                                <?= $user['name'] ?>
                                            </a>
                                        </li>
                                        <li>                                        
                                            <a tabindex="-1" href="#">
                                                <i class="si si-envelope pull-right"></i>
                                                <?= $user['email'] ?>
                                            </a>
                                        </li>
                                        
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Actions</li>
                                        <li>                                            
                                            <?= $this->Html->link(__('<i class="si si-shuffle pull-right"></i>&nbsp;&nbsp; Change Password'), 
                                                    ['controller'=>'Users','action'=>'changepwd',$user['id']],
                                                    ['tabindex'=>'-1','escape'=>false]) 
                                            ?>
                                        </li>
                                        <li>
                                            <a tabindex="-1" href="/agency/Users/logout">
                                                <i class="si si-logout pull-right"></i>Sign out
                                            </a>
                                        </li>

                                <?php } else {?>
                                        <li class="dropdown-header">Profile</li>                                
                                        <li>                                            
                                            <?= $this->Html->link(__('<i class="si si-login pull-right"></i>&nbsp;&nbsp; Sign In'), 
                                                    ['controller'=>'Users','action'=>'login'],
                                                    ['tabindex'=>'-1','escape'=>false]) 
                                            ?>
                                        </li>
                                <?php }?>
                            </ul>
                        </div>
                    </li>
                    
                </ul>
                <!-- END Header Navigation Right -->

                <!-- Header Navigation Left -->
                <ul class="nav-header pull-left">
                    <li class="hidden-md hidden-lg">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                            <i class="fa fa-navicon"></i>
                        </button>
                    </li>
                    <li class="hidden-xs hidden-sm">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                    </li>
                    
                </ul>
                <!-- END Header Navigation Left -->
            </header>
            <!-- END Header -->
            
            <!-- Main Container -->
            <main id="main-container">
                <?= $this->Flash->render() ?>
                <?= $this->Flash->render('auth') ?>
                <!-- Page Header -->
                <div class="content bg-image overflow-hidden" style="background-image: url('/agency/assets/img/photos/bg.jpg');">
                    <div class="push-50-t push-15">
                        <?= $this->Cell('Dashboard::companyName') ?>
                        <h2 class="h5 text-white-op animated zoomIn">
                            Welcome 
                            <?php
                                if(isset($user))
                                {
                                    echo $user['name'];
                                }
                            ?>
                        </h2>
                    </div>
                </div>
                <!-- END Page Header -->
                <!-- Page Content -->
                <div class="content">
                    <?= $this->fetch('content') ?>
                </div>
                <!-- END Page Content -->
            </main>
            
            <!-- Footer -->
            <!--footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-right">
                    Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="javascript:void(0)" target="_blank">VicSoft Technologies</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="javascript:void(0)" target="_blank">Agency+ 1.0</a> &copy; <span class="js-year-copy"></span>
                </div>
            </footer-->
            <!-- END Footer -->
        
        </div>
        <!-- END Page Container -->

        <?php //$this->Html->script('core/jquery.min.js'); ?>
        <?= $this->Html->script('jquery-3.2.1.min.js'); ?>
        <?= $this->Html->script('core/bootstrap.min.js'); ?>
        <?= $this->Html->script('core/jquery.slimscroll.min.js'); ?>
        <?= $this->Html->script('core/jquery.scrollLock.min.js'); ?>
        <?= $this->Html->script('core/jquery.appear.min.js'); ?>
        <?= $this->Html->script('core/jquery.countTo.min.js'); ?>
        <?= $this->Html->script('core/jquery.placeholder.min.js'); ?>
        <?= $this->Html->script('core/js.cookie.min.js'); ?>
        <?= $this->Html->script('app.js'); ?>

        <!-- Page JS Plugins -->
        <!--script src="js/plugins/datatables/jquery.dataTables.min.js"></script-->
        <?= $this->Html->script('plugins/datatables/jquery.dataTables.min.js'); ?>
        <?= $this->Html->script('plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js'); ?>
        <?= $this->Html->script('plugins/jquery-validation/jquery.validate.min.js'); ?>

        <!-- Page JS Code -->
        <?= $this->Html->script('pages/base_forms_wizard.js'); ?>
        <?= $this->Html->script('pages/base_tables_datatables.js'); ?>
        
        <script>
            $(function () {
                // Init page helpers (Slick Slider plugin)
                App.initHelpers('slick');
            });
        </script>
</body>
</html>
