<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Report[]|\Cake\Collection\CollectionInterface $reports
 */
?>

<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                
                <div class="btn-group">                     
                    <?= $this->Html->link(__('<i class="fa fa-plus-square"></i>&nbsp;&nbsp;NEW'), ['action' => 'add'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'New Report','escape'=>false]) ?>
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                        ['controller' => 'Policies', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>
                
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                            ['controller' => 'InsuranceFirms', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>

                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;LOB\'s'), 
                            ['controller' => 'LinesOfBusinesses', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Lines of Business','escape'=>false]) ?>
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>MISC</li>
                    <li><a class="link-effect" href="">Reports</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Reports <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                &nbsp;
            </ol>
        </div>
        <!-- End Excel Button --> 
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-hover table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 7%;"></th>                        
                        <th class="hidden-sm">Report</th>                        
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($reports as $report): ?>
                    <tr>
                        <td class="text-center"><?= $this->Number->format($report->id) ?></td>
                        <td class="font-w300"><?= h($report->name) ?></td>
                        <td hidden="hidden"></td>
                        <td hidden="hidden"></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <?php
                                    if($report->type=='PDF')
                                    {
                                        echo $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                        'controller'=> h($report->controller) ,
                                        'action' => h($report->action),'_ext' => 'pdf'],
                                            ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                'data-toggle'=>'tooltip', 'title'=>'Run Report','escape'=>false]);
                                    }
                                    elseif ($report->type=='EXCEL') 
                                    {
                                        echo $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                        'controller'=> h($report->controller) ,
                                        'action' => h($report->action),'_ext' => 'xlsx'],
                                            ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                'data-toggle'=>'tooltip', 'title'=>'Run Report','escape'=>false]);                                                                            
                                    }
                                    else 
                                    {
                                        echo $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                        'controller'=> h($report->controller) ,
                                        'action' => h($report->action)],
                                            ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                'data-toggle'=>'tooltip', 'title'=>'Run Report','escape'=>false]);
                                    }
                                ?>
                                                                                         
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->