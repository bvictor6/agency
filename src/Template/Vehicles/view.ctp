<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vehicle $vehicle
 */
?>
<?= $this->Html->script('jquery-3.2.1.min.js'); ?>
<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $vehicle->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Vehicle Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;VEHICLES'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Vehicles','escape'=>false]) ?>
                    
                
            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Underwriting</li>
                <li> <?= $this->Html->link('Vehicles', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($vehicle->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->


<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header col-sm-7">
        <h3 class="block-title"><?= $vehicle->has('policy') ? $this->Html->link($vehicle->policy->name, ['controller' => 'Policies', 'action' => 'view', $vehicle->policy->id]) : '' ?></h3>
    </div>
    
    <!-- Excel Button -->
    <div class="col-sm-5 text-right hidden-xs">
        <ol class="breadcrumb push-10-t">
            <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Extra Cover'), 
                        ['controller'=>'ExtraCovers','action' => 'add',$vehicle->id],
                        ['class'=>'btn btn-xs btn-info animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Add Extra Cover','escape'=>false]) ?>
        </ol>
    </div>
    <!-- End Excel Button -->
    
    <div class="block-content">
        <div class="row items-push col-sm-12">
            <div class="col-sm-4">
                <h6>Category</h6>
                <ul>                    
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $vehicle->has('category') ? $this->Html->link($vehicle->category->name, ['controller' => 'Categories', 'action' => 'view', $vehicle->category->id]) : '' ?></li>                   
                </ul>
                <h6>Registration#</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(strtoupper($vehicle->registration_number)) ?></li>                    
                </ul>
                <h6>Color</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $vehicle->has('color') ? $this->Html->link($vehicle->color->name, ['controller' => 'Colors', 'action' => 'view', $vehicle->color->id]) : '' ?></li>
                </ul>
                <h6>Value</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(number_format($vehicle->value)) ?></li>
                </ul>
            </div>
            
            <div class="col-sm-4">
                <h6>Make</h6>
                <ul>                    
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $vehicle->has('make') ? $this->Html->link($vehicle->make->name, ['controller' => 'Makes', 'action' => 'view', $vehicle->make->id]) : '' ?></li>                    
                </ul>  
                <h6>Model</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $vehicle->has('model') ? $this->Html->link($vehicle->model->name, ['controller' => 'Models', 'action' => 'view', $vehicle->model->id]) : '' ?></li>
                </ul>
                <h6>Year</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h($vehicle->year) ?></li>
                </ul>
                <h6>Premium</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(number_format($vehicle->premium)) ?></li>
                </ul>    
            </div>
            
            <div class="col-sm-4">
                <h6>Cover Type</h6>
                <ul>                    
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $vehicle->has('cover_type') ? $this->Html->link($vehicle->cover_type->name, ['controller' => 'CoverTypes', 'action' => 'view', $vehicle->cover_type->id]) : '' ?></li>                   
                </ul>                
                <h6>Chasis#</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(strtoupper($vehicle->chasis_number)) ?></li>
                </ul>
                <h6>Engine#</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(strtoupper($vehicle->engine_number)) ?></li>
                </ul>
                <h6>Period</h6>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;
                        <?= h(date('Y-M-d',  strtotime($vehicle->effective_date))) .' to '.h(date('Y-M-d',  strtotime($vehicle->expiry_date))) ?>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="col-sm-12">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-6">
                <div class="btn-group">
                    
                    <?php 
                        echo $this->Form->button('<i class="fa fa-print"></i>&nbsp;&nbsp;ASSIGN CERTIFICATE', 
                                [
                                    'data-toggle' => 'modal', 
                                    'data-target' => '#MyModal',
                                    'title'=>'Click to set auto-resupply stores',
                                    'class' => 'btn btn-info btn-sm'
                                ]) ;
                    ?>
                    
                    <?= $this->Html->link(__('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;ADD CLAIM'), 
                            ['controller' => 'Claims', 'action' => 'add',$vehicle->id,$vehicle->policy->id],
                            ['class'=>'btn btn-sm btn-info animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'New Claim','escape'=>false]) ?>

                </div> 
            </div>             
            <div class="col-sm-3">&nbsp;</div>
        </div>
        
        <!-- Related Payments -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title"><span class="badge">Related <small><span class="badge badge">Payments</span></small></span></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($vehicle->payments)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="hidden-sm">Amount</th>
                                <th class="hidden-sm">Date</th>
                                <th class="hidden-xs">Ref#</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vehicle->payments as $payments): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="hidden-xs"><?= h(number_format($payments->amount)) ?></td>
                                <td class="hidden-xs"><?= h(date('Y-M-d', strtotime($payments->payment_date))) ?></td>
                                <td class="hidden-xs"><?= h($payments->transaction_ref) ?></td>
                                                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Payments', 'action' => 'view', $payments->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Payment Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
        <!-- Related Certificates -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title"><span class="badge badge">Related <small><span class="badge badge">Certificates</span></small></span></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($vehicle->certificates)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="hidden-sm">Batch</th>
                                <th class="hidden-sm">Serial#</th>
                                <th class="hidden-sm">Printed#</th>
                                <th class="hidden-sm">From</th>
                                <th class="hidden-sm">To</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vehicle->certificates as $certificate): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="hidden-xs"><?= h($certificate->batch_no) ?></td>
                                <td class="hidden-xs"><?= h($certificate->serial_no) ?></td>
                                <td class="hidden-xs"><?= ($certificate->printed)?
                                "<span class='label label-success fa fa-check-circle'>  Printed</span>":
                                "<span class='label label-danger fa fa-times-circle'>  Not Printed</span>" ?>
                                </td>
                                <td class="hidden-xs"><?= date('Y-M-d',  strtotime($certificate->date_from)) ?></td>
                                <td class="hidden-xs"><?= date('Y-M-d',  strtotime($certificate->date_to)) ?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Certificates', 'action' => 'view', $certificate->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Certificate Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Related Certificates -->
        
        <!-- Related Extra Covers -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title"><span class="badge badge">Related <small><span class="badge badge">Additional Covers</span></small></span></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($vehicle->extra_covers)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="hidden-sm">Date</th>
                                <th class="hidden-sm">Amount</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vehicle->extra_covers as $extras): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="hidden-xs"><?= h(date('Y-M-d', strtotime($extras->date_created))) ?></td>
                                <td class="hidden-xs"><?= h(number_format($extras->amount)) ?></td>
                                <td hidden="hidden">&nbsp;</td>                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'ExtraCovers', 'action' => 'view', $extras->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Extra Cover Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Related Extra Covers -->
        
        <!-- Related Claims -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title"><span class="badge badge">Related <small><span class="badge badge">Claims</span></small></span></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($vehicle->claims)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="hidden-sm">Date Reported</th>
                                <th class="hidden-sm">Accident Date</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vehicle->claims as $claims): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="hidden-xs"><?= h(date('Y-M-d', strtotime($claims->date_reported))) ?></td>
                                <td class="hidden-xs"><?= h(date('Y-M-d', strtotime($claims->accident_date))) ?></td>
                                                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Claims', 'action' => 'view', $claims->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Claim Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Related Claims -->
                
    </div>
</div>
<!-- End Paragraphs in Block -->
<!-- Modal screen for selecting certificate -->
<!-- Large Modal -->
<div class="modal" id="MyModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Certificates</h3>
                </div>
                <div class="block-content">
                    <p>
                        <div class="block">
                            <div class="block-content">
                                <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 7%;"></th>
                                            <th class="font-s12">Serial#</th>
                                            <th class="font-s12">Assign?</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($certificates as $certificate): ?>
                                        <tr>
                                            <td class="font-s12"><?= h($certificate->id) ?></td>
                                            <td class="font-s12"><?= h($certificate->serial_no) ?></td>
                                            <td class="font-s12">
                                                <div class="btn-group">

                                                    <?= $this->Form->create($certificate) ?>
                                                        <?php echo $this->Form->control('status',['label'=>'','id'=>"$certificate->id"]); ?>
                                                    <?= $this->Form->end() ?>

                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <!--button class="btn btn-primary btn-md fa fa-close" type="button" data-dismiss="modal" id="btn-close">Cancel</button-->
                <button class="btn btn-danger btn-md fa fa-check" type="button" data-dismiss="modal" id="btn-submit">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- END Large Modal -->

<div id="div1">
    <p>
        <span class="label label-warning" id="log">**</span>
    </p>
</div>

<!-- Ajax loading indicator -->
<div id="wait" style="display: none;width:69px;height:89px;position:absolute;top:50%;
    left:50%;padding:2px;">
   <?= $this->Html->image('ajax_loader.gif',['width'=>64,'height'=>64]) ?>
</div>
<!-- end Ajax loading -->

<script>
    $('body').on('change', 'input[type="checkbox"]', function() 
    {
        testing(event.target.id);
    });


    function testing(spanID)
    {
        //alert(event.target.id+" - "+window.location.origin);
        $.ajax({
                url: window.location.origin + '/agency/certificates/cert/' 
                        + spanID + '/'
                        +"<?php echo $vehicle->id ?>", 
                success: function(result)
                {                
                    $("#log").html(result);
                    if(result == '200 OK')
                    {
                        //alert(window.location.origin + '/agency/certificates/edit/' + spanID);
                        window.location = (window.location.origin 
                                + '/agency/certificates/edit/' 
                                + spanID + '/'
                                +"<?php echo $vehicle->category->id ?>");                                        
                    }
                    else{
                        $("#log").html(result);
                    }
                }
            });
            //$("#MyModal").modal('toggle');
    }
        
</script>