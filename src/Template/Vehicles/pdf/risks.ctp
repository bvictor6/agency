<?= $this->Html->css('base.css',['fullBase' => true]) ?>
<?= $this->Html->css('style.css',['fullBase' => true]) ?>
<?= $this->Html->css('home.css',['fullBase' => true]) ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">

<div class="row">
    <div class="columns large-12">
        <h4><small>RISKS</small></h4>
        <table>
            <thead>
                <tr>
                    <th class="text-center" style="width: 7%;"></th>                           
                    <th>Policy#</th>
                    <th>Reg#</th>
                    <th>Year</th>
                    <th>Make</th>
                    <th>Model</th>  
                    <th>Cover</th>
                </tr>
            </thead>
            <tbody
                <?php foreach ($vehicles as $vehicle): ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($vehicle->id) ?></td>                        
                    <td class="font-w300"><?= $vehicle->has('policy') ? $this->Html->link($vehicle->policy->name, ['controller' => 'Policies', 'action' => 'view', $vehicle->policy->id]) : '' ?></td>
                    <td class="hidden-xs"><?= h($vehicle->registration_number) ?></td>
                    <td class="hidden-xs"><?= h($vehicle->year) ?></td>
                    <td class="hidden-xs"><?= h($vehicle->make) ?></td>
                    <td class="hidden-xs"><?= h($vehicle->model) ?></td>                                    
                    <td class="hidden-xs"><?= $vehicle->has('cover_type') ? $this->Html->link($vehicle->cover_type->name, ['controller' => 'CoverTypes', 'action' => 'view', $vehicle->cover_type->id]) : '' ?></td>                                                              
                    
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
