<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PolicyAction $policyAction
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;POLICIES'), [
                    'controller'=>'Policies','action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Policies</li>
                <li> <?= $this->Html->link('Policies', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($policyAction->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Details</h3>
    </div>
    <div class="block-content">
        <div class="row items-push">
            <div class="col-sm-6">
                
                <table class="table table-hover table-borderless">
                    <tr>
                        <td class="font-w600"><?= __('Policy#') ?></th>
                        <td class="font-w400">
                            <?= $policyAction->has('policy') ? $this->Html->link($policyAction->policy->name, 
                                    ['controller' => 'Policies', 'action' => 'view', $policyAction->policy->id]) : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-w600"><?= __('Action') ?></th>
                        <td class="font-w400"><?= h($policyAction->action) ?></td>
                    </tr>      
                    <tr>
                        <td class="font-w600"><?= __('Action Date') ?></th>
                        <td class="font-w400"><?= h(date('Y-M-d', strtotime($policyAction->action_date))) ?></td>
                    </tr>
                </table>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <h4><?= __('Remarks') ?></h4>
                    <?= $this->Text->autoParagraph(h($policyAction->remarks)); ?>
                </div>
                                
        </div>
    </div>
</div>
<!-- End Paragraphs in Block -->

