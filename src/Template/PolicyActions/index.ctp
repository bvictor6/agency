<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PolicyAction[]|\Cake\Collection\CollectionInterface $policyActions
 */
?>


<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                
                <div class="btn-group">                     
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                            ['controller'=>'Policies','action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                        ['controller' => 'CoverTypes', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                            ['controller' => 'InsuranceFirms', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>

                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;VEHICLES'), 
                            ['controller' => 'Vehicles', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Motor Vehicles','escape'=>false]) ?>
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Policies</li>
                    <li><a class="link-effect" href="">Actions</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Policy Logs <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                &nbsp;
            </ol>
        </div>
        <!-- End Excel Button --> 
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>                        
                        <th class="text-center" style="width: 7%;"></th>                        
                        <th class="hidden-sm">Policy#</th>
                        <th class="hidden-sm">Action</th>
                        <th class="hidden-sm">Date</th>
                        <th class="hidden-sm">User</th>                       
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($policyActions as $policyAction): ?>
                    <tr>
                        <td class="text-center"><?= $this->Number->format($policyAction->id) ?></td>
                        <td class="font-w300">
                            <?= $policyAction->has('policy') ? $this->Html->link($policyAction->policy->name, 
                                    ['controller' => 'Policies', 'action' => 'view', $policyAction->policy->id]) : '' ?>
                        </td>
                        <td class="hidden-xs"><?= h(strtoupper($policyAction->action)) ?></td>
                        <td class="hidden-xs"><?= h(date('Y-M-d', strtotime($policyAction->action_date))) ?></td>
                        <td class="hidden-xs"><?= $policyAction->has('user') ? $this->Html->link($policyAction->user->name, ['controller' => 'Users', 'action' => 'view', $policyAction->user->id]) : '' ?></td>
                        
                        <td class="text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $policyAction->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Details','escape'=>false]) ?>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->
