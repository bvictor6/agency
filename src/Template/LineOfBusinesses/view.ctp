<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LineOfBusiness $lineOfBusiness
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $lineOfBusiness->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit LOB Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;LOB\'s'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View LOB\'s','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create New LOB','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>LOB's</li>
                <li> <?= $this->Html->link('LOB\'s', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($lineOfBusiness->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Details</h3>
    </div>
    <div class="block-content">
        <div class="row items-push">             
            <div class="row">
                <div class="col-sm-6">
                    <span class="font-w600">LOB</span>
                    <div>
                        <?= h($lineOfBusiness->name) ?>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <span class="font-w600">Description</span>
                    <?= $this->Text->autoParagraph(h($lineOfBusiness->description)); ?>
                </div>
            </div> 
        </div>
        
        <!-- Related Policies -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Policies</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($lineOfBusiness->policies)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="hidden-sm">Policy#</th>
                                <th class="hidden-sm">Effective Date</th>
                                <th class="hidden-xs">Expiry Date</th>
                                <th class="hidden-xs">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($lineOfBusiness->policies as $policies): ?>
                            <tr>                
                                <td class="hidden-xs"><?= h($policies->name) ?></td>
                                <td class="hidden-xs"><?= h($policies->effective_date) ?></td>
                                <td class="hidden-xs"><?= h($policies->expiry_date) ?></td>
                                <td class="hidden-xs"><?= h($policies->status) ?></td>                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Policies', 'action' => 'view', $policies->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Policy Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Related Policies -->        
    </div>
</div>
<!-- End Paragraphs in Block -->