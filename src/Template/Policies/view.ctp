<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Policy $policy
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?php 
                        if($policy->status!='cancelled')
                        {
                            echo $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                            ['action' => 'edit', $policy->id],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'Edit Policy Details','escape'=>false]);
                        }
                ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;POLICIES'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create New Policy','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Policies</li>
                <li> <?= $this->Html->link('Policies', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($policy->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header col-sm-7">
        <h3 class="block-title"><?= h($policy->name) ?></h3>
    </div>
    <!-- Excel Button -->
    <div class="col-sm-5 text-right hidden-xs">
        <ol class="breadcrumb push-10-t">
            <?= $this->Html->link(__('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF'), 
                        ['action' => 'view', $policy->id,0,'_ext' => 'pdf'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Export PDF - With Payments','escape'=>false]) ?>
            
            <?= $this->Html->link(__('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF'), 
                        ['action' => 'view', $policy->id,1,'_ext' => 'pdf'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Export PDF - No Payments','escape'=>false]) ?>
            
            <?php 
                  if($policy->status!='cancelled')
                  {
                        echo $this->Html->link(__('<i class="si si-close"></i>&nbsp;&nbsp;CANCEL'), 
                        ['controller'=>'PolicyActions','action' => 'add', $policy->id,'cancelled'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                        'data-toggle'=>'tooltip', 'title'=>'Cancel Policy','escape'=>false]);
                  }
                  else
                  {
                      echo $this->Html->link(__('<i class="fa fa-unlock"></i>&nbsp;&nbsp;REINSTATE'), 
                        ['controller'=>'PolicyActions','action' => 'add', $policy->id,'active'],
                        ['class'=>'btn btn-xs btn-success animated zoomIn','type'=>'button',
                        'data-toggle'=>'tooltip', 'title'=>'Reinstate Policy','escape'=>false]);
                  }
                  
                  echo $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;LOG'), 
                        ['controller'=>'PolicyActions','action' => 'index', $policy->id],
                        ['class'=>'btn btn-xs btn-info animated zoomIn','type'=>'button',
                        'data-toggle'=>'tooltip', 'title'=>'Policy Actions Log','escape'=>false]);
            ?>
        </ol>
    </div>
    <!-- End Excel Button -->
    <div class="block-content">
        <div class="row items-push col-sm-12">
            <div class="col-sm-4">
                <h5>Insured</h5>
                <ul>                    
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $policy->has('client') ? $this->Html->link($policy->client->name, ['controller' => 'Clients', 'action' => 'view', $policy->client->id]) : '' ?></li>                   
                </ul>
                <h5>Insurance Firm</h5>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $policy->has('insurance_firm') ? $this->Html->link($policy->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $policy->insurance_firm->id]) : '' ?></li>                    
                </ul>
            </div>
            
            <div class="col-sm-4">
                <h5>LOB</h5>
                <ul>                    
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= $policy->has('line_of_business') ? $this->Html->link($policy->line_of_business->name, ['controller' => 'LineOfBusinesses', 'action' => 'view', $policy->line_of_business->id]) : '' ?></li>                    
                </ul>  
                <h5>Status</h5>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(strtoupper($policy->status)) ?></li>
                </ul>
                    
            </div>
            
            <div class="col-sm-4">
                <h5>Effective Date</h5>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(date('Y-M-d', strtotime($policy->effective_date))) ?></li>
                </ul>
                <h5>Expiry Date</h5>
                <ul>
                    <li class="fa fa-caret-right">&nbsp;&nbsp;<?= h(date('Y-M-d', strtotime($policy->expiry_date))) ?></li>
                </ul>
            </div>
            <div class="col-sm-12">
                <h6><?= __('Remarks') ?></h6>
                <?= $this->Text->autoParagraph(h($policy->remarks)); ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-6">
                <div class="btn-group">
                  <?php 
                  if($policy->status!='cancelled')
                  { ?>                    
                    <?= $this->Html->link(__('<i class="fa fa-upload"></i>&nbsp;&nbsp;UPLOAD DOCUMENTS'), 
                            ['controller' => 'Documents', 'action' => 'add',$policy->id],
                            ['class'=>'btn btn-sm btn-danger animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'Upload Policy Documents','escape'=>false]) ?>

                    <?= $this->Html->link(__('<i class="fa fa-car"></i>&nbsp;&nbsp;ADD RISK'), 
                            ['controller' => 'Vehicles', 'action' => 'add',$policy->id, str_replace('/', '_', $policy->expiry_date)],
                            ['class'=>'btn btn-sm btn-danger animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'New Policy Vehicle','escape'=>false]) ?>

                    <?= $this->Html->link(__('<i class="si si-refresh"></i>&nbsp;&nbsp;RENEW'), 
                            ['controller' => 'Policies', 'action' => 'renew',$policy->id/*, str_replace('/', '_', $policy->expiry_date)*/],
                            ['class'=>'btn btn-sm btn-danger animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'Renew Policy','escape'=>false]) ?>
                  <?php 
                  } ?>                      
                </div>
            </div>             
            <div class="col-sm-3">&nbsp;</div>
        </div>
        <!-- Rellated Vehicles -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Risks</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($policy->vehicles)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="hidden-sm">Year</th>
                                <th class="hidden-sm">Make</th>
                                <th class="hidden-xs">Model</th>
                                <th class="hidden-xs">REG#</th>
                                <th class="hidden-xs">Color</th>
                                <th class="hidden-xs">Value</th>
                                <th class="hidden-xs">Premium</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($policy->vehicles as $vehicles):  ?>
                            <tr>
                                <td class="hidden-xs"><?= h($vehicles->year) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->make->name) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->model->name) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->registration_number) ?></td>
                                <td class="hidden-xs"><?= h($vehicles->color->name) ?></td>
                                <td class="hidden-xs"><?= number_format($vehicles->value) ?></td>
                                <td class="hidden-xs"><?= number_format($vehicles->premium) ?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Vehicles', 'action' => 'view', $vehicles->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Vehicle Details','escape'=>false]) ?>
                                        <?php 
                                        if($policy->status!='cancelled')
                                        { ?>
                                        <?= $this->Html->link(__('<i class="fa fa-dollar"></i>'), ['controller' => 'Payments', 'action' => 'add',$policy->id ,$vehicles->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'Make Payment','escape'=>false]) ?>
                                        <?php } ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End related vehicles -->
        <!-- Related Documents -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Documents</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($policy->documents)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="hidden-xs">Doc Type</th>
                                <th class="hidden-sm">Name</th>
                                <th class="hidden-sm">Filesize</th>                                
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($policy->documents as $documents): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="hidden-xs"><?= h($documents->document_type->name) ?></td>
                                <td class="hidden-xs"><?= h($documents->name) ?></td>
                                <td class="hidden-xs"><?= h($documents->filesize) ?></td>
                                                                                           
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Documents', 'action' => 'view', $documents->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Document Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- End Related Documents -->
        
        <!-- Related Payments -->
        <div class="row items-push">
            <div class="col-sm-12">
                <div class="block-header">
                    <h3 class="block-title">Related <small>Payments</small></h3>
                </div>
                <div class="block-content">
                    <?php if (!empty($policy->payments)): ?>
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                    <table class="table table-responsive js-dataTable-full">
                        <thead>
                            <tr>
                                <th hidden="hidden"></th>
                                <th class="hidden-sm">Amount</th>
                                <th class="hidden-sm">Date</th>
                                <th class="hidden-xs">Ref#</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($policy->payments as $payments): ?>
                            <tr>
                                <td hidden="hidden">&nbsp;</td>
                                <td class="hidden-xs"><?= h($payments->amount) ?></td>
                                <td class="hidden-xs"><?= h($payments->payment_date) ?></td>
                                <td class="hidden-xs"><?= h($payments->transaction_ref) ?></td>
                                                            
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['controller' => 'Payments', 'action' => 'view', $payments->id],
                                                ['class'=>'btn btn-xs btn-info','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'View Payment Details','escape'=>false]) ?>

                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Paragraphs in Block -->



