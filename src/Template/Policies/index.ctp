<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Policy[]|\Cake\Collection\CollectionInterface $policies
 */
?>

<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                
                <div class="btn-group">                     
                    <?= $this->Html->link(__('<i class="fa fa-plus-square"></i>&nbsp;&nbsp;NEW'), ['action' => 'add'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'New Policy','escape'=>false]) ?>
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                        ['controller' => 'CoverTypes', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                            ['controller' => 'InsuranceFirms', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>

                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;VEHICLES'), 
                            ['controller' => 'Vehicles', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Motor Vehicles','escape'=>false]) ?>
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Underwriting</li>
                    <li><a class="link-effect" href="">Policies</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Policies <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                &nbsp;
            </ol>
        </div>
        <!-- End Excel Button --> 
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 7%;"></th>                        
                        <th class="hidden-sm">Policy#</th>
                        <th class="hidden-sm">From</th>
                        <th class="hidden-sm">Expiry</th>
                        <th class="hidden-sm">Status</th>                       
                        <th class="hidden-sm">Insurance</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($policies as $policy): ?>
                    <tr>
                        <td class="text-center"><?= $this->Number->format($policy->id) ?></td>
                        <td class="font-w300"><?= h($policy->name) ?></td>
                        <td class="hidden-xs"><?= h(date('Y-M-d', strtotime($policy->effective_date))) ?></td>
                        <td class="hidden-xs"><?= h(date('Y-M-d', strtotime($policy->expiry_date))) ?></td>
                        <td class="hidden-xs"><?= strtoupper($policy->status) ?></td>                      
                        <td class="hidden-xs"><?= $policy->has('insurance_firm') ? $this->Html->link($policy->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $policy->insurance_firm->id]) : '' ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $policy->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Policy Details','escape'=>false]) ?>
                                
                                <?php 
                                    if($policy->status!='cancelled')
                                    {
                                       echo $this->Html->link(__('<i class="fa fa-pencil"></i>'), ['action' => 'edit', $policy->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'Edit Policy Details','escape'=>false]); 
                                    }                                    
                                 
                                ?>
                                                                                         
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->
