<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim $claim
 */
?>

<?= $this->Html->script('jquery-3.2.1.min.js'); ?>
<?= $this->Html->script('bootstrap-datepicker.min.js'); ?>
<?= $this->Html->css('bootstrap-datepicker3.css'); ?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <!--h3 class="h3 text-danger animated zoomIn"><i class="fa fa-plus"></i>&nbsp;Users</h3-->

            <div class="btn-group">    
                &nbsp;                
            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Reports</li>
                
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Form -->
<div class="block block-bordered">
    <div class="block-header bg-gray-lighter">
        <ul class="block-options">
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
        </ul>
        <h3 class="block-title">PARAMETERS <small>&nbsp;DATES</small></h3>
    </div>
    <div class="block-content"> 
        <?= $this->Form->create()  ?>
        <fieldset>
            
            <div class="col-sm-6"><?php echo $this->Form->control('date_from',['label'=>'Start Date','type'=>'text','placeholder'=>'YYYY-MM-DD','required']); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('date_to',['label'=>'End Date','type'=>'text','placeholder'=>'YYYY-MM-DD','required']); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('insurance_firm_id', ['options' => $insuranceFirms,'required']);  ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('criteria',['label'=>'Criteria','options'=>['0'=>'Effective Dates','1'=>'Renewal Dates']]); ?></div>
        </fieldset>
        <?= $this->Form->button(__("&nbsp;&nbsp;Generate Report"), 
                ['class'=>'btn btn-md btn-danger fa fa-gears','data-action'=>'refresh_toggle',
                    'data-toggle'=>'block-option']) ?>
        <?= $this->Form->end() ?>        
        <div>&nbsp;</div>
    </div>
</div>
<!-- End Form -->


<script>
    $(document).ready(function()
    {
      var date_input=$('input[name="date_from"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
    
    $(document).ready(function()
    {
      var date_input=$('input[name="date_to"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>