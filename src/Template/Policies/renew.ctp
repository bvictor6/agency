<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Policy $policy
 */
?>

<?= $this->Html->script('jquery-3.2.1.min.js'); ?>
<?= $this->Html->script('bootstrap-datepicker.min.js'); ?>
<?= $this->Html->css('bootstrap-datepicker3.css'); ?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <!--h3 class="h3 text-danger animated zoomIn"><i class="fa fa-plus"></i>&nbsp;Users</h3-->

            <div class="btn-group">    
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                        ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>

                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                        ['controller' => 'CoverTypes', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                        ['controller' => 'InsuranceFirms', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;LOB\'s'), 
                        ['controller' => 'LinesOfBusinesses', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Lines of Business','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Policies</li>
                <li><?= $this->Html->link('Policies',['action'=>'index'] ,['class'=>'link-effect']) ?></li>
                <li><?= $this->Html->link('Renew',['action'=>'renew',$policy->id] ,['class'=>'link-effect']) ?></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Form -->
<div class="block block-bordered">
    <div class="block-header bg-gray-lighter">
        <ul class="block-options">
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
        </ul>
        <h3 class="block-title">Policy <small>&nbsp;Renew</small></h3>
    </div>
    <div class="block-content"> 
        <?= $this->Form->create($policy,["autocomplete"=>"off"])  ?>
        <fieldset>
            <div class="col-sm-6"><?php echo $this->Form->control('name',['label'=>'Policy Number','disabled']); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('client_id',['disabled']);  ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('insurance_firm_id',['disabled']);  ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('line_of_business_id',['disabled']);  ?></div>            
            <div class="col-sm-6"><?php echo $this->Form->control('effective_date',['label'=>'Effective Date','type'=>'text','placeholder'=>'YYYY-MM-DD','required']); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('expiry_date',['label'=>'Expiry Date','type'=>'text','placeholder'=>'YYYY-MM-DD','required']); ?></div>
            <div class="col-sm-6"><?php echo $this->Form->control('status', ['options'=>[
                'active'=>'Active', 'lapsed'=>'Lapsed', 'cancelled'=>'Cancelled'
            ]]);  ?>
            </div>
            
            <div class="col-sm-12"><?php echo $this->Form->control('remarks');  ?></div>
        </fieldset>
        <?= $this->Form->button(__("&nbsp;&nbsp;Renew Policy Details"), 
                ['class'=>'btn btn-md btn-danger si si-refresh','data-action'=>'refresh_toggle',
                    'data-toggle'=>'block-option']) ?>
        <?= $this->Form->end() ?>        
        <div>&nbsp;</div>
    </div>
</div>
<!-- End Form -->

<script>
    $(document).ready(function()
    {
      var date_input=$('input[name="effective_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
    
    $(document).ready(function()
    {
      var date_input=$('input[name="expiry_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>