<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Policy[]|\Cake\Collection\CollectionInterface $policies
 */
?>

<!-- Page Content -->
<div class="row">
       
    <div  class="columns large-12">
         <h3 class="block-title">Policies <small><?= $from ?> To <?= $to ?></small></h3>
        <div>
            <table>
                <thead>
                    <tr>
                        <th class="font-s12">Policy#</th>
                        <th class="font-s12">From</th>
                        <th class="font-s12">Expiry</th>
                        <th class="font-s12">Client</th>                       
                        <th class="font-s12">Insurance</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($policies as $policy): ?>
                    <tr>
                        <td class="font-s12"><?= h($policy->name) ?></td>
                        <td class="font-s12"><?= h($policy->effective_date) ?></td>
                        <td class="font-s12"><?= h($policy->expiry_date) ?></td>
                        <td class="font-s12"><?= $policy->has('client') ? $this->Html->link($policy->client->name, ['controller' => 'Clients', 'action' => 'view', $policy->client->id]) : '' ?></td>                      
                        <td class="font-s12"><?= $policy->has('insurance_firm') ? $this->Html->link($policy->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $policy->insurance_firm->id]) : '' ?></td>
                        
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                        
        </div>
    </div>
</div>
<!-- End Page Content -->
