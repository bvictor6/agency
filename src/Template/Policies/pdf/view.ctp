<?= $this->Html->css('base.css',['fullBase' => true]) ?>
<?= $this->Html->css('style.css',['fullBase' => true]) ?>
<?= $this->Html->css('home.css',['fullBase' => true]) ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">

<div class="row">
    <div class="columns large-12">
        <table>
            <tr>
                <td colspan="2"><h4><?= h($policy->name) ?></h4></td>
            </tr>
            <tr>
                <td>
                    <div class="columns large-6">                    
                        <ul>
                            <li class=""><?= $policy->has('insurance_firm') ? $this->Html->link($policy->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $policy->insurance_firm->id]) : '' ?></li>
                            <li class=""><?= $policy->has('line_of_business') ? $this->Html->link($policy->line_of_business->name, ['controller' => 'LineOfBusinesses', 'action' => 'view', $policy->line_of_business->id]) : '' ?></li>
                            
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="columns large-6">
                        <ul>
                            <li><?= h($policy->effective_date) ?></li>
                            <li><?= h($policy->expiry_date) ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table> 
    </div>      
</div>
<!-- Client Details -->
<div class="row">
    <div class="columns large-12">
        <h4><small>Policy Holder Details</small></h4>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>ID#</th>
                    <th>PIN#</th>
                    <th>Type</th>                    
                </tr>            
            </thead>
            <tbody>
                <tr>
                    <td><?= h(strtoupper($policy->client->name)) ?></td>
                    <td><?= h(strtoupper($policy->client->phone)) ?></td>
                    <td><?= h(strtoupper($policy->client->id_no)) ?></td>
                    <td><?= h(strtoupper($policy->client->pin_no)) ?></td>
                    <td><?= h(strtoupper($policy->client->type)) ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<!-- Related Risks -->

<div class="row">
    <div class="columns large-12">
        <h4><small>Related Risks</small></h4>
        <?php if (!empty($policy->vehicles)): ?>
        <table>
            <thead>
                <tr>
                    <th>Year</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>REG#</th>
                    <th>Color</th>
                    <th>Value</th>
                    <th>Premium</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($policy->vehicles as $vehicles):  ?>
                <tr>
                    <td><?= h($vehicles->year) ?></td>
                    <td><?= h($vehicles->make->name) ?></td>
                    <td><?= h($vehicles->model->name) ?></td>
                    <td><?= h($vehicles->registration_number) ?></td>
                    <td><?= h($vehicles->color->name) ?></td>
                    <td><?= number_format($vehicles->value) ?></td>
                    <td><?= number_format($vehicles->premium) ?></td>

                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
</div>

<!-- End Related Risks -->
<!-- Related Payments -->
<?php if ($opt==0): ?>
<div class="row">
    <div class="columns large-12">
        <h4><small>Related Payments</small></h4>
        <div class="block-content">
            <?php if (!empty($policy->payments)): ?>
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table>
                <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Ref#</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($policy->payments as $payments): ?>
                    <tr>
                        <td hidden="hidden">&nbsp;</td>
                        <td><?= h($payments->amount) ?></td>
                        <td><?= h($payments->payment_date) ?></td>
                        <td><?= h($payments->transaction_ref) ?></td>


                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>    
            <?php endif; ?>
        </div>
    </div>
</div>
<?php endif; ?>
<!-- End Related Payments -->

<!-- Related Documents -->
<div class="row">
    <div class="columns large-12">
        <h4><small>Related Documents</small></h4>
        <?php if (!empty($policy->documents)): ?>
        <table>
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Size</th>                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($policy->documents as $documents): ?>
                <tr>
                    <td><?= h($documents->document_type->name) ?></td>
                    <td><?= h($documents->name) ?></td>
                    <td><?= h($documents->filesize) ?></td>                    
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
</div>
<!-- End Related Documents -->




