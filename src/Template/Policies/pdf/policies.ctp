<?= $this->Html->css('base.css',['fullBase' => true]) ?>
<?= $this->Html->css('style.css',['fullBase' => true]) ?>
<?= $this->Html->css('home.css',['fullBase' => true]) ?>
<link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">


<div class="row">
    <div class="columns large-12">
        <h4><small>POLICIES</small></h4>
        <table>
            <thead>
                <tr>
                    <th class="text-center" style="width: 7%;"></th>                        
                    <th>Policy#</th>
                    <th>From</th>
                    <th>Expiry</th>
                    <th>Status</th>                       
                    <th>Insurance</th>
                </tr>
            </thead>
            <tbody
                <?php foreach ($policies as $policy): ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($policy->id) ?></td>
                    <td><?= h($policy->name) ?></td>
                    <td><?= h($policy->effective_date) ?></td>
                    <td><?= h($policy->expiry_date) ?></td>
                    <td><?= strtoupper($policy->status) ?></td>                      
                    <td><?= $policy->has('insurance_firm') ? $this->Html->link($policy->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $policy->insurance_firm->id]) : '' ?></td>
                    
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
