<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Document $document
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;DOCUMENTS'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Documents','escape'=>false]) ?>
                    

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Underwriting</li>
                <li> <?= $this->Html->link('Documents', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($document->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Details</h3>
    </div>
    <div class="block-content">
        <div class="row items-push">
            <div class="col-sm-6">
                
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600"><?= __('Name') ?></th>
                            <td class="font-w400"><?= h($document->name) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Policy') ?></th>
                            <td class="font-w400"><?= $document->has('policy') ? $this->Html->link($document->policy->name, ['controller' => 'Policies', 'action' => 'view', $document->policy->id]) : '' ?></td>
                        </tr>

                        <tr>
                            <td class="font-w600"><?= __('Mime Type') ?></th>
                            <td class="font-w400"><?= h($document->filemime) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Size') ?></th>
                            <td class="font-w400"><?= $this->Number->format($document->filesize) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Uploaded') ?></th>
                            <td class="font-w400"><?= h($document->date_created) ?></td>
                        </tr>
                                                                        
                    </tbody>
                </table>
                <div class="col-sm-12">
                    <div class="col-sm-3">&nbsp;</div>
                    <div class="col-sm-6">
                        <div class="btn-group">
                            <?= $this->Html->link(__('<i class="fa fa-download"></i>&nbsp;&nbsp;DOWNLOAD DOCUMENT'), 
                                    ['controller' => 'Documents', 'action' => 'downloadFile', $document->id],
                                    ['class'=>'btn btn-sm btn-info animated zoomIn','type'=>'button',
                                                    'data-toggle'=>'tooltip', 'title'=>'Download Policy Documents','escape'=>false]) ?>

                        </div> 
                    </div>             
                    <div class="col-sm-3">&nbsp;</div>
                </div>
            </div>            
        </div>
    </div>    
</div>
<!-- End Paragraphs in Block -->

