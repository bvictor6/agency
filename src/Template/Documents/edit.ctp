<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Document $document
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <!--h3 class="h3 text-danger animated zoomIn"><i class="fa fa-plus"></i>&nbsp;Users</h3-->

            <div class="btn-group">    
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;DOCUMENTS'), 
                        ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Documents','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                        ['controller' => 'Policies', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>

                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                        ['controller' => 'CoverTypes', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                        ['controller' => 'InsuranceFirms', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;LOB\'s'), 
                        ['controller' => 'LinesOfBusinesses', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Lines of Business','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Underwriting</li>
                <li><?= $this->Html->link('Documents',['action'=>'index'] ,['class'=>'link-effect']) ?></li>
                <li><?= $this->Html->link('Edit',['action'=>'edit', $document->id] ,['class'=>'link-effect']) ?></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Form -->
<div class="block block-bordered">
    <div class="block-header bg-gray-lighter">
        <ul class="block-options">
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
        </ul>
        <h3 class="block-title">Document <small>&nbsp;UPLOAD</small></h3>
    </div>
    <div class="block-content"> 
        <?= $this->Form->create($document,['horizontal'=>true,'type'=>'file','autocomplete'=>'off'])  ?>
        <fieldset>
            <div class="col-sm-6"><?php echo $this->Form->control('policy_id', ['options' => $policies]); ?></div>            
                        
        </fieldset>
        <?= $this->Form->button(__("&nbsp;&nbsp;Update Document Details"), 
                ['class'=>'btn btn-md btn-danger fa fa-save','data-action'=>'refresh_toggle',
                    'data-toggle'=>'block-option']) ?>
        <?= $this->Form->end() ?>        
        <div>&nbsp;</div>
    </div>
</div>
<!-- End Form -->
