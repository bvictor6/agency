<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Document[]|\Cake\Collection\CollectionInterface $documents
 */
?>

<!-- Page Content -->
<div>
    <!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <!--h2 class="h2 text-danger animated zoomIn"><i class="fa fa-info-circle"></i></h2-->
                
                <div class="btn-group">  
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;POLICIES'), 
                        ['controller' => 'Policies', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Policies','escape'=>false]) ?>
                    
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;COVER TYPES'), 
                        ['controller' => 'CoverTypes', 'action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Cover Types','escape'=>false]) ?>
                
                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;INSURANCE FIRMS'), 
                            ['controller' => 'InsuranceFirms', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Insurance Firms','escape'=>false]) ?>

                    <?= $this->Html->link(__('<i class="fa fa-bars"></i>&nbsp;&nbsp;LOB\'s'), 
                            ['controller' => 'LinesOfBusinesses', 'action' => 'index'],
                            ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Lines of Business','escape'=>false]) ?>
                </div>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Underwriting</li>
                    <li><a class="link-effect" href="">Documents</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- END Page Header -->
    
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="col-sm-7 block-header">
            <h3 class="block-title">Documents <small>List</small></h3>
        </div>
        <!-- Excel Button -->
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                &nbsp;
            </ol>
        </div>
        <!-- End Excel Button --> 
        <div class="block-content">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
            <table class="table table-bordered table-borderless js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 7%;"></th>
                        <th class="hidden-sm">Name</th>
                        <th class="hidden-sm">Policy#</th>
                        <th class="hidden-sm">Mime Type</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody
                    <?php foreach ($documents as $document): ?>
                    <tr>
                        <td class="text-center"><?= $this->Number->format($document->id) ?></td>
                        <td class="font-w300"><?= h($document->name) ?></td>
                        <td class="hidden-xs"><?= $document->has('policy') ? $this->Html->link($document->policy->name, ['controller' => 'Policies', 'action' => 'view', $document->policy->id]) : '' ?></td>
                        <td class="hidden-xs"><?= h($document->filemime) ?></td>
                                                                                                                         
                        <td class="text-center">
                            <div class="btn-group">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'view', $document->id],
                                        ['class'=>'btn btn-xs btn-info','type'=>'button',
                                            'data-toggle'=>'tooltip', 'title'=>'View Document Details','escape'=>false]) ?>
                                                                                                                                                         
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <!--div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div-->
            
        </div>
    </div>
</div>
<!-- End Page Content -->
