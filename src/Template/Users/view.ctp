<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $user->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit User Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;USERS'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Users','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create New User','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-key"></i>&nbsp;&nbsp;Reset Password'), 
                        ['action' => 'changepwd',$user->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Reset Password','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Admin</li>
                <li> <?= $this->Html->link('Users', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($user->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title">Details</h3>
    </div>
    <div class="block-content">
        <div class="row items-push">
            <div class="col-sm-6">
                
                <table class="table table-hover table-borderless">
                    <tbody>
                        <tr>
                            <td class="font-w600"><?= __('Name') ?></th>
                            <td class="font-w400"><?= h($user->name) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Email') ?></th>
                            <td class="font-w400"><?= h($user->email) ?></td>
                        </tr>

                        <tr>
                            <td class="font-w600"><?= __('Role') ?></th>
                            <td class="font-w400"><?= h($user->role) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Created') ?></th>
                            <td class="font-w400"><?= h($user->date_created) ?></td>
                        </tr>
                        <tr>
                            <td class="font-w600"><?= __('Last Login') ?></th>
                            <td class="font-w400"><?= h($user->last_login) ?></td>
                        </tr>
                                                                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Paragraphs in Block -->

