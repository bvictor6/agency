

<div>
    <div class="col-sm-3">
        &nbsp;
    </div>

    <div class="col-sm-6">
        <!-- Material Login -->
        <div class="block block-themed">
            <div class="block-header bg-primary">
                <ul class="block-options">
                    <li>
                        <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                    </li>
                    <li>
                        <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                    </li>
                </ul>
                <h3 class="block-title">User Login</h3>
            </div>
            <div class="block-content">
                <?= $this->Form->create()  ?>
                <fieldset>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material">
                                <?php echo $this->Form->control('email',['class'=>'form-control','placeholder'=>'Enter your username..']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="form-material">
                                <?php echo $this->Form->control('password',['class'=>'form-control','placeholder'=>'Enter your password..']);  ?>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                    <div class="col-xs-12">
                        <?= $this->Form->button(__("&nbsp;&nbsp;Login"), 
                        ['class'=>'btn btn-md btn-primary fa fa-arrow-right push-5-r','data-action'=>'refresh_toggle',
                            'data-toggle'=>'block-option']) ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
                <div>&nbsp;</div>
            </div>
        </div>
        <!-- END Material Login -->
    </div>


    <div class="col-sm-3">
        &nbsp;
    </div>
</div>

