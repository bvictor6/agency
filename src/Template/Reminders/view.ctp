<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Reminder $reminder
 */
?>


<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">

            <div class="btn-group">
                <?= $this->Html->link(__('<i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT'), 
                        ['action' => 'edit', $reminder->id],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Edit Reminder Details','escape'=>false]) ?>
                
                <?= $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;REMINDERS'), ['action' => 'index'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'View Reminders','escape'=>false]) ?>
                    
                <?= $this->Html->link(__('<i class="fa fa-plus"></i>&nbsp;&nbsp;NEW'), 
                        ['action' => 'add'],
                        ['class'=>'btn btn-xs btn-primary animated zoomIn','type'=>'button',
                                        'data-toggle'=>'tooltip', 'title'=>'Create Reminder','escape'=>false]) ?>

            </div>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>MISC</li>
                <li> <?= $this->Html->link('Reminder', ['action'=>'index']) ?> </li>
                <li> <?= $this->Html->link('View', ['action'=>'view', h($reminder->id)]) ?> </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Paragraphs in Block -->
<div class="block">
    <div class="block-header">
        <h3 class="block-title"><?= h($reminder->name) ?></h3>
    </div>
    <div class="block-content">
        <div class="row items-push">  
            <div class="col-sm-6">
                <table>
        
                    <tr>
                        <td><?= __('Insurance Firm') ?></td>
                        <td><?= $reminder->has('insurance_firm') ? $this->Html->link($reminder->insurance_firm->name, ['controller' => 'InsuranceFirms', 'action' => 'view', $reminder->insurance_firm->id]) : '' ?></td>
                    </tr>
                    <tr>
                        <td><?= __('Status') ?></td>
                        <td><?= h($reminder->status) ?></td>
                    </tr>
                    <tr>
                        <td><?= __('Reminder Date') ?></td>
                        <td><?= h($reminder->reminder_date) ?></td>
                    </tr>

                </table>
            </div>
            <div class="col-sm-6">
                <h5><?= __('Remarks') ?></h5>
                <?= $this->Text->autoParagraph(h($reminder->remarks)); ?>
            </div> 
        </div>
        <!-- End of main content -->
    </div>
</div>
<!-- End Paragraphs in Block -->
