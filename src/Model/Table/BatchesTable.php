<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Batches Model
 *
 * @property \App\Model\Table\InsuranceFirmsTable|\Cake\ORM\Association\BelongsTo $InsuranceFirms
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\CertificatesTable|\Cake\ORM\Association\HasMany $Certificates
 *
 * @method \App\Model\Entity\Batch get($primaryKey, $options = [])
 * @method \App\Model\Entity\Batch newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Batch[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Batch|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Batch patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Batch[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Batch findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BatchesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('batches');
        $this->setDisplayField('batch_no');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('InsuranceFirms', [
            'foreignKey' => 'insurance_firm_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Certificates', [
            'foreignKey' => 'batch_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
        $validator
            ->scalar('batch_no')
            ->maxLength('batch_no', 20)
            ->requirePresence('batch_no', 'create')
            ->notEmpty('batch_no')
            ->add('batch_no', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
        
        $validator
            ->integer('batch_from')
            ->requirePresence('batch_from', 'create')
            ->notEmpty('batch_from');
        
        $validator
            ->integer('batch_to')
            ->requirePresence('batch_to', 'create')
            ->notEmpty('batch_to');

        $validator
            ->date('date_received')
            ->requirePresence('date_received', 'create')
            ->notEmpty('date_received');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['insurance_firm_id'], 'InsuranceFirms'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }
}
