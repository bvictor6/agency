<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Policies Model
 *
 * @property \App\Model\Table\LineOfBusinessesTable|\Cake\ORM\Association\BelongsTo $LineOfBusinesses
 * @property \App\Model\Table\InsuranceFirmsTable|\Cake\ORM\Association\BelongsTo $InsuranceFirms
 * @property \App\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 * @property \App\Model\Table\DocumentsTable|\Cake\ORM\Association\HasMany $Documents
 * @property \App\Model\Table\VehiclesTable|\Cake\ORM\Association\HasMany $Vehicles
 * @property \App\Model\Table\PaymentsTable|\Cake\ORM\Association\HasMany $Payments
 *
 * @method \App\Model\Entity\Policy get($primaryKey, $options = [])
 * @method \App\Model\Entity\Policy newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Policy[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Policy|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Policy patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Policy[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Policy findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PoliciesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('policies');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('LineOfBusinesses', [
            'foreignKey' => 'line_of_business_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('InsuranceFirms', [
            'foreignKey' => 'insurance_firm_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Documents', [
            'foreignKey' => 'policy_id'
        ]);
        $this->hasMany('Vehicles', [
            'foreignKey' => 'policy_id'
        ]);
        
        $this->hasMany('Payments', [
            'foreignKey' => 'policy_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->date('effective_date')
            ->requirePresence('effective_date', 'create')
            ->notEmpty('effective_date');

        $validator
            ->date('expiry_date')
            ->requirePresence('expiry_date', 'create')
            ->notEmpty('expiry_date');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status')
            ->add('status', 'inList',[
                'rule'=>['inList',['active','lapsed','cancelled']],
                'message'=>'Please enter a valid status!'
            ]);

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');
        
        $validator
            ->scalar('remarks')
            ->allowEmpty('remarks');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->existsIn(['line_of_business_id'], 'LineOfBusinesses'));        
        $rules->add($rules->existsIn(['client_id'], 'Clients'));
        $rules->add($rules->existsIn(['insurance_firm_id'], 'InsuranceFirms'));

        return $rules;
    }
}
