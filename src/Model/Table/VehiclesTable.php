<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vehicles Model
 *
 * @property \App\Model\Table\PoliciesTable|\Cake\ORM\Association\BelongsTo $Policies
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\ModelsTable|\Cake\ORM\Association\BelongsTo $Models
 * @property \App\Model\Table\MakesTable|\Cake\ORM\Association\BelongsTo $Makes
 * @property \App\Model\Table\ColorsTable|\Cake\ORM\Association\BelongsTo $Colors
 * @property \App\Model\Table\ClaimsTable|\Cake\ORM\Association\HasMany $Claims
 * @property \App\Model\Table\PaymentsTable|\Cake\ORM\Association\HasMany $Payments
 * @property \App\Model\Table\CertificatesTable|\Cake\ORM\Association\HasMany $Certificates
 * @property \App\Model\Table\ExtraCoversTable|\Cake\ORM\Association\HasMany $ExtraCovers
 * @property \App\Model\Table\CoverTypesTable|\Cake\ORM\Association\BelongsTo $CoverTypes
 *
 * @method \App\Model\Entity\Vehicle get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vehicle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vehicle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vehicle|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vehicle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vehicle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vehicle findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VehiclesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vehicles');
        $this->setDisplayField('registration_number');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Policies', [
            'foreignKey' => 'policy_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Models', [
            'foreignKey' => 'model_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Makes', [
            'foreignKey' => 'make_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Colors', [
            'foreignKey' => 'color_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CoverTypes', [
            'foreignKey' => 'cover_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Claims', [
            'foreignKey' => 'vehicle_id'
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'vehicle_id'
        ]);
        
        $this->hasMany('Certificates', [
            'foreignKey' => 'vehicle_id'
        ]);
        
        $this->hasMany('ExtraCovers', [
            'foreignKey' => 'vehicle_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
        $validator
            ->numeric('value')
            ->requirePresence('value', 'create')
            ->notEmpty('value');
        
        $validator
            ->numeric('premium')
            ->requirePresence('premium', 'create')
            ->notEmpty('premium');
        
        $validator
            ->date('effective_date')
            ->requirePresence('effective_date', 'create')
            ->notEmpty('effective_date');

        $validator
            ->date('expiry_date')
            ->requirePresence('expiry_date', 'create')
            ->notEmpty('expiry_date');

        $validator
            ->integer('year')
            ->requirePresence('year', 'create')
            ->notEmpty('year');

        $validator
            ->scalar('registration_number')
            ->maxLength('registration_number', 15)
            ->requirePresence('registration_number', 'create')
            ->notEmpty('registration_number')
            ->add('registration_number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('chasis_number')
            ->maxLength('chasis_number', 20)
            ->allowEmpty('chasis_number');

        $validator
            ->scalar('engine_number')
            ->maxLength('engine_number', 20)
            ->allowEmpty('engine_number');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['registration_number']));
        $rules->add($rules->existsIn(['policy_id'], 'Policies'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['model_id'], 'Models'));
        $rules->add($rules->existsIn(['make_id'], 'Makes'));
        $rules->add($rules->existsIn(['color_id'], 'Colors'));
        $rules->add($rules->existsIn(['cover_type_id'], 'CoverTypes'));
        return $rules;
    }
}
