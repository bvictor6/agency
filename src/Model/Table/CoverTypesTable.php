<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CoverTypes Model
 *
 * @property \App\Model\Table\LineOfBusinessesTable|\Cake\ORM\Association\BelongsTo $LineOfBusinesses
 * @property \App\Model\Table\VehiclesTable|\Cake\ORM\Association\HasMany $Vehicles
 * @property \App\Model\Table\RisksTable|\Cake\ORM\Association\HasMany $Risks
 *
 * @method \App\Model\Entity\CoverType get($primaryKey, $options = [])
 * @method \App\Model\Entity\CoverType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CoverType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CoverType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CoverType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CoverType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CoverType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CoverTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cover_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('LineOfBusinesses', [
            'foreignKey' => 'line_of_business_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Vehicles', [
            'foreignKey' => 'cover_type_id'
        ]);
        $this->hasMany('Risks', [
            'foreignKey' => 'cover_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 20)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['line_of_business_id'], 'LineOfBusinesses'));

        return $rules;
    }
}
