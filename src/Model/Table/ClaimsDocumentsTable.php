<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimsDocuments Model
 *
 * @property \App\Model\Table\DocumentTypesTable|\Cake\ORM\Association\BelongsTo $DocumentTypes
 * @property \App\Model\Table\ClaimsTable|\Cake\ORM\Association\BelongsTo $Claims
 *
 * @method \App\Model\Entity\ClaimsDocument get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimsDocument newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimsDocument[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimsDocument|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimsDocument patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimsDocument[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimsDocument findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClaimsDocumentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claims_documents');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('DocumentTypes', [
            'foreignKey' => 'document_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('filesize')
            ->requirePresence('filesize', 'create')
            ->notEmpty('filesize');

        $validator
            ->scalar('filemime')
            ->maxLength('filemime', 45)
            ->allowEmpty('filemime');

        $validator
            ->scalar('path')
            ->maxLength('path', 255)
            ->requirePresence('path', 'create')
            ->notEmpty('path');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['document_type_id'], 'DocumentTypes'));
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));

        return $rules;
    }
}
