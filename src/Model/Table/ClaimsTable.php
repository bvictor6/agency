<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Claims Model
 *
 * @property \App\Model\Table\VehiclesTable|\Cake\ORM\Association\BelongsTo $Vehicles
 * @property \App\Model\Table\PoliciesTable|\Cake\ORM\Association\BelongsTo $Policies
 * @property \App\Model\Table\GaragesTable|\Cake\ORM\Association\BelongsTo $Garages
 * @property \App\Model\Table\ClaimsDocumentsTable|\Cake\ORM\Association\HasMany $ClaimsDocuments
 *
 * @method \App\Model\Entity\Claim get($primaryKey, $options = [])
 * @method \App\Model\Entity\Claim newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Claim[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Claim|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Claim patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Claim[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Claim findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClaimsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claims');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Vehicles', [
            'foreignKey' => 'vehicle_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Policies', [
            'foreignKey' => 'policy_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Garages', [
            'foreignKey' => 'garage_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('ClaimsDocuments', [
            'foreignKey' => 'claim_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
        $validator
            ->scalar('claim_no')
            ->maxLength('claim_no', 45)
            ->requirePresence('claim_no', 'create')
            ->notEmpty('claim_no')
            ->add('claim_no', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->dateTime('accident_date')
            ->requirePresence('accident_date', 'create')
            ->notEmpty('accident_date');
        
        $validator
            ->dateTime('date_reported')
            ->requirePresence('date_reported', 'create')
            ->notEmpty('date_reported');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['claim_no']));
        $rules->add($rules->existsIn(['vehicle_id'], 'Vehicles'));
        $rules->add($rules->existsIn(['policy_id'], 'Policies'));
        $rules->add($rules->existsIn(['garage_id'], 'Garages'));

        return $rules;
    }
}
