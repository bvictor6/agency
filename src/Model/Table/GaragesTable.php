<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Garages Model
 *
 * @property \App\Model\Table\ClaimsTable|\Cake\ORM\Association\HasMany $Claims
 *
 * @method \App\Model\Entity\Garage get($primaryKey, $options = [])
 * @method \App\Model\Entity\Garage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Garage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Garage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Garage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Garage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Garage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GaragesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('garages');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Claims', [
            'foreignKey' => 'garage_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmpty('name');
        
        $validator
            ->scalar('contact_person')
            ->allowEmpty('contact_person');
        
        $validator
            ->scalar('email')
            ->allowEmpty('email');
        
        $validator
            ->scalar('phone')
            ->allowEmpty('phone');
        
        $validator
            ->scalar('pin_no')
            ->allowEmpty('pin_no');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        return $validator;
    }
}
