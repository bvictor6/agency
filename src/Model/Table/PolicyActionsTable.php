<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PolicyActions Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Policies
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\PolicyAction get($primaryKey, $options = [])
 * @method \App\Model\Entity\PolicyAction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PolicyAction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PolicyAction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PolicyAction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PolicyAction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PolicyAction findOrCreate($search, callable $callback = null, $options = [])
 */
class PolicyActionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('policy_actions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Policies', [
            'foreignKey' => 'policy_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('action')
            ->maxLength('action', 55)
            ->requirePresence('action', 'create')
            ->notEmpty('action');

        $validator
            ->scalar('remarks')
            ->requirePresence('remarks', 'create')
            ->notEmpty('remarks');

        $validator
            ->dateTime('action_date')
            ->requirePresence('action_date', 'create')
            ->notEmpty('action_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['policy_id'], 'Policies'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
