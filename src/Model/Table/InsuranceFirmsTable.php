<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InsuranceFirms Model
 *
 * @method \App\Model\Entity\InsuranceFirm get($primaryKey, $options = [])
 * @method \App\Model\Entity\InsuranceFirm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InsuranceFirm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InsuranceFirm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InsuranceFirm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InsuranceFirm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InsuranceFirm findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InsuranceFirmsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('insurance_firms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        
        $this->hasMany('Policies', [
            'foreignKey' => 'insurance_firm_id'
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('tel')
            ->maxLength('tel', 15)
            ->allowEmpty('tel');
        
        $validator
            ->scalar('code')
            ->maxLength('code', 5)
            ->allowEmpty('code');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('address')
            ->allowEmpty('address');

        $validator
            ->scalar('location')
            ->maxLength('location', 20)
            ->allowEmpty('location');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
