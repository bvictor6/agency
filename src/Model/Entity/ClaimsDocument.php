<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimsDocument Entity
 *
 * @property int $id
 * @property string $name
 * @property int $document_type_id
 * @property int $claim_id
 * @property int $filesize
 * @property string $filemime
 * @property string $path
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\DocumentType $document_type
 * @property \App\Model\Entity\Claim $claim
 */
class ClaimsDocument extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'document_type_id' => true,
        'claim_id' => true,
        'filesize' => true,
        'filemime' => true,
        'path' => true,
        'date_created' => true,
        'modified' => true,
        'document_type' => true,
        'claim' => true
    ];
}
