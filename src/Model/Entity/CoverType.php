<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CoverType Entity
 *
 * @property int $id
 * @property string $name
 * @property int $line_of_business_id
 * @property string $description
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\LineOfBusiness $line_of_business
 * @property \App\Model\Entity\Risk $risk
 * @property \App\Model\Entity\Vehicle[] $vehicles
 */
class CoverType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'line_of_business_id' => true,
        'description' => true,
        'date_created' => true,
        'modified' => true,
        'line_of_business' => true,
        'vehicles' => true
    ];
}
