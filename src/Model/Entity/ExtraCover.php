<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ExtraCover Entity
 *
 * @property int $id
 * @property int $cover_type_id
 * @property int $vehicle_id
 * @property float $amount
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CoverType $cover_type
 * @property \App\Model\Entity\Vehicle $vehicle
 * @property \App\Model\Entity\User $user
 */
class ExtraCover extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cover_type_id' => true,
        'vehicle_id' => true,
        'amount' => true,
        'user_id' => true,
        'date_created' => true,
        'modified' => true,
        'cover_type' => true,
        'vehicle' => true,
        'user' => true
    ];
}
