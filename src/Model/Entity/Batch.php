<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Batch Entity
 *
 * @property int $id
 * @property string $batch_no
 * @property int $insurance_firm_id
 * @property int $category_id
 * @property int $batch_from
 * @property int $batch_to
 * @property \Cake\I18n\FrozenDate $date_received
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\InsuranceFirm $insurance_firm
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Certificate[] $certificates
 */
class Batch extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'batch_no' => true,
        'insurance_firm_id' => true,
        'category_id' => true,
        'batch_from' => true,
        'batch_to' => true,
        'date_received' => true,
        'date_created' => true,
        'modified' => true,
        'insurance_firm' => true,
        'category' => true,
        'certificates' => true
    ];
}
