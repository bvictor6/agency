<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Document Entity
 *
 * @property int $id
 * @property string $name
 * @property int $document_type_id
 * @property int $policy_id
 * @property int $filesize
 * @property string $filemime
 * @property string $path
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Policy $policy
 * @property \App\Model\Entity\DucumentType $document_type
 */
class Document extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'document_type_id' => true,
        'policy_id' => true,
        'filesize' => true,
        'filemime' => true,
        'path' => true,
        'date_created' => true,
        'modified' => true,
        'policy' => true
    ];
}
