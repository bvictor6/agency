<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Policy Entity
 *
 * @property int $id
 * @property string $name
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $effective_date
 * @property \Cake\I18n\FrozenTime $expiry_date
 * @property bool $status
 * @property int $line_of_business_id
 * @property int $client_id
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\LineOfBusiness $line_of_business
 * @property \App\Model\Entity\CoverType $cover_type
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\Document[] $documents
 * @property \App\Model\Entity\Vehicle[] $vehicles
 * @property \App\Model\Entity\Payment[] $payments
 */
class Policy extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'effective_date' => true,
        'expiry_date' => true,
        'status' => true,
        'insurance_firm_id' => true,
        'line_of_business_id' => true,
        'client_id' => true,
        'date_created' => true,
        'modified' => true,
        'line_of_business' => true,
        'cover_type' => true,
        'documents' => true,
        'vehicles' => true,
        'payments' => true,
        'remarks' => true
    ];
}
