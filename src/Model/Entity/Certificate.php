<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Certificate Entity
 *
 * @property int $id
 * @property int $serial_no
 * @property int $vehicle_id
 * @property int $category_id
 * @property int $batch_id
 * @property int $insurance_firm_id
 * @property string $batch_no
 * @property bool $status
 * @property bool $printed
 * @property \Cake\I18n\FrozenTime $date_received
 * @property \Cake\I18n\FrozenTime $date_from
 * @property \Cake\I18n\FrozenTime $date_to
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Vehicle $vehicle
 * @property \App\Model\Entity\InsuranceFirm $insurance_firm
 */
class Certificate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'serial_no' => true,
        'vehicle_id' => true,
        'insurance_firm_id' => true,
        'category_id'=>true,
        'batch_id'=>true,
        'batch_no' => true,
        'status' => true,
        'printed' => true,
        'date_created' => true,
        'date_from' => true,
        'date_to' => true,
        'date_received' => true,
        'modified' => true,
        'vehicle' => true,
        'insurance_firm' => true
    ];
}
