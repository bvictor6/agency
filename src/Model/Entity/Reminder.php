<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reminder Entity
 *
 * @property int $id
 * @property string $name
 * @property int $insurance_firm_id
 * @property \Cake\I18n\FrozenDate $reminder_date
 * @property string $status
 * @property string $remarks
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\InsuranceFirm $insurance_firm
 * @property \App\Model\Entity\User $user
 */
class Reminder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'insurance_firm_id' => true,
        'reminder_date' => true,
        'status' => true,
        'remarks' => true,
        'user_id' => true,
        'date_created' => true,
        'modified' => true,
        'insurance_firm' => true,
        'user' => true
    ];
}
