<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vehicle Entity
 *
 * @property int $id
 * @property int $policy_id
 * @property float $value
 * @property float $premium
 * @property int $cover_type_id
 * @property int $category_id
 * @property \Cake\I18n\FrozenTime $effective_date
 * @property \Cake\I18n\FrozenTime $expiry_date
 * @property int $year
 * @property int $make_id
 * @property int $model_id
 * @property string $registration_number
 * @property int $color_id
 * @property string $chasis_number
 * @property string $engine_number
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Policy $policy
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Model $model
 * @property \App\Model\Entity\Make $make
 * @property \App\Model\Entity\Color $color
 * @property \App\Model\Entity\Claim[] $claims
 * @property \App\Model\Entity\Payment[] $payments
 * @property \App\Model\Entity\Certificate[] $certificates
 * @property \App\Model\Entity\CoverType $cover_type
 */
class Vehicle extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'policy_id' => true,
        'value' => true,
        'premium' => true,
        'effective_date' => true,
        'expiry_date' => true,
        'cover_type_id' => true,
        'category_id' => true,
        'year' => true,
        'make_id' => true,
        'model_id' => true,
        'registration_number' => true,
        'color_id' => true,
        'chasis_number' => true,
        'engine_number' => true,
        'date_created' => true,
        'modified' => true,
        'policy' => true,
        'claims' => true,
        'payments' => true,
        'certificates'=>true,
        'color'=>true,
        'make'=>true,
        'model'=>true
    ];
}
