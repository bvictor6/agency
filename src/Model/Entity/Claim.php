<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Claim Entity
 *
 * @property int $id
 * @property int $vehicle_id
 * @property int $policy_id
 * @property int $garage_id
 * @property \Cake\I18n\FrozenTime $accident_date
 * @property \Cake\I18n\FrozenTime $date_reported
 * @property \Cake\I18n\FrozenTime $date_created
 * @property string $description
 * @property string $claim_no
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Vehicle $vehicle
 * @property \App\Model\Entity\Policy $policy
 * @property \App\Model\Entity\Garage $garage
 * @property \App\Model\Entity\ClaimsDocument[] $claimsDocuments
 */
class Claim extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_no'=>true,
        'vehicle_id' => true,
        'policy_id' => true,
        'garage_id' => true,
        'accident_date' => true,
        'date_reported' => true,
        'date_created' => true,
        'description' => true,
        'garage' => true,
        'modified' => true,
        'vehicle' => true,
        'policy'=>true,
        'garage' => true,
        'claims_documents'=>true
    ];
}
