<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Garage Entity
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $contact_person
 * @property string $pin_no
 * @property string $description
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Claim[] $claims
 */
class Garage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'phone'=>true,
        'email'=>true,
        'contact_person'=>true,
        'pin_no'=>true,
        'description' => true,
        'date_created' => true,
        'modified' => true,
        'claims' => true
    ];
}
