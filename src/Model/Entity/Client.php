<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity
 *
 * @property int $id
 * @property string $name
 * @property string $gender
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $city
 * @property string $id_no
 * @property string $nationality
 * @property string $type
 * @property string $pin_no
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Client extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'gender' => true,
        'email' => true,
        'phone' => true,
        'address' => true,
        'city' => true,
        'id_no' => true,
        'pin_no' => true,
        'type' => true,
        'nationality' => true,
        'date_created' => true,
        'modified' => true
    ];
}
