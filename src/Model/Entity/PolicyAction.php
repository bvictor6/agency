<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PolicyAction Entity
 *
 * @property int $id
 * @property int $policy_id
 * @property string $action
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $action_date
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class PolicyAction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'policy_id' => true,
        'action' => true,
        'remarks' => true,
        'action_date' => true,
        'user_id' => true,
        'user' => true
    ];
}
