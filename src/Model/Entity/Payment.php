<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property int $id
 * @property int $policy_id
 * @property int $vehicle_id
 * @property float $amount
 * @property string $transaction_ref
 * @property string $payment_method
 * @property \Cake\I18n\FrozenDate $payment_date
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Policy $policy
 * @property \App\Model\Entity\Vehicle $vehicle
 */
class Payment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'policy_id' => true,
        'vehicle_id' => true,
        'amount' => true,
        'transaction_ref' => true,
        'payment_method' => true,
        'payment_date' => true,
        'remarks' => true,
        'date_created' => true,
        'modified' => true,
        'policy' => true,
        'vehicle' => true
    ];
}
