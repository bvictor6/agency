<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Vehicles Controller
 *
 * @property \App\Model\Table\VehiclesTable $Vehicles
 *
 * @method \App\Model\Entity\Vehicle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VehiclesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Policies','CoverTypes']
        ];*/
        $vehicles = $this->Vehicles->find('all',[
            'contain' => ['Policies','CoverTypes','Colors','Makes','Models']
        ]);
        //die(print_r($vehicles));
        $this->set(compact('vehicles'));
    }

    /**
     * View method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vehicle = $this->Vehicles->get($id, [
            'contain' => ['Policies','Claims', 'CoverTypes','Certificates','Categories','Payments',
                'Colors','Makes','Models','ExtraCovers']
        ]);
        
        $this->set('vehicle', $vehicle);
        
        /****/
        //die(($vehicle['category']));
        $certificates = $this->Vehicles->Certificates->find('all',[
            'conditions'=>[
                'insurance_firm_id'=> $vehicle['policy']['insurance_firm_id'],
                'category_id'=>$vehicle['category']["id"],
                'status'=>0],
            'fields'=>['id','serial_no','status'],
            'recursive'=>1
        ]);
        $this->set('certificates',$certificates);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id=null,$edate = null)
    {
        $vehicle = $this->Vehicles->newEntity();
        if ($this->request->is('post')) 
        {
            $vehicle = $this->Vehicles->patchEntity($vehicle, $this->request->getData());
            $vehicle->date_created = date(DATE_ATOM);
            $vehicle->effective_date = $this->request->getData('effective_date');
            $vehicle->expiry_date = $this->request->getData('expiry_date');
            $vehicle->user_id = $this->request->session()->read('Config.uid');
            if($vehicle->value<=0)
            {
               $this->Flash->error(__('Invalid vehicle value provided, must be more than 0. Please, try again.')); 
            }
            elseif ($vehicle->premium<=0) 
            {
                $this->Flash->error(__('Invalid vehicle premium provided, must be more than 0. Please, try again.'));
            }
            else {
                if ($this->Vehicles->save($vehicle)) 
                {
                    $this->Flash->success(__('The vehicle has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
				//die(print_r($vehicle->errors()));
                $this->Flash->error(__('The vehicle could not be saved. Please, try again.'));
            }
            
        }//die($edate);
        $a = strpos($edate, '_');
        $b = strrpos($edate, '_');
        $mon = substr($edate, 0,$a);
        $dd = substr($edate,$a+1, ($b-$a)-1);
        $yr = substr($edate, $b+1);
        //die($yr.'-'.$mon.'-'.$dd);
        $vehicle->expiry_date = date('20'.$yr.'-'.$mon.'-'.$dd);
        $policies = $this->Vehicles->Policies->find('list', ['limit' => 1])->where(['id'=>$id]);
        $coverTypes = $this->Vehicles->CoverTypes->find('list', ['limit' => 200]);
        $categories = $this->Vehicles->Categories->find('list', ['limit' => 200]);
        $models = $this->Vehicles->Models->find('list', ['limit' => 200]);
        $makes = $this->Vehicles->Makes->find('list', ['limit' => 200]);
        $colors = $this->Vehicles->Colors->find('list', ['limit' => 200]);
        $this->set(compact('vehicle', 'policies', 'coverTypes','categories','makes','models','colors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vehicle = $this->Vehicles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $vehicle = $this->Vehicles->patchEntity($vehicle, $this->request->getData());
            $vehicle->effective_date = $this->request->getData('effective_date');
            $vehicle->expiry_date = $this->request->getData('expiry_date');
            if ($this->Vehicles->save($vehicle)) {
                $this->Flash->success(__('The vehicle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vehicle could not be saved. Please, try again.'));
        }
        $vehicle->expiry_date = date('Y-m-d', strtotime($vehicle->expiry_date));
        $vehicle->effective_date = date('Y-m-d', strtotime($vehicle->effective_date));
        $policies = $this->Vehicles->Policies->find('list', ['limit' => 200]);
        $coverTypes = $this->Vehicles->CoverTypes->find('list', ['limit' => 200]);
        $categories = $this->Vehicles->Categories->find('list', ['limit' => 200]);
        $models = $this->Vehicles->Models->find('list', ['limit' => 200]);
        $makes = $this->Vehicles->Makes->find('list', ['limit' => 200]);
        $colors = $this->Vehicles->Colors->find('list', ['limit' => 200]);
        $this->set(compact('vehicle', 'policies', 'coverTypes','categories','makes','models','colors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vehicle = $this->Vehicles->get($id);
        if ($this->Vehicles->delete($vehicle)) {
            $this->Flash->success(__('The vehicle has been deleted.'));
        } else {
            $this->Flash->error(__('The vehicle could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /***
     * Reports
     */
    
    public function risks() 
    {
        $vehicles = $this->Vehicles->find('all',[
            'contain' => ['Policies','CoverTypes','Categories']
        ]);
        $this->pdfConfig = 
        [
            'margin' => [
            'bottom' => 15,
            'left' => 10,
            'right' => 10,
            'top' => 45],
            'orientation' => 'portrait',
            'filename' => 'VEHICLES_LIST'.'.pdf' 
        ];
        $this->set('vehicles', $vehicles);
    }
}
