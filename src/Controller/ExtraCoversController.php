<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ExtraCovers Controller
 *
 * @property \App\Model\Table\ExtraCoversTable $ExtraCovers
 *
 * @method \App\Model\Entity\ExtraCover[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExtraCoversController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['CoverTypes', 'Vehicles', 'Users']
        ];
        $extraCovers = $this->paginate($this->ExtraCovers);

        $this->set(compact('extraCovers'));
    }

    /**
     * View method
     *
     * @param string|null $id Extra Cover id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $extraCover = $this->ExtraCovers->get($id, [
            'contain' => ['CoverTypes', 'Vehicles', 'Users']
        ]);

        $this->set('extraCover', $extraCover);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        $extraCover = $this->ExtraCovers->newEntity();
        if ($this->request->is('post')) 
        {
            $extraCover = $this->ExtraCovers->patchEntity($extraCover, $this->request->getData());
            $extraCover->date_created = date(DATE_ATOM);
            $extraCover->user_id = $this->request->session()->read('Config.uid');
            if ($this->ExtraCovers->save($extraCover)) 
            {
                $this->Flash->success(__('The extra cover has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The extra cover could not be saved. Please, try again.'));
        }
        $coverTypes = $this->ExtraCovers->CoverTypes->find('list', ['limit' => 200]);
        $vehicles = $this->ExtraCovers->Vehicles->find('list', ['limit' => 200])->where(['id'=>$id]);
        $users = $this->ExtraCovers->Users->find('list', ['limit' => 200]);
        $this->set(compact('extraCover', 'coverTypes', 'vehicles', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Extra Cover id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $extraCover = $this->ExtraCovers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $extraCover = $this->ExtraCovers->patchEntity($extraCover, $this->request->getData());
            if ($this->ExtraCovers->save($extraCover)) {
                $this->Flash->success(__('The extra cover has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The extra cover could not be saved. Please, try again.'));
        }
        $coverTypes = $this->ExtraCovers->CoverTypes->find('list', ['limit' => 200]);
        $vehicles = $this->ExtraCovers->Vehicles->find('list', ['limit' => 200])->where(['id'=>$extraCover->vehicle_id]);
        $users = $this->ExtraCovers->Users->find('list', ['limit' => 200]);
        $this->set(compact('extraCover', 'coverTypes', 'vehicles', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Extra Cover id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $extraCover = $this->ExtraCovers->get($id);
        if ($this->ExtraCovers->delete($extraCover)) {
            $this->Flash->success(__('The extra cover has been deleted.'));
        } else {
            $this->Flash->error(__('The extra cover could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
