<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Policies Controller
 *
 * @property \App\Model\Table\PoliciesTable $Policies
 *
 * @method \App\Model\Entity\Policy[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PoliciesController extends AppController
{
    
    public function initialize() 
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->_validViewOptions[] = 'pdfConfig';
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['InsuranceFirms']
        ];
        $policies = $this->paginate($this->Policies);*/
        $policies = $this->Policies->find('all',[
            'contain' => ['InsuranceFirms']
        ]);

        $this->set(compact('policies'));
    }

    /**
     * View method
     *
     * @param string|null $id Policy id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null,$opt = 0)
    {
        $policy = $this->Policies->get($id, [
            'contain' => ['LineOfBusinesses', 'Documents'=>['DocumentTypes'], 
                'Vehicles'=>['Makes','Models','Colors'], 
                'InsuranceFirms',
                'Clients','Payments']
        ]);
        
        $this->pdfConfig = 
        [
            'margin' => [
            'bottom' => 15,
            'left' => 10,
            'right' => 10,
            'top' => 15],
            'orientation' => 'portrait',
            'filename' => 'POLICY_'.$policy['name'].'.pdf' 
        ];
        $this->set('opt',$opt);
        $this->set('policy', $policy);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $policy = $this->Policies->newEntity();
        if ($this->request->is('post')) 
        {
            $policy = $this->Policies->patchEntity($policy, $this->request->getData());
            $policy->effective_date = $this->request->getData("effective_date");
            $policy->expiry_date = $this->request->getData("expiry_date");
            $policy->underwriting_year = date('Y');
            $policy->date_created = date(DATE_ATOM);
            $policy->user_id = $this->request->session()->read('Config.uid');
            if ($this->Policies->save($policy)) {
                $this->Flash->success(__('The policy has been saved.'));
                $id = $policy->id;
                //debug($id);
                return $this->redirect(['action' => 'view',$id]);
            }
            $this->Flash->error(__('The policy could not be saved. Please, try again.'));
        }
        $lineOfBusinesses = $this->Policies->LineOfBusinesses->find('list', ['limit' => 200]);        
        $clients = $this->Policies->Clients->find('list', ['limit' => 200]);
        $insuranceFirms = $this->Policies->InsuranceFirms->find('list', ['limit' => 200]);
        $this->set(compact('policy', 'lineOfBusinesses', 'insuranceFirms', 'clients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Policy id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $policy = $this->Policies->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $policy = $this->Policies->patchEntity($policy, $this->request->getData());
            $policy->effective_date = $this->request->getData("effective_date");
            $policy->expiry_date = $this->request->getData("expiry_date");
            $action = $this->request->getData("status");
            if ($this->Policies->save($policy)) 
            {                                
                $this->Flash->success(__('The policy has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The policy could not be saved. Please, try again.'));
        }
        $policy->effective_date = date('Y-m-d',  strtotime($policy->effective_date));
        $policy->expiry_date = date('Y-m-d', strtotime($policy->expiry_date));
        $lineOfBusinesses = $this->Policies->LineOfBusinesses->find('list', ['limit' => 200]);        
        $clients = $this->Policies->Clients->find('list', ['limit' => 200]);
        $insuranceFirms = $this->Policies->InsuranceFirms->find('list', ['limit' => 200]);
        $this->set(compact('policy', 'lineOfBusinesses', 'insuranceFirms','clients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Policy id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $policy = $this->Policies->get($id);
        if ($this->Policies->delete($policy)) {
            $this->Flash->success(__('The policy has been deleted.'));
        } else {
            $this->Flash->error(__('The policy could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function renew($id =  null) 
    {
        
        $policy = $this->Policies->get($id, [
            'contain' => []
        ]);
        
        $renewal_date = date('Y-m-d',  strtotime($policy->expiry_date.' +1 day'));
        $exp_date = date('Y-m-d', strtotime($policy->expiry_date.' +1 year'));
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $policy = $this->Policies->patchEntity($policy, $this->request->getData());
            $policy->effective_date = $this->request->getData("effective_date");
            $policy->expiry_date = $this->request->getData("expiry_date");
            $policy->underwriting_year = date('Y');
            $policy->user_id = $this->request->session()->read('Config.uid');
            if(strtotime($renewal_date) > strtotime($this->request->getData("effective_date")))
            {
                $this->Flash->error(__('Renewal date must be greater or equal to '.$renewal_date.' . Please, try again.'));
            }
            elseif (strtotime($this->request->getData("effective_date")) > strtotime($this->request->getData("expiry_date"))) 
            {
                $this->Flash->error(__('Policy expiry date cannot be earlier than start date!'));
            }
            else
            {
                if($this->archivePolicy($id))
                {
                    if ($this->Policies->save($policy)) 
                    {
                        //Update vehicles linked to policy to have the new renwal and expiry date
                        $v_renewal_date = $this->request->getData("effective_date");
                        $v_expiry_date = $this->request->getData("expiry_date");

                        $vehicles = \Cake\ORM\TableRegistry::get('Vehicles');// TableRegistry::get('Vehicles');
                        $vehicle = $vehicles->find('all')->where(['policy_id' => $id])->first();

                        $vehicle->effective_date = $v_renewal_date;
                        $vehicle->expiry_date = $v_expiry_date;
                        if($vehicles->save($vehicle))
                        {                        
                            $this->Flash->success(__('The policy has been renewed succesfully.'));

                            return $this->redirect(['action' => 'view', $id]);
                        }
                        $this->Flash->error(__('The policy has been renewed but vehicle details failed to renew - You need to change this manually!'));
                    }
                }                
            }
            /**/
            $this->Flash->error(__('The policy could not be renewed. Please, try again.'));
        }
        $policy->effective_date = date('Y-m-d',  strtotime($policy->expiry_date.' +1 day'));
        $policy->expiry_date = date('Y-m-d', strtotime($policy->expiry_date.' +1 year'));
        $end = date('Y-m-d', strtotime('+5 years'));
        $lineOfBusinesses = $this->Policies->LineOfBusinesses->find('list', ['limit' => 200]);        
        $clients = $this->Policies->Clients->find('list', ['limit' => 200]);
        $insuranceFirms = $this->Policies->InsuranceFirms->find('list', ['limit' => 200]);
        $this->set(compact('policy', 'lineOfBusinesses', 'insuranceFirms','clients'));
        
        /**/
    }
    
    private function archivePolicy($id = null)
    {
        $this->autoRender = false;
        $connection = \Cake\Datasource\ConnectionManager::get('default');
        $status = 0;
        echo "---init renew policy\r\n";
        $stmt = $connection->execute('INSERT INTO old_policies SELECT * FROM policies WHERE policies.id='.$id);
        //echo "Policy info duplicated!";
        $status = 1;
        $stmt = $connection->execute('INSERT INTO old_vehicles SELECT * FROM vehicles WHERE vehicles.policy_id='.$id);
        //echo "Vehicle info duplicated!";  
        $status = 2;
        if($status>0){
            return true;
        } else{
            return false;
        }
    }
    
    public function cancel($id = null) 
    {
        
    }

        /**
     * Reports
     */
    
    public function policies() 
    {
        $policies = $this->Policies->find('all',[
            'contain' => ['InsuranceFirms']
        ]);
        $this->pdfConfig = 
        [
            'margin' => [
            'bottom' => 15,
            'left' => 10,
            'right' => 10,
            'top' => 45],
            'orientation' => 'portrait',
            'filename' => 'POLICIES_LIST'.'.pdf' 
        ];
        $this->set('policies', $policies);
    }
    
    public function dateparams() 
    {
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'policybydates', 
                $this->request->getData('criteria'),
                $this->request->getData('date_from'),
                $this->request->getData('date_to')]);
        }        
    }
    
    public function firmparams() 
    {
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'policiesbyfirm', 
                        $this->request->getData('criteria'),
                        $this->request->getData('insurance_firm_id'),
                        $this->request->getData('date_from'),
                        $this->request->getData('date_to')]);
        }    
        $insuranceFirms = $this->Policies->InsuranceFirms->find('list', ['limit' => 200]);
        $this->set(compact('policy','insuranceFirms'));
    }
    
    public function policybydates($c,$from,$to) 
    {
        if($c==0)
        {
            $policies = $this->Policies->find('all',[
                'contain' => ['InsuranceFirms','Clients']
            ])->where(['effective_date>='."'".$from."'",'effective_date<='."'".$to."'"]);
            
            $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Certificates.pdf' 
                    ];
            
            $this->set('c',$c);
            $this->set('from',$to);
            $this->set('to',$from);
            
            $this->set(compact('policies'));
        }
        else
        {
            $policies = $this->Policies->find('all',[
                'contain' => ['InsuranceFirms','Clients']
            ])->where(['expiry_date>='."'".$from."'",'expiry_date<='."'".$to."'"]);
            
            $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Certificates.pdf' 
                    ];
            
            $this->set('c',$c);
            $this->set('from',$to);
            $this->set('to',$from);
            
            $this->set(compact('policies'));
        }
        
    }
    
    public function policiesbyfirm($c=null,$id=null,$from=null,$to=null) 
    {
        if($c==0)
        {
            $policies = $this->Policies->find('all',[
                'contain' => ['InsuranceFirms','Clients']
            ])->where([
                'Policies.insurance_firm_id'=>$id,
                'effective_date>='."'".$from."'",'effective_date<='."'".$to."'"
                ]);;
            
            $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Certificates.pdf' 
                    ];
                
            $this->set('c',$c);
            $this->set('from',$from);
            $this->set('to',$to);
            $this->set('id',$id);
            $this->set(compact('policies'));            
        }
        else
        {
            $policies = $this->Policies->find('all',[
                'contain' => ['InsuranceFirms','Clients']
            ])->where([
                'Policies.insurance_firm_id'=>$id,
                'expiry_date>='."'".$from."'",'expiry_date<='."'".$to."'"
                ]);
            
            $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Certificates.pdf' 
                    ];
            
            $this->set('c',$c);
            $this->set('from',$from);
            $this->set('to',$to);
            $this->set('id',$id);
            $this->set(compact('policies')); 
        }
        
    }
}
