<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Certificates Controller
 *
 * @property \App\Model\Table\CertificatesTable $Certificates
 *
 * @method \App\Model\Entity\Certificate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CertificatesController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }
    
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->_validViewOptions[] = 'pdfConfig';
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($printed = null)
    {
        /*$this->paginate = [
            'contain' => ['Policies', 'InsuranceFirms']
        ];*/
        $certificates = $this->Certificates->find('all',[
                'conditions'=>['Certificates.printed'=>$printed],
                'contain' => ['Vehicles', 'InsuranceFirms'],
                'ORDER'=>['id'=>'DESC']
            ]);
        //die(print_r($certificates));
        $this->set(compact('certificates'));
    }
    
    public function indexls()
    {
        $standards = $this->Certificates->find('all');

        $this->set(compact('standards'));
    }

    /**
     * View method
     *
     * @param string|null $id Certificate id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $certificate = $this->Certificates->get($id, [
            'contain' => ['Vehicles', 'InsuranceFirms','Categories']
        ]);

        $this->set('certificate', $certificate);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        
        if ($this->request->is('post')) 
        {
            //die(($this->request->getData('date_received')['year']));
            $certFrom = $this->request->getData('from');
            $certTo = $this->request->getData('to');
            $firmId = $this->request->getData('insurance_firm_id');
            $batch = strtoupper($this->request->getData('batch_no'));
            $rDate = $this->request->getData('date_received');
            if($certFrom <= 0 )
            {
                $this->Flash->error(__('Provide a valid starting serial number. Please, try again.'));
            }
            elseif ($certTo<=0) 
            {
                $this->Flash->error(__('Provide a valid ending serial number. Please, try again.'));
            }
            elseif ($certFrom>$certTo) 
            {
                $this->Flash->error(__('Serial# To cannot be less than serial# from. Please, try again.'));
            }
            else 
            {                
                $count = 0;
                for($i=$certFrom;$i<=$certTo;$i++)
                {
                    $certificate = $this->Certificates->newEntity();
                    $certificate = $this->Certificates->patchEntity($certificate, $this->request->getData());
                    $certificate->insurance_firm_id = $firmId;
                    $certificate->date_received = $rDate;
                    $certificate->batch_no = $batch;
                    $certificate->serial_no = $i;
                    $certificate->status = 0;
                    $certificate->printed = 0;
                    $certificate->date_created = date(DATE_ATOM);
                    if ($this->Certificates->save($certificate)) 
                    {
                        $count++;
                    }
                }
            
                if($count>0)
                {
                    $this->Flash->success(__($count . ' Certificates generation was succesfull!'));

                    return $this->redirect(['action' => 'index',0]);
                } else {
                    //die(print_r($certificate->errors()));
                    $this->Flash->error(__('Certificates generation failed! Please, try again.'));
                }
                
            }
            
        }
        $insuranceFirms = $this->Certificates->InsuranceFirms->find('list', ['limit' => 200]);
        $categories = $this->Certificates->Categories->find('list', ['limit' => 200]);
        $this->set(compact('certificate', 'insuranceFirms','categories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Certificate id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null,$cat = null)
    {
        $certificate = $this->Certificates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $certificate = $this->Certificates->patchEntity($certificate, $this->request->getData());
            $certificate->printed = 0;
            //die(print_r($this->request->getData()));
            $certificate->date_from = $this->request->getData("date_from");
            $certificate->date_to = $this->request->getData("date_to");
            if($certificate->vehicle_id>0)
            {
                if($certificate->status>0)
                {
                    $certificate->user_id = $this->request->session()->read('Config.uid');
                    if ($this->Certificates->save($certificate)) 
                    {
                        $this->Flash->success(__('The certificate has been saved.'));

                        /*$this->redirect(['action'=>'printcert',$id,$certificate->vehicle_id,
                            date('Y-m-d', strtotime($this->request->getData("date_from"))),
                            date('Y-m-d',  strtotime($this->request->getData("date_to"))),
							$certificate->serial_no,
                            '_ext' => 'pdf']);*/
			$this->redirect(['action'=>'view',$id]);
                    }else{
                        $this->Flash->error(__('The certificate could not be saved. Please, try again.'));
                    }
                }
                else
                {
                    $certificate->printed = 0;
                    $this->Flash->error(__('The certificate could not be printed because it is not yet approved.Please, try again.'));
                }
                
            } 
            else 
            {        
                $certificate->printed = 0;
                $this->Flash->error(__('The certificate could not be printed because it is not assigned to a vehicle. Please, try again.'));
            }
        }
        if($certificate->printed==1)
        {
            $certificate->date_from = date('Y-m-d',  strtotime($certificate->date_from));
            $certificate->date_to = date('Y-m-d',  strtotime($certificate->date_to));
        }
        else
        {
            $cert_dates = $this->Certificates->Vehicles->find('all',['fields'=>['effective_date','expiry_date']])->where(['id'=>$certificate->vehicle_id]);
        
            $results = $cert_dates->all();
            $data = $results->toArray();
            $certificate->date_from = date('Y-m-d', strtotime($data[0]['effective_date']));
            $certificate->date_to = date('Y-m-d', strtotime($data[0]['expiry_date']));            
        }
                
        $vehicles = $this->Certificates->Vehicles->find('list', ['limit' => 1])->where(['id'=>$certificate->vehicle_id]);
        
        $insuranceFirms = $this->Certificates->InsuranceFirms->find('list', ['limit' => 200])->where(['id'=>$certificate->insurance_firm_id]);
        $categories = $this->Certificates->Categories->find('list', ['limit' => 200])->where(['id'=>$cat]);
        $this->set(compact('certificate', 'vehicles', 'insuranceFirms','categories'));
    }
    
    public function cert($id = null,$vid = null, $fid = null)
    {
        if ($this->request->is('ajax')) 
        {
            $this->autoRender = false;
                        
            $certificatesTable = TableRegistry::get('Certificates');
            $certificate = $certificatesTable->get($id);
            $data = [
                    'status' => '1',
                    'vehicle_id' => $vid
                ];
            
            $certificatesTable->patchEntity($certificate, $data);
            if($certificatesTable->save($certificate))
            {
                //echo "OK -".$id. " == ".$vid;
                echo '200 OK';
            }else {print_r( $certificate->errors());}
            
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Certificate id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $certificate = $this->Certificates->get($id);
        if ($this->Certificates->delete($certificate)) {
            $this->Flash->success(__('The certificate has been deleted.'));
        } else {
            $this->Flash->error(__('The certificate could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * 
     * 
     */
    
    public function staging($id = null,$vid = null,$dfrom=null,$dto=null,$sno=null) 
    {
        //
        $certificatesTable = TableRegistry::get('Certificates');
        $row = $certificatesTable->get($id); // 
        
        if($vid>0)
        {
            $row->printed = 1;
            if($certificatesTable->save($row))
            {
                $this->redirect(['action'=>'printcert',$id,$vid,$dfrom,$dto,$sno,
                            '_ext' => 'pdf']);

            }  else {
                $this->Flash->error(__('The certificate could not be printed because it is not assigned to a vehicle. Please, try again.'));
            }                    
            
        }
        else
        {
            $this->Flash->error(__('The certificate could not be printed because it is not yet approved.Please, try again.'));
        }
        return $this->redirect(['action' => 'view',$id]);
        
    }
    
    public function printcert($id = null,$vid = null,$dfrom=null,$dto=null,$sno=null)
    {
        
        $connection = ConnectionManager::get('default');

        $stmt = $connection->execute('select vehicles.registration_number as reg,
                date(vehicles.effective_date) As edate,date(vehicles.expiry_date) AS exdate,
                policies.name as policy,
                cover_types.name as cover,clients.name as client,
                insurance_firms.name as firm
                from vehicles
                left join policies on policies.id = vehicles.policy_id
                left join cover_types on cover_types.id = vehicles.cover_type_id
                inner join clients on policies.client_id=clients.id
                inner join insurance_firms on policies.insurance_firm_id=insurance_firms.id
                where vehicles.id=?', [$vid]);

        /*$this->pdfConfig = [
            'orientation' => 'portrait',
            'filename' => 'Certificate_'.$sno.'.pdf' 
        ];*/
        
        $this->pdfConfig = 
        [
            'margin' => [
            'bottom' => 15,
            'left' => 0,
            'right' => 0,
            'top' => 5],
            'pageSize' => 'A3',
            'orientation' => 'portrait',
            'filename' => 'Certificate_'.$sno.'.pdf' 
        ];

        // Read one row.
        //$row = $stmt->fetch('assoc');
        // Read all rows.
        $rows = $stmt->fetchAll('assoc');
        $this->set('dTo', $dto);
        $this->set('dFrom',$dfrom);
                $this->set('serial',$sno);
        $this->set('rows', $rows);
        //return $this->redirect(['action' => 'view',$id]); 
        
    }
    
    public function dateparams() 
    {
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'certbyrcvddates', 
                        $this->request->getData('all'),
                $this->request->getData('date_from'),
                $this->request->getData('date_to')]);
        }        
    }
    
    public function firmparams() 
    {
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'certbyfirm', 
                        $this->request->getData('all'),
                        $this->request->getData('insurance_firm_id'),
                        $this->request->getData('date_from'),
                        $this->request->getData('date_to')]);
        }    
        $insuranceFirms = $this->Certificates->InsuranceFirms->find('list', ['limit' => 200]);
        $categories = $this->Certificates->Categories->find('list', ['limit' => 200]);
        $this->set(compact('certificate', 'vehicles', 'insuranceFirms','categories'));
    }
    
    public function certbyrcvddates($all=null,$from=null,$to=null) 
    {
        if($all=='Yes')
        {
            $certificates = $this->Certificates->find('all',[
                'contain' => [
                    'Vehicles', 
                    'InsuranceFirms']
            ])
                    ->where(['date_received>='."'".$from."'",'date_received<='."'".$to."'"]);

            $this->pdfConfig = [
                'orientation' => 'landscape',
                'filename' => 'Certificates.pdf' 
            ];
            
            $this->set(compact('certificates'));
            $this->set('all',$all);
            $this->set('from',$from);
            $this->set('to',$to);
        }
        else 
        {
            $certificates = $this->Certificates->find('all',[
                'contain' => [
                    'Vehicles'=>['Policies'], 
                    'InsuranceFirms']
            ])
                    ->where(['date_received>='."'".$from."'",'date_received<='."'".$to."'"]);
            $this->pdfConfig = [
                'orientation' => 'landscape',
                'filename' => 'Certificates.pdf' 
            ];
            $this->set('all',$all);
            $this->set('from',$from);
            $this->set('to',$to);
            $this->set(compact('certificates'));
        }
        
        
    }
    
    public function certbyfirm($all=null,$id=null,$from=null,$to=null) 
    {
        if($all=='Yes')
        {
            $certificates = $this->Certificates->find('all',[
                'contain' => ['Vehicles', 'InsuranceFirms']
            ])
                    ->where([
                        'Certificates.insurance_firm_id'=>$id,
                        'Certificates.date_received>='."'".$from."'",
                        'Certificates.date_received<='."'".$to."'"]);
            
            $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Certificates.pdf' 
                    ];
                        
            $this->set('id',$id);
            $this->set('from',$from);
            $this->set('to',$to);
            $this->set('all',$all);
            $this->set(compact('certificates'));
        }
        else
        {
            $certificates = $this->Certificates->find('all',[
                'contain' => ['Vehicles'=>['Policies'], 'InsuranceFirms']
            ])
                    ->where([
                        'Certificates.insurance_firm_id'=>$id,
                        'Certificates.date_received>='."'".$from."'",
                        'Certificates.date_received<='."'".$to."'"]);
            
            $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Certificates.pdf' 
                    ];
            
            
            $this->set('id',$id);
            $this->set('from',$from);
            $this->set('to',$to);
            $this->set('all',$all);
            $this->set(compact('certificates'));
        }        
    }
    
    public function certpdf($all=null,$from=null,$to=null) 
    {
        $this->redirect(['action'=>'certbyrcvddates',$all,$from,$to,
                            '_ext' => 'pdf']);        
    }
    
    public function batchparams() 
    {
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'batchcerts', 
                        $this->request->getData('batch')]);
        }         
    }


    public function batchcerts($batch=null) 
    {
        $certificates = $this->Certificates->find('all',[
                'contain' => [
                    'Vehicles'=>['Policies'], 
                    'InsuranceFirms']
            ])
                    ->where(['batch_no'=>$batch]);
        
        $details = $this->Certificates->Batches->find('all',[
            'contain' => ['InsuranceFirms','Categories']
        ])->where(['batch_no'=>$batch]);

            $this->pdfConfig = [                
                'orientation' => 'portrait',
                'filename' => 'Certificates.pdf' 
            ];
            
            $this->set(compact('certificates'));
            $this->set('all',$batch);
            $this->set(compact('details'));
    }
}
