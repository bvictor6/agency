<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimsTable $Claims
 *
 * @method \App\Model\Entity\Claim[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClaimsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Vehicles','Garages']
        ];*/
        $claims = $this->Claims->find('all',[
            'contain' => ['Vehicles','Garages','Policies']
        ]);

        $this->set(compact('claims'));
    }

    /**
     * View method
     *
     * @param string|null $id Claim id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $claim = $this->Claims->get($id, [
            'contain' => ['Vehicles','Garages','Policies','ClaimsDocuments'=>['DocumentTypes']]
        ]);
        
        $this->pdfConfig = 
        [
            'margin' => [
            'bottom' => 15,
            'left' => 10,
            'right' => 10,
            'top' => 15],
            'orientation' => 'portrait',
            'filename' => 'CLAIM_'.$claim['name'].'.pdf' 
        ];

        $this->set('claim', $claim);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($vid = null,$pid = null)
    {
        $claim = $this->Claims->newEntity();
        if ($this->request->is('post')) 
        {
            //die(($this->request->getData("date1")));
            $claim = $this->Claims->patchEntity($claim, $this->request->getData());
            $claim->date_created = date(DATE_ATOM);
            $claim->date_reported = $this->request->getData("date_reported");
            $claim->accident_date = $this->request->getData("accident_date");
            $claim->user_id = $this->request->session()->read('Config.uid');
            $claim->policy_id = $pid;
            if ($this->Claims->save($claim)) 
            {
                $this->Flash->success(__('The claim has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The claim could not be saved. Please, try again.'));
        }
        $vehicles = $this->Claims->Vehicles->find('list', ['limit' => 200])->where(['id'=>$vid]);
        $garages = $this->Claims->Garages->find('list', ['limit' => 200]);
        $this->set(compact('claim', 'vehicles','garages'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Claim id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $claim = $this->Claims->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $claim = $this->Claims->patchEntity($claim, $this->request->getData());
            $claim->date_created = date(DATE_ATOM);
            $claim->date_reported = $this->request->getData("date_reported");
            $claim->accident_date = $this->request->getData("accident_date");
            if ($this->Claims->save($claim)) {
                $this->Flash->success(__('The claim has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The claim could not be saved. Please, try again.'));
        }
        $claim->date_reported = date('Y-m-d', strtotime($claim->date_reported));
        $claim->accident_date = date('Y-m-d', strtotime($claim->accident_date));
        $vehicles = $this->Claims->Vehicles->find('list', ['limit' => 200]);
        $garages = $this->Claims->Garages->find('list', ['limit' => 200]);
        $this->set(compact('claim', 'vehicles','garages'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Claim id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $claim = $this->Claims->get($id);
        if ($this->Claims->delete($claim)) {
            $this->Flash->success(__('The claim has been deleted.'));
        } else {
            $this->Flash->error(__('The claim could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function dateparams() 
    {
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'claimsbydates', 
                $this->request->getData('date_from'),
                $this->request->getData('date_to')]);
        }        
    }
    
    public function firmparams() 
    {        
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'claimsbyfirm', 
                        $this->request->getData('insurance_firm_id'),
                        $this->request->getData('date_from'),
                        $this->request->getData('date_to')]);
        }  
        $this->loadModel('InsuranceFirms');
        $insuranceFirms = $this->InsuranceFirms->find('list', ['limit' => 200]);
        $this->set(compact('claims', 'insuranceFirms'));
    }
    
    public function claimsbydates($from=null,$to=null) 
    {
        $claims = $this->Claims->find('all',[
            'contain' => ['Vehicles','Garages','Policies']
        ])->where(['date_reported>='."'".$from."'",'date_reported<='."'".$to."'"]);
        
        $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'portrait',
                        'filename' => 'Payments.pdf' 
                    ];
        
        $this->set('from',$from);
        $this->set('to',$to);

        $this->set(compact('claims'));        
    }
    
    
    public function claimsbyfirm($c=null,$from=null,$to=null) 
    {
        $claims = $this->Claims->find('all',[
            'contain' => ['Vehicles','Garages','Policies']
        ])->where(['date_reported>='."'".$from."'",'date_reported<='."'".$to."'"]);
        
        $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'portrait',
                        'filename' => 'Payments.pdf' 
                    ];
        
        $this->set('from',$from);
        $this->set('to',$to);
        $this->set('c',$c);
        $this->set(compact('claims'));        
    }
}
