<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CoverTypes Controller
 *
 * @property \App\Model\Table\CoverTypesTable $CoverTypes
 *
 * @method \App\Model\Entity\CoverType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CoverTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['LineOfBusinesses']
        ];*/
        $coverTypes = $this->CoverTypes->find('all',[
            'contain' => ['LineOfBusinesses']
        ]);

        $this->set(compact('coverTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Cover Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $coverType = $this->CoverTypes->get($id, [
            'contain' => ['LineOfBusinesses', 'Vehicles','Risks']
        ]);

        $this->set('coverType', $coverType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $coverType = $this->CoverTypes->newEntity();
        if ($this->request->is('post')) {
            $coverType = $this->CoverTypes->patchEntity($coverType, $this->request->getData());
            $coverType->user_id = $this->request->session()->read('Config.uid');
            $coverType->date_created = date(DATE_ATOM);
            if ($this->CoverTypes->save($coverType)) {
                $this->Flash->success(__('The cover type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cover type could not be saved. Please, try again.'));
        }
        $lineOfBusinesses = $this->CoverTypes->LineOfBusinesses->find('list', ['limit' => 200]);
        $this->set(compact('coverType', 'lineOfBusinesses'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cover Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $coverType = $this->CoverTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $coverType = $this->CoverTypes->patchEntity($coverType, $this->request->getData());
            if ($this->CoverTypes->save($coverType)) {
                $this->Flash->success(__('The cover type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cover type could not be saved. Please, try again.'));
        }
        $lineOfBusinesses = $this->CoverTypes->LineOfBusinesses->find('list', ['limit' => 200]);
        $this->set(compact('coverType', 'lineOfBusinesses'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cover Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $coverType = $this->CoverTypes->get($id);
        if ($this->CoverTypes->delete($coverType)) {
            $this->Flash->success(__('The cover type has been deleted.'));
        } else {
            $this->Flash->error(__('The cover type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
