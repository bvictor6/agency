<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PolicyActions Controller
 *
 * @property \App\Model\Table\PolicyActionsTable $PolicyActions
 *
 * @method \App\Model\Entity\PolicyAction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PolicyActionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($id = null)
    {
        
        $policyActions = $this->PolicyActions->find('all',[
            'contain' => ['Users', 'Policies']
        ])->where(['policy_id'=>$id]);

        $this->set(compact('policyActions'));
    }

    /**
     * View method
     *
     * @param string|null $id Policy Action id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $policyAction = $this->PolicyActions->get($id, [
            'contain' => ['Users','Policies']
        ]);

        $this->set('policyAction', $policyAction);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null,$action = null)
    {
        $policyAction = $this->PolicyActions->newEntity();
        if ($this->request->is('post')) 
        {
            $policyAction = $this->PolicyActions->patchEntity($policyAction, $this->request->getData());
            $policyAction->action_date = date(DATE_ATOM);
            $policyAction->user_id = $this->request->session()->read('Config.uid');
            $p_action = $this->request->getData('action');
            //die(print_r($this->request->getData()));
            if ($this->PolicyActions->save($policyAction)) 
            {
                //update policy with the action performed
                $policyTable = \Cake\ORM\TableRegistry::get('Policies');
                $policy = $policyTable->find('all')->where(['id'=>$id])->first();
                $policy->status = $p_action;
                
                if($policyTable->save($policy))
                {
                    $this->Flash->success(__('The policy action has been saved.'));
                    return $this->redirect(['action' => 'view',$policyAction->id]);
                }
                
            }
            $this->Flash->error(__('The policy action could not be saved. Please, try again.'));
        }
        $policies = $this->PolicyActions->Policies->find('list', ['limit' => 1])->where(['id'=>$id]);
        //$policyAction->action = $action;
        //$users = $this->PolicyActions->Users->find('list', ['limit' => 200]);
        $this->set(compact('policyAction', 'policies','action'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Policy Action id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $policyAction = $this->PolicyActions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $policyAction = $this->PolicyActions->patchEntity($policyAction, $this->request->getData());
            if ($this->PolicyActions->save($policyAction)) {
                $this->Flash->success(__('The policy action has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The policy action could not be saved. Please, try again.'));
        }
        $users = $this->PolicyActions->Users->find('list', ['limit' => 200]);
        $this->set(compact('policyAction', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Policy Action id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $policyAction = $this->PolicyActions->get($id);
        if ($this->PolicyActions->delete($policyAction)) {
            $this->Flash->success(__('The policy action has been deleted.'));
        } else {
            $this->Flash->error(__('The policy action could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
