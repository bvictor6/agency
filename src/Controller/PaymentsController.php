<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Payments Controller
 *
 * @property \App\Model\Table\PaymentsTable $Payments
 *
 * @method \App\Model\Entity\Payment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Policies', 'Vehicles']
        ];*/
        $payments = $this->Payments->find('all',[
            'contain' => ['Policies', 'Vehicles']
        ]);

        $this->set(compact('payments'));
    }

    /**
     * View method
     *
     * @param string|null $id Payment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $payment = $this->Payments->get($id, [
            'contain' => ['Policies', 'Vehicles']
        ]);

        $this->set('payment', $payment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($p = null, $v = null)
    {
        $payment = $this->Payments->newEntity();
        if ($this->request->is('post')) 
        {
            $payment = $this->Payments->patchEntity($payment, $this->request->getData());
            $payment->date_created = date(DATE_ATOM);
            $payment->user_id = $this->request->session()->read('Config.uid');
            $payment->payment_date = $this->request->getData("payment_date");
            if ($this->Payments->save($payment)) {
                $this->Flash->success(__('The payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payment could not be saved. Please, try again.'));
        }
        $policies = $this->Payments->Policies->find('list', ['limit' => 200])->where(['id'=>$p]);
        $vehicles = $this->Payments->Vehicles->find('list', ['limit' => 200])->where(['id'=>$v]);
        $this->set(compact('payment', 'policies', 'vehicles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Payment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $payment = $this->Payments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $payment = $this->Payments->patchEntity($payment, $this->request->getData());
            $payment->payment_date = $this->request->getData("payment_date");
            if ($this->Payments->save($payment)) {
                $this->Flash->success(__('The payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payment could not be saved. Please, try again.'));
        }
        $payment->payment_date = date('Y-m-d', strtotime($payment->payment_date));
        $policies = $this->Payments->Policies->find('list', ['limit' => 200]);
        $vehicles = $this->Payments->Vehicles->find('list', ['limit' => 200]);
        $this->set(compact('payment', 'policies', 'vehicles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Payment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $payment = $this->Payments->get($id);
        if ($this->Payments->delete($payment)) {
            $this->Flash->success(__('The payment has been deleted.'));
        } else {
            $this->Flash->error(__('The payment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function dateparams() 
    {
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'paymentsbydates', 
                $this->request->getData('date_from'),
                $this->request->getData('date_to')]);
        }        
    }
    
    public function firmparams() 
    {        
        if ($this->request->is('post')) 
        {
            return $this->redirect(['action' => 'paymentsbyfirm', 
                        $this->request->getData('insurance_firm_id'),
                        $this->request->getData('date_from'),
                        $this->request->getData('date_to')]);
        }  
        $this->loadModel('InsuranceFirms');
        $insuranceFirms = $this->InsuranceFirms->find('list', ['limit' => 200]);
        $this->set(compact('payments', 'insuranceFirms'));
    }
    
    public function paymentsbydates($from=null,$to=null) 
    {
        $payments = $this->Payments->find('all',[
            'contain' => ['Policies'=>['InsuranceFirms'], 'Vehicles']
        ])->where(['payment_date>='."'".$from."'",'payment_date<='."'".$to."'"]);
        
        $this->pdfConfig = 
                    [
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Payments.pdf' 
                    ];
        
        $this->set('from',$from);
        $this->set('to',$to);

        $this->set(compact('payments'));
        
    }
    
    public function paymentsbyfirm($c=null,$from=null,$to=null) 
    {
        $payments = $this->Payments->find('all',[
            'contain' => ['Policies'=>['InsuranceFirms'], 'Vehicles']
        ])->where(['payment_date>='."'".$from."'",'payment_date<='."'".$to."'"]);
        
        $this->pdfConfig = 
                    [
                        'options' => [
            'print-media-type' => false,
            'outline' => true,
            'dpi' => 96,
            'page-width'=>150,
            'page-height'=>150
        ],
                        'margin' => [
                        'bottom' => 15,
                        'left' => 0,
                        'right' => 0,
                        'top' => 15],
                        'orientation' => 'landscape',
                        'filename' => 'Payments.pdf' 
                    ];
        
        $this->set('from',$from);
        $this->set('to',$to);
        $this->set('c',$c);

        $this->set(compact('payments'));
        
    }
}
