<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * InsuranceFirms Controller
 *
 * @property \App\Model\Table\InsuranceFirmsTable $InsuranceFirms
 *
 * @method \App\Model\Entity\InsuranceFirm[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InsuranceFirmsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $insuranceFirms = $this->InsuranceFirms->find('all');

        $this->set(compact('insuranceFirms'));
    }

    /**
     * View method
     *
     * @param string|null $id Insurance Firm id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $insuranceFirm = $this->InsuranceFirms->get($id, [
            'contain' => ['Policies']
        ]);

        $this->set('insuranceFirm', $insuranceFirm);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $insuranceFirm = $this->InsuranceFirms->newEntity();
        if ($this->request->is('post')) {
            $insuranceFirm = $this->InsuranceFirms->patchEntity($insuranceFirm, $this->request->getData());
            $insuranceFirm->user_id = $this->request->session()->read('Config.uid');
            if ($this->InsuranceFirms->save($insuranceFirm)) {
                $this->Flash->success(__('The insurance firm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The insurance firm could not be saved. Please, try again.'));
        }
        $this->set(compact('insuranceFirm'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Insurance Firm id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $insuranceFirm = $this->InsuranceFirms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $insuranceFirm = $this->InsuranceFirms->patchEntity($insuranceFirm, $this->request->getData());
            if ($this->InsuranceFirms->save($insuranceFirm)) {
                $this->Flash->success(__('The insurance firm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The insurance firm could not be saved. Please, try again.'));
        }
        $this->set(compact('insuranceFirm'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Insurance Firm id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $insuranceFirm = $this->InsuranceFirms->get($id);
        if ($this->InsuranceFirms->delete($insuranceFirm)) {
            $this->Flash->success(__('The insurance firm has been deleted.'));
        } else {
            $this->Flash->error(__('The insurance firm could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
