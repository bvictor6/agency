<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClaimsDocuments Controller
 *
 * @property \App\Model\Table\ClaimsDocumentsTable $ClaimsDocuments
 *
 * @method \App\Model\Entity\ClaimsDocument[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClaimsDocumentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['DocumentTypes', 'Claims']
        ];*/
        $claimsDocuments = $this->ClaimsDocuments->find('all',[
            'contain' => ['DocumentTypes', 'Claims'=>['Vehicles']]
        ]);

        $this->set(compact('claimsDocuments'));
    }

    /**
     * View method
     *
     * @param string|null $id Claims Document id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $claimsDocument = $this->ClaimsDocuments->get($id, [
            'contain' => ['DocumentTypes', 'Claims']
        ]);

        $this->set('claimsDocument', $claimsDocument);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id=null)
    {
        $claimsDocument = $this->ClaimsDocuments->newEntity();
        if ($this->request->is('post')) 
        {
            //die($this->request->getData('policy_doc')['name']);
            if(($this->request->getData('claims_doc')['size']/1024)>0)
            {
                $ERR_CODE = $this->request->getData('claims_doc')['error'];
                switch ($ERR_CODE)
                {
                    case 0://UPLOAD_ERR_OK
                        //$this->Flash->success('There is no error, the file uploaded with success.');
                        $dir = WWW_ROOT.'files'.DS.'claims';
                        $fileName = $dir.DS.$this->request->getData('claims_doc')['name'];//die($dir);
                        if(file_exists($dir) && is_dir($dir))
                        {
                            if(move_uploaded_file($this->request->getData('claims_doc')['tmp_name'], $fileName))
                            {
                                $claimsDocument = $this->ClaimsDocuments->patchEntity($claimsDocument, $this->request->getData());
                                $claimsDocument->date_created = date(DATE_ATOM);
                                $claimsDocument->filemime = $this->request->getData('claims_doc')['type'];
                                $claimsDocument->filesize = $this->request->getData('claims_doc')['size']/1024;
                                $claimsDocument->name = $this->request->getData('claims_doc')['name'];
                                $claimsDocument->path = $dir.DS.$this->request->getData('claims_doc')['name'];
                                $claimsDocument->policy_id = $this->request->getData('claim_id');
                                $claimsDocument->user_id = $this->request->session()->read('Config.uid');
                                if ($this->ClaimsDocuments->save($claimsDocument)) 
                                {
                                    $this->Flash->success(__('The document has been saved.'));

                                    return $this->redirect(['controller'=>'Claims','action' => 'view',$claimsDocument->claim_id]);
                                }
                                $this->Flash->error(__('The document could not be saved. Please, try again.'));
                            }
                        }
                        break;
                    case 1:
                        $this->Flash->error('The uploaded file exceeds the upload_max_filesize directive in php.ini.');
                        break;
                    case 2:
                        $this->Flash->error('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
                        break;
                    case 3:
                        $this->Flash->error('The uploaded file was only partially uploaded.');
                        break;
                    case 4:
                        $this->Flash->error('No file was uploaded.');
                        break;
                    case 6:
                        $this->Flash->error('Missing a temporary folder.');
                        break;
                    case 7:
                        $this->Flash->error(' Failed to write file to disk. ');
                        break;
                    case 8:
                        $this->Flash->error('A PHP extension stopped the file upload.');
                        break;
                    default:
                        $this->Flash->error('An unknown error occured, probably file to large. Please, try again later!');
                        break;
                }
            }
            else {
                $this->Flash->error('An unknown error occured, probably file to large. Please, try again later!');
            }
            
        }
        $documentTypes = $this->ClaimsDocuments->DocumentTypes->find('list', ['limit' => 200]);
        $claims = $this->ClaimsDocuments->Claims->find('list', ['limit' => 200])->where(['id'=>$id]);
        $this->set(compact('claimsDocument', 'documentTypes', 'claims'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Claims Document id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $claimsDocument = $this->ClaimsDocuments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $claimsDocument = $this->ClaimsDocuments->patchEntity($claimsDocument, $this->request->getData());
            if ($this->ClaimsDocuments->save($claimsDocument)) {
                $this->Flash->success(__('The claims document has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The claims document could not be saved. Please, try again.'));
        }
        $documentTypes = $this->ClaimsDocuments->DocumentTypes->find('list', ['limit' => 200]);
        $claims = $this->ClaimsDocuments->Claims->find('list', ['limit' => 200]);
        $this->set(compact('claimsDocument', 'documentTypes', 'claims'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Claims Document id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $claimsDocument = $this->ClaimsDocuments->get($id);
        if ($this->ClaimsDocuments->delete($claimsDocument)) {
            $this->Flash->success(__('The claims document has been deleted.'));
        } else {
            $this->Flash->error(__('The claims document could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function downloadFile($id)
    {
        $file = $this->Documents->get($id);//die($file['path']);
        $response = $this->response->withFile(
                    $file['path'],
                    ['download' => true, 'name' => $file['name']]
                );
        // Return the response to prevent controller from trying to render
        // a view.
        return $response;
    }
}
