<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LineOfBusinesses Controller
 *
 * @property \App\Model\Table\LineOfBusinessesTable $LineOfBusinesses
 *
 * @method \App\Model\Entity\LineOfBusiness[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LineOfBusinessesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $lineOfBusinesses = $this->LineOfBusinesses->find('all');

        $this->set(compact('lineOfBusinesses'));
    }

    /**
     * View method
     *
     * @param string|null $id Line Of Business id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lineOfBusiness = $this->LineOfBusinesses->get($id, [
            'contain' => ['Policies']
        ]);

        $this->set('lineOfBusiness', $lineOfBusiness);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lineOfBusiness = $this->LineOfBusinesses->newEntity();
        if ($this->request->is('post')) {
            $lineOfBusiness = $this->LineOfBusinesses->patchEntity($lineOfBusiness, $this->request->getData());
            $lineOfBusiness->user_id = $this->request->session()->read('Config.uid');
            if ($this->LineOfBusinesses->save($lineOfBusiness)) {
                $this->Flash->success(__('The line of business has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The line of business could not be saved. Please, try again.'));
        }
        $this->set(compact('lineOfBusiness'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Line Of Business id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lineOfBusiness = $this->LineOfBusinesses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lineOfBusiness = $this->LineOfBusinesses->patchEntity($lineOfBusiness, $this->request->getData());
            if ($this->LineOfBusinesses->save($lineOfBusiness)) {
                $this->Flash->success(__('The line of business has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The line of business could not be saved. Please, try again.'));
        }
        $this->set(compact('lineOfBusiness'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Line Of Business id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lineOfBusiness = $this->LineOfBusinesses->get($id);
        if ($this->LineOfBusinesses->delete($lineOfBusiness)) {
            $this->Flash->success(__('The line of business has been deleted.'));
        } else {
            $this->Flash->error(__('The line of business could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
