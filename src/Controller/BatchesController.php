<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Batches Controller
 *
 * @property \App\Model\Table\BatchesTable $Batches
 *
 * @method \App\Model\Entity\Batch[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BatchesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['InsuranceFirms', 'Categories']
        ];*/
        $batches = $this->Batches->find('all',[
            'contain' => ['InsuranceFirms', 'Categories']
        ]);

        $this->set(compact('batches'));
    }

    /**
     * View method
     *
     * @param string|null $id Batch id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $batch = $this->Batches->get($id, [
            'contain' => ['InsuranceFirms', 'Categories','Certificates'/*=>[
                'Vehicles'=>['Policies']
            ]*/]
        ]);
        $this->set('batch', $batch);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $batch = $this->Batches->newEntity();
        if ($this->request->is('post')) 
        {
            $batch = $this->Batches->patchEntity($batch, $this->request->getData());
            
            $batch->date_created = date(DATE_ATOM);
            $batch->user_id = $this->request->session()->read('Config.uid');
            
            $certFrom = $this->request->getData('from');
            $certTo = $this->request->getData('to');
            $firmId = $this->request->getData('insurance_firm_id');
            $batch_no = strtoupper($this->request->getData('batch_no'));
            $rDate = $this->request->getData('date_received');
            $catId = $this->request->getData('category_id');
            
            $batch->batch_from = $certFrom;
            $batch->batch_to = $certTo;
            
            if($certFrom <= 0 )
            {
                $this->Flash->error(__('Provide a valid starting serial number. Please, try again.'));
            }
            elseif ($certTo<=0) 
            {
                $this->Flash->error(__('Provide a valid ending serial number. Please, try again.'));
            }
            elseif ($certFrom>$certTo) 
            {
                $this->Flash->error(__('Serial# To cannot be less than serial# from. Please, try again.'));
            }
            else
            {
                if ($this->Batches->save($batch)) 
                {
                    //$this->Flash->success(__('The batch has been saved.'));
                    //return $this->redirect(['action' => 'index']);
                    $count = 0;
                    for($i=$certFrom;$i<=$certTo;$i++)
                    {
                        $cert = $this->loadModel('Certificates');
                        $certificate = $cert->newEntity();// $this->Certificates->newEntity();
                        /*$data = [
                                    'insurance_firm_id' => $firmId,
                                    'date_received' => $rDate,
                                    'batch_no' => $batch_no,
                                    'batch_id' => $batch->id,
                                    'serial_no' => $i,
                                    'status' => 0,
                                    'status' => 0,
                                    'printed' => 0,
                                    'date_created' => date(DATE_ATOM)
                                ];*/
                        $certificate = /*$cert->patchEntity($certificate, $data);*/ $this->Certificates->patchEntity($certificate, $this->request->getData());
                        $certificate->insurance_firm_id = $firmId;
                        $certificate->date_received = $rDate;
                        $certificate->batch_no = $batch_no;
                        $certificate->batch_id = $batch->id;
                        $certificate->serial_no = $i;
                        $certificate->status = 0;
                        $certificate->printed = 0;
                        $certificate->date_created = date(DATE_ATOM);
                        if ($cert->save($certificate)) 
                        {
                            $count++;
                        }
                    }

                    if($count>0)
                    {
                        $this->Flash->success(__($count . ' Certificates generation was succesfull!'));

                        return $this->redirect(['action' => 'index']);
                    } else {
                        //die(print_r($certificate->errors()));
                        $this->Flash->error(__('Certificates generation failed! Please, try again.'));
                    }
                }
                //die(print_r($batch->errors()));
                $this->Flash->error(__('The batch could not be saved. Please, try again.'));                
            }
            
            
        }
        
        $insuranceFirms = $this->Batches->InsuranceFirms->find('list', ['limit' => 200]);
        $categories = $this->Batches->Categories->find('list', ['limit' => 200]);
        $this->set(compact('batch', 'insuranceFirms', 'categories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Batch id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function edit($id = null)
    {
        $batch = $this->Batches->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $batch = $this->Batches->patchEntity($batch, $this->request->getData());
            if ($this->Batches->save($batch)) {
                $this->Flash->success(__('The batch has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The batch could not be saved. Please, try again.'));
        }
        $insuranceFirms = $this->Batches->InsuranceFirms->find('list', ['limit' => 200]);
        $categories = $this->Batches->Categories->find('list', ['limit' => 200]);
        $this->set(compact('batch', 'insuranceFirms', 'categories'));
    }*/

    /**
     * Delete method
     *
     * @param string|null $id Batch id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $batch = $this->Batches->get($id);
        if ($this->Batches->delete($batch)) {
            $this->Flash->success(__('The batch has been deleted.'));
        } else {
            $this->Flash->error(__('The batch could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function savecerts($start, $end) {
        
    }
}
