<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Documents Controller
 *
 * @property \App\Model\Table\DocumentsTable $Documents
 *
 * @method \App\Model\Entity\Document[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Policies','DocumentTypes']
        ];*/
        $documents = $this->Documents->find('all',[
            'contain' => ['Policies','DocumentTypes']
        ]);

        $this->set(compact('documents'));
    }

    /**
     * View method
     *
     * @param string|null $id Document id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $document = $this->Documents->get($id, [
            'contain' => ['Policies','DocumentTypes']
        ]);

        $this->set('document', $document);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id=null)
    {
        $document = $this->Documents->newEntity();
        if ($this->request->is('post')) 
        {
            //die($this->request->getData('policy_doc')['name']);
            if(($this->request->getData('policy_doc')['size']/1024)>0)
            {
                $ERR_CODE = $this->request->getData('policy_doc')['error'];
                switch ($ERR_CODE)
                {
                    case 0://UPLOAD_ERR_OK
                        //$this->Flash->success('There is no error, the file uploaded with success.');
                        $dir = WWW_ROOT.'files';
                        $fileName = $dir.DS.$this->request->getData('policy_doc')['name'];//die($dir);
                        if(file_exists($dir) && is_dir($dir))
                        {
                            if(move_uploaded_file($this->request->getData('policy_doc')['tmp_name'], $fileName))
                            {
                                $document = $this->Documents->patchEntity($document, $this->request->getData());
                                $document->date_created = date(DATE_ATOM);
                                $document->filemime = $this->request->getData('policy_doc')['type'];
                                $document->filesize = $this->request->getData('policy_doc')['size']/1024;
                                $document->name = $this->request->getData('policy_doc')['name'];
                                $document->path = $dir.DS.$this->request->getData('policy_doc')['name'];
                                $document->policy_id = $this->request->getData('policy_id');
                                $document->user_id = $this->request->session()->read('Config.uid');
                                if ($this->Documents->save($document)) 
                                {
                                    $this->Flash->success(__('The document has been saved.'));

                                    return $this->redirect(['controller'=>'Policies','action' => 'view',$document->policy_id]);
                                }
                                $this->Flash->error(__('The document could not be saved. Please, try again.'));
                            }
                        }
                        break;
                    case 1:
                        $this->Flash->error('The uploaded file exceeds the upload_max_filesize directive in php.ini.');
                        break;
                    case 2:
                        $this->Flash->error('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
                        break;
                    case 3:
                        $this->Flash->error('The uploaded file was only partially uploaded.');
                        break;
                    case 4:
                        $this->Flash->error('No file was uploaded.');
                        break;
                    case 6:
                        $this->Flash->error('Missing a temporary folder.');
                        break;
                    case 7:
                        $this->Flash->error(' Failed to write file to disk. ');
                        break;
                    case 8:
                        $this->Flash->error('A PHP extension stopped the file upload.');
                        break;
                    default:
                        $this->Flash->error('An unknown error occured, probably file to large. Please, try again later!');
                        break;
                }
            }
            else {
                $this->Flash->error('An unknown error occured, probably file to large. Please, try again later!');
            }
            
            
            /*---*/
        }
        $policies = $this->Documents->Policies->find('list', ['limit' => 200])->where(['id'=>$id]);
        $document_types = $this->Documents->DocumentTypes->find('list', ['limit' => 200]);
        $this->set(compact('document', 'policies','document_types'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Document id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $document = $this->Documents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $document = $this->Documents->patchEntity($document, $this->request->getData());
            if ($this->Documents->save($document)) {
                $this->Flash->success(__('The document has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The document could not be saved. Please, try again.'));
        }
        $policies = $this->Documents->Policies->find('list', ['limit' => 200]);
        $this->set(compact('document', 'policies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Document id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $document = $this->Documents->get($id);
        if ($this->Documents->delete($document)) {
            $this->Flash->success(__('The document has been deleted.'));
        } else {
            $this->Flash->error(__('The document could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function downloadFile($id)
    {
        $file = $this->Documents->get($id);//die($file['path']);
        $response = $this->response->withFile(
                    $file['path'],
                    ['download' => true, 'name' => $file['name']]
                );
        // Return the response to prevent controller from trying to render
        // a view.
        return $response;
    }
}
