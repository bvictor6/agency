<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    
    public function initialize() {
        parent::initialize();
        //$this->Auth->allow('add','logout');
    }
    
    public function login() 
    {
        if($this->request->is('post'))
        {
            $user = $this->Auth->identify();
            if($user)
            {
                $this->Auth->setUser($user);
                //
                $usersTable = TableRegistry::get('Users');
                $row = $usersTable->get($this->Auth->user('id')); // Return article with id 12
                if($row->disabled)
                {
                    $this->Flash->error(__('Account is disabled!'));
                    //
                    return $this->redirect($this->Auth->logout());
                }
                else
                {
                    $row->last_login = date(DATE_ATOM);
                    $usersTable->save($row);
                    //
                    //set dept session variable
                    $session = $this->request->session();
                    $session->write('Config.uid', $this->Auth->user('id'));
                    return $this->redirect($this->Auth->redirectUrl());
                }
                
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }        
    }
    
    public function logout()
    {
        $this->Flash->success(__('You are now logged out'));
        //
        $session = $this->request->session();
        $session->write('Config.uid', "Destroyed");
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->Users->find('all');

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->date_created = date(DATE_ATOM);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }
    
    public function changepwd($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $oldpass = $user->password;
            $pass = $this->request->getData('new_password');
            $pass1 = $this->request->getData("confirm_password");
            if($pass===$pass1)
            {
                $user->password = $pass;
                if ($this->Users->save($user)) 
                {
                    $this->Flash->success(__('Password succesfully changed.'));

                    return $this->redirect(['action' => 'view',$id]);
                }
                $this->Flash->error(__('Unable to change password. Please, try again.'));                
            }
            
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /**
    * Dumps the MySQL database that this controller's model is attached to.
    * This action will serve the sql file as a download so that the user can save the backup to their local computer.
    *
    * @param string $tables Comma separated list of tables you want to download, or '*' if you want to download them all.
    */
    
   
   
}
