<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Garages Controller
 *
 * @property \App\Model\Table\GaragesTable $Garages
 *
 * @method \App\Model\Entity\Garage[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GaragesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $garages = $this->Garages->find('all');

        $this->set(compact('garages'));
    }
    
    public function indexls()
    {
        $standards = $this->Garages->find('all');

        $this->set(compact('standards'));
    }

    /**
     * View method
     *
     * @param string|null $id Garage id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $garage = $this->Garages->get($id, [
            'contain' => ['Claims']
        ]);

        $this->set('garage', $garage);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $garage = $this->Garages->newEntity();
        if ($this->request->is('post')) 
        {
            $garage = $this->Garages->patchEntity($garage, $this->request->getData());
            $garage->user_id = $this->request->session()->read('Config.uid');
            $garage->date_created = date(DATE_ATOM);
            if ($this->Garages->save($garage)) {
                $this->Flash->success(__('The garage has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The garage could not be saved. Please, try again.'));
        }
        $this->set(compact('garage'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Garage id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $garage = $this->Garages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $garage = $this->Garages->patchEntity($garage, $this->request->getData());
            if ($this->Garages->save($garage)) {
                $this->Flash->success(__('The garage has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The garage could not be saved. Please, try again.'));
        }
        $this->set(compact('garage'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Garage id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $garage = $this->Garages->get($id);
        if ($this->Garages->delete($garage)) {
            $this->Flash->success(__('The garage has been deleted.'));
        } else {
            $this->Flash->error(__('The garage could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
