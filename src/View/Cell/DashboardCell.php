<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * Dashboard cell
 */
class DashboardCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        /*$this->loadModel('StoreAutoSupplies');
        $sum = $this->StoreAutoSupplies->find();
        $sum->select([
                'pairs'=>$sum->func()->sum('total_stock'),'store'=>'store_id'
            ])->group('store')
              ->limit(10)
              ->order('pairs DESC');
        $sum->hydrate(false);
        $this->set('totals',$sum->toList());*/
        //die(print_r($sum->toList()))
        
        //MySQL DAYOFWEEK() returns the week day number (1 for Sunday,2 for Monday …… 7 for Saturday ) for a date specified as argument.
        
        $this->loadModel('Policies');
        $latest = $this->Policies->find()->where('date_created>= DATE(NOW()) - INTERVAL 7 DAY');
        $latest->select([
            'total'=>$latest->func()->count('id'),'weekday'=>'DAYOFWEEK(date_created)'
        ])->group('weekday');
        $latest->hydrate(false);//results as arrays instead of entities
        $this->set('policies',$latest->toList());//execute the query and return the array
    }
    
    public function totalFirms() 
    {
        $this->loadModel('InsuranceFirms');
        $total = $this->InsuranceFirms->find();
        $total->select([
            'total'=>$total->func()->count('id')
        ]);
        $total->hydrate(false);
        $this->set('firms', $total->toList());
    }
    
    public function totallob() 
    {
        $this->loadModel('LineOfBusinesses');
        $total = $this->LineOfBusinesses->find();
        $total->select([
            'total'=>$total->func()->count('id')
        ]);
        $total->hydrate(false);
        $this->set('lobs', $total->toList());        
    }
    
    public function totalPolicies() 
    {
        $this->loadModel('Policies');
        $total = $this->Policies->find();
        $total->select([
            'total'=>$total->func()->count('id')
        ]);
        $total->hydrate(false);
        $this->set('policies', $total->toList());
    }
    
    public function totalClients() 
    {
        $this->loadModel('Clients');
        $total = $this->Clients->find();
        $total->select([
            'total'=>$total->func()->count('id')
        ]);
        $total->hydrate(false);
        $this->set('clients', $total->toList());
    }
    
    public function latestPolicies() 
    {
        $this->loadModel('Policies');
        $latest = $this->Policies->find('all',['limit'=>20,'order'=>'date_created DESC']);
        $latest->hydrate(false);//results as arrays instead of entities
        $this->set('policies',$latest->toList());//execute the query and return the array
    }
    
    public function lastweek() 
    {
        $this->loadModel('Policies');
        $latest = $this->Policies->find()->where('date_created>= DATE(NOW()) - INTERVAL 7 DAY');
        $latest->select([
            'total'=>$latest->func()->count('id')
        ]);
        $latest->hydrate(false);//results as arrays instead of entities
        $this->set('policies',$latest->toList());//execute the query and return the array
    }
    
    public function last24hrs() 
    {
        $this->loadModel('Policies');
        $latest = $this->Policies->find()->where('date_created>= DATE(NOW()) - INTERVAL 1 DAY');
        $latest->select([
            'total'=>$latest->func()->count('id')
        ]);
        $latest->hydrate(false);
        $this->set('policies',$latest->toList());
    }
    
    public function clientsLastWeek()
    {
        $this->loadModel('Clients');
        $clients = $this->Clients->find()->where('date_created>= DATE(NOW()) - INTERVAL 7 DAY');
        $clients->select([
            'total'=>$clients->func()->count('id')
        ]);
        $clients->hydrate(false);
        $this->set('clients',$clients->toList());
    }
    
    public function vehiclesLastWeek()
    {
        $this->loadModel('Vehicles');
        $vehicles = $this->Vehicles->find()->where('date_created>= DATE(NOW()) - INTERVAL 7 DAY');
        $vehicles->select([
            'total'=>$vehicles->func()->count('id')
        ]);
        $vehicles->hydrate(false);
        $this->set('vehicles',$vehicles->toList());
    }
    
    public function certsLastWeek()
    {
        $this->loadModel('Certificates');
        $certificates = $this->Certificates->find()->where('date_created>= DATE(NOW()) - INTERVAL 7 DAY');
        $certificates->select([
            'total'=>$certificates->func()->count('id')
        ]);
        $certificates->hydrate(false);
        $this->set('certificates',$certificates->toList());
    }
    
    public function policiesLastWeek() 
    {
        $this->loadModel('Policies');
        $policies = $this->Policies->find()->where('date_created>= DATE(NOW()) - INTERVAL 7 DAY');
        $policies->select([
            'total'=>$policies->func()->count('id')
        ]);
        $policies->hydrate(false);//results as arrays instead of entities
        $this->set('policies',$policies->toList());//execute the query and return the array
    }
    
    public function companyName() 
    {
        $this->loadModel('Settings');
        $details = $this->Settings->find('all',['limit'=>1]);
        $details->enableHydration(false);
        $this->set('details', $details->toList());
        
    }
}
