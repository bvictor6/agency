# Insurance Agency Web Application
A web based application for Insurance Agencies to carry out their underwriting business. Developd using CakePHP framework.

## Installation

Clone the project to your local machine and you are good to go.

## Update

The project is open source and distributed free of charge. You can update and modify the code to suite your needs. 
Occasionaly check back here to see if there is any new stuff.

## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your setup.


