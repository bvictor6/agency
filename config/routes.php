<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);
# Set this before you specify any routes
Router::extensions('xlsx');

/*** cakePdf - victor **/
/*Router::scope('/', function ($routes) {
    $routes->extensions('pdf');
    $routes->connect('/view/*', ['controller' => 'Users', 'action' => 'view']);
    $routes->fallbacks('InflectedRoute');
});*/

/*Router::scope('/Policies/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    //$routes->extensions('pdf');
    $routes->connect('/view/*', ['controller' => 'Policies', 'action' => 'view']);
    $routes->fallbacks('InflectedRoute');
});/*
/***    ***/

Router::scope('/', function (RouteBuilder $routes) {
    Router::extensions(['pdf']);
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/Certificates/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/printcert/*', ['controller' => 'Certificates', 'action' => 'printcert']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Policies/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/view/*', ['controller' => 'Policies', 'action' => 'view']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Policies/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/policies/*', ['controller' => 'Policies', 'action' => 'policies']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Policies/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/policybydates/*', ['controller' => 'Policies', 'action' => 'policybydates']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Policies/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/policiesbyfirm/*', ['controller' => 'Policies', 'action' => 'policiesbyfirm']);
    $routes->fallbacks('InflectedRoute');
});
Router::scope('/Clients/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/clients/*', ['controller' => 'Clients', 'action' => 'clients']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Vehicles/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/risks/*', ['controller' => 'Vehicles', 'action' => 'risks']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Certificates/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/certbyrcvddates/*', ['controller' => 'Certificates', 'action' => 'certbyrcvddates']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Certificates/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/certbyfirm/*', ['controller' => 'Certificates', 'action' => 'certbyfirm']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Certificates/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/batchcerts/*', ['controller' => 'Certificates', 'action' => 'batchcerts']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Payments/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/paymentsbydates/*', ['controller' => 'Payments', 'action' => 'paymentsbydates']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Payments/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/paymentsbyfirm/*', ['controller' => 'Payments', 'action' => 'paymentsbyfirm']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Claims/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/claimsbydates/*', ['controller' => 'Claims', 'action' => 'claimsbydates']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Claims/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/view/*', ['controller' => 'Claims', 'action' => 'view']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Claims/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/claimsbyfirm/*', ['controller' => 'Claims', 'action' => 'claimsbyfirm']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/Expenses/', function ($routes) 
{
    
    $routes->setExtensions('pdf');
    $routes->connect('/expensesbydates/*', ['controller' => 'Expenses', 'action' => 'expensesbydates']);
    $routes->fallbacks('InflectedRoute');
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
